import React, {Component} from "react";
import {createAppContainer} from "react-navigation";

import {RootNavigator} from './router';


export default class App extends Component<Props> {
  render() {
    const RenderScreen = createAppContainer(RootNavigator());
    return <RenderScreen/>;
  }
}
