
import React from "react";
import {
  Image, Platform
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createBottomTabNavigator
} from "react-navigation";

import colors from "./src/styles/colors";
import Home from "./src/Screens/Home";
import Otp from "./src/Screens/Otp";
import ProjectSelection from "./src/Screens/ProjectSelection";
import WorkerListScreen from "./src/Screens/WorkerListScreen";
import Splash from "./src/Screens/Splash";
import SlideMenu from "./src/Components/SlideMenu";
import SlideMenuLC from "./src/Components/SlideMenuLC";
import SlideMenuWorker from "./src/Components/SlideMenuWorker";
import Verification from "./src/Screens/Verification";
import VerificationCode from "./src/Screens/VerificationCode";
import JOBS from "./src/Screens/JobList";
import People from "./src/Screens/People";
import GeneralContractor from "./src/Screens/GeneralContractor";
import LaborContractor from "./src/Screens/LaborContractor";
import LaborJobList from "./src/Screens/LaborJobList";
import ModalAddProjects from "./src/Modals/ModalAddProjects";
import PROFILE from "./src/Screens/Profile";
import AddJobs from "./src/Screens/AddJobs";
import SelectContractorForJob from "./src/Screens/SelectContractorForJob";
import PROPOSAL from "./src/Screens/ProposalList";
import LaborInvoices from "./src/Screens/LaborInvoices";
import LaborInvoiceDetails from "./src/Screens/LaborInvoiceDetails";
import ProfileLaborContractor from "./src/Screens/ProfileLaborContractor";
import ProposalAppliedLabor from "./src/Screens/ProposalAppliedLabor";
import JobApplicantJobList from "./src/Screens/JobApplicantJobList";
import JobApplicantLaborConList from "./src/Screens/JobApplicantLaborConList";
import JobApplicantHireWorker from "./src/Screens/JobApplicantHireWorker";
import WorkerSchedule from "./src/Screens/WorkerSchedule";
import Worker from "./src/Screens/Worker";
import WorkerTimeSheet from "./src/Screens/WorkerTimeSheet";
import WorkerClockTime from "./src/Screens/WorkerClockTime";
import WorkerProfile from "./src/Screens/WorkerProfile";
import StartProject from "./src/Screens/StartProject";
import ChatRoomScreen from "./src/lib/ex-screen-chat-chatroom";
import SelectedInvoicesGC from "./src/Screens/SelectedInvoicesGC";
import AprroveRejectInvoice from "./src/Screens/AprroveRejectInvoice";
import TimeSheetOrInvoice from "./src/Screens/TimesheetOrInvoice";
import SelectedInvoice from "./src/Screens/SelectJobForInvoice";
import ChooseTimeSheetJobs from "./src/Screens/ChooseTimeSheetJobs";
import ApproveTimeSheetJobs from "./src/Screens/ApproveTimeSheetJobs";
import HistoryGCJobListAllType from "./src/Screens/HistoryGCJobListAllType";
import HistoryGCDetails from "./src/Screens/HistoryGCDetails";
import HistoryLCJobListAllType from "./src/Screens/HistoryLCJobListAllType";
import HistoryLCDetails from "./src/Screens/HistoryLCDetails";

export const RootNavigator = () => {

  var titleMarginLeft = 0
  if(Platform.OS === 'android'){
    titleMarginLeft = -40
  }
  return createStackNavigator(
    {
      Splash: {
        screen: Splash,
        navigationOptions: {
          header: null
        }
      },
      StartProject: {
        screen: StartProject,
        navigationOptions: {
          header: null
        }
      },
      Home: {
        screen: Home,
        navigationOptions: {
          header: null
        }
      },
      Otp: {
        screen: Otp,
        navigationOptions: {
          header: null
        }
      },
      AddJobs: {
        screen: AddJobs,
        navigationOptions: {
          header: null
        }
      },SelectContractorForJob: {
        screen: SelectContractorForJob,
        navigationOptions: {
          header: null
        }
      },
      ProposalAppliedLabor: {
        screen: ProposalAppliedLabor,
        navigationOptions: {
          header: null
        }
      },
      Verification: {
        screen: Verification,
        navigationOptions: {
          header: null
        }
      },
      Worker: {
        screen: Worker,
        navigationOptions: {
          header: null
        }
      },
      VerificationCode: {
        screen: VerificationCode,
        navigationOptions: {
          header: null
        }
      },
      People: {
        screen: People,
        navigationOptions: {
          header: null
        }
      },
      ProjectSelection: {
        screen: ProjectSelection,
        navigationOptions: {
          header: null
        }
      },
      GeneralContractor: {
        screen: GeneralContractor,
        navigationOptions: {
          header: null
        }
      },

      LaborContractor: {
        screen: LaborContractor,
        navigationOptions: {
          header: null
        }
      },
      LaborJobList: {
        screen: LaborJobList,
        navigationOptions: {
          header: null
        }
      },
      ModalAddProjects: {
        screen: ModalAddProjects,
        navigationOptions: {
          header: null
        }
      },
      WorkerList: {
        screen: WorkerListScreen,
        navigationOptions: {
          header: null
        }
      },
    
      ChatStart: {
        screen: ChatRoomScreen,
        navigationOptions: {
          title: "Messages",

          headerStyle: {
            marginTop: 24,
            backgroundColor: colors.colorGradient
          },
          headerTintColor: "#fff",
          

          
          headerTitleStyle: {
            fontWeight: "600",
            fontSize: 20,
            marginTop: 5,
            flex: 1,
            justifyContent: "center",
            textAlign: "center",
            alignSelf: "center",
            color: colors.white,
            marginLeft:titleMarginLeft
          }
        }
      },


      MainTabNavigatorGC: {
        screen: drawerTabGC,
        navigationOptions: {
          header: null
        }
      },
      MainTabNavigator: {
        screen: drawerTabWorker,
        navigationOptions: {
          header: null
        }
      },
      HistoryGCStack: {
        screen: HistoryStackGC,
        navigationOptions: {
          header: null
        }
      },
      HistoryLCStack: {
        screen: HistoryStackLC,
        navigationOptions: {
          header: null
        }
      },
      MainTabNavigatorLabor: {
        screen: drawerTabLC,
        navigationOptions: { header: null }
      }
    }
  );
};

// const JobsStack = createStackNavigator({
//   JobsList: {
//     screen: JOBS,
//     navigationOptions: {
//       header: null
//     }
//   },
//   AddJobs: {
//     screen: AddJobs,
//     navigationOptions: {
//       header: null
//     }
//   }
// });

const HistoryStackGC = createStackNavigator({
  HistoryGCJobListAllType: {
    screen: HistoryGCJobListAllType,
    navigationOptions: {
      header: null
    }
  },
  HistoryGCDetails: {
    screen: HistoryGCDetails,
    navigationOptions: {
      header: null
    }
  }
});

const HistoryStackLC = createStackNavigator({
  HistoryLCJobListAllType: {
    screen: HistoryLCJobListAllType,
    navigationOptions: {
      header: null
    }
  },
  LaborInvoices: {
    screen: LaborInvoices,
    navigationOptions: {
      header: null
    }
  },
  LaborInvoiceDetails: {
    screen: LaborInvoiceDetails,
    navigationOptions: {
      header: null
    }
  },
  HistoryLCDetails: {
    screen: HistoryLCDetails,
    navigationOptions: {
      header: null
    }
  }
});

const ProfileStack = createStackNavigator({

  PROFILE: {
    screen: PROFILE,
    navigationOptions: {
      header: null
    }
  },
  GeneralContractor: {
    screen: GeneralContractor,
    navigationOptions: {
      header: null
    }
  },
})

const JobsApplicantStack = createStackNavigator({
  JobsApplicantList: {
    screen: JobApplicantJobList,
    navigationOptions: {
      header: null
    }
  },
  AddJobs: {
    screen: AddJobs,
    navigationOptions: {
      header: null
    }
  },
  SelectContractorForJob: {
    screen: SelectContractorForJob,
    navigationOptions: {
      header: null
    }
  },
  JobApplicantLaborConList: {
    screen: JobApplicantLaborConList,
    navigationOptions: {
      header: null
    }
  },
  JobApplicantHireWorker: {
    screen: JobApplicantHireWorker,
    navigationOptions: {
      header: null
    }
  },
  chatGC: {
    screen: ChatRoomScreen,
    navigationOptions: {
      header: null
    }
  }
});

const ProposalStack = createStackNavigator({
  ProposalInterestedAndApplied: {
    screen: PROPOSAL,
    navigationOptions: {
      header: null
    }
  },
  ProposalAppliedLaborList: {
    screen: ProposalAppliedLabor,
    navigationOptions: {
      header: null
    }
  }
});

const LCINvoiceStack = createStackNavigator({
  LaborInvoices: {
    screen: LaborInvoices,
    navigationOptions: {
      header: null
    }
  },
  LaborInvoiceDetails: {
    screen: LaborInvoiceDetails,
    navigationOptions: {
      header: null
    }
  }
});
const LCProfileStack = createStackNavigator({
  LCProfile: {
    screen: ProfileLaborContractor,
    navigationOptions: {
      header: null
    }
  },
  LCLaborList: {
    screen: WorkerListScreen,
    navigationOptions: {
      header: null
    }
  },
  LaborContractor: {
    screen: LaborContractor,
    navigationOptions: {
      header: null
    }
  },
});

const Dashboard = createStackNavigator({
  TimeSheetOrInvoice: {
    screen: TimeSheetOrInvoice,
    navigationOptions: {
      header: null
    }
  },
  ChooseTimeSheetJobs: {
    screen: ChooseTimeSheetJobs,
    navigationOptions: {
      header: null
    }
  },
  ApproveTimeSheetJobs: {
    screen: ApproveTimeSheetJobs,
    navigationOptions: {
      header: null
    }
  },
  SelectedInvoice: {
    screen: SelectedInvoice,
    navigationOptions: {
      header: null
    }
  },
  SelectedInvoicesGC: {
    screen: SelectedInvoicesGC,
    navigationOptions: {
      header: null
    }
  },
  AprroveRejectInvoice: {
    screen: AprroveRejectInvoice,
    navigationOptions: {
      header: null
    }
  }
});

const WorkerProfileStack = createStackNavigator({

  WorkerProfile: {
    screen: WorkerProfile,
    navigationOptions: {
      header: null
    }
  },
  Worker: {
    screen: Worker,
    navigationOptions: {
      header: null
    }
  },
})

const TabBarWorker = new createBottomTabNavigator(
  {
    WorkerSchedule: { screen: WorkerSchedule },
    WorkerTimeSheet: { screen: WorkerTimeSheet },
    WorkerClockTime: { screen: WorkerClockTime },
    WorkerProfile: { screen: WorkerProfileStack }
    // WorkerProfile: { screen: WorkerProfile }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "WorkerSchedule") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/schedule.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "WorkerTimeSheet") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/clock-time.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "WorkerClockTime") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/clock-time.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "WorkerProfile") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/profile.png")}
              resizeMode="contain"
            />
          );
        }
      }
    }),
    tabBarOptions: {
      activeTintColor: colors.colorGradient,
      inactiveTintColor: "#D9D6D8",
      showLabel: true,
      lazyLoad: true,
      style: {
        backgroundColor: '#F0F0F0',
        // backgroundColor: colors.colorGradient,
        borderTopWidth: 0,
        height: 55,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: -2 },
        shadowOpacity: 0.05,
        shadowRadius: 15
      }
    }
  }
);

const TabBarGC = new createBottomTabNavigator(
  {
    Home: { screen: Dashboard },
    // MyJobs: { screen: JobsStack },
    Jobs: { screen: JobsApplicantStack },
    // PROFILE: { screen: PROFILE }
    PROFILE: { screen: ProfileStack }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === "Home") {
          return (

            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/praposal.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "MyJobs") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/jobs-select.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "Jobs") {
          return (


            <Image
              style={{ tintColor: tintColor }}

              source={require("./src/assets/profile.png")}
              resizeMode="stretch"
            />


          );
        } else if (routeName === "PROFILE") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/profile.png")}
              resizeMode="contain"
            />
          );
        }
      }
    }),
    tabBarOptions: {
      activeTintColor: colors.colorGradient,
      inactiveTintColor: "#D9D6D8",
      showLabel: true,
      lazyLoad: true,
      style: {
        backgroundColor: '#F0F0F0',
        borderTopWidth: 0,
        height: 55,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: -2 },
        shadowOpacity: 0.05,
        shadowRadius: 15
      }
    }
  }
);

const TabBarLaborContractor = new createBottomTabNavigator(
  {
    PROPOSAL: { screen: ProposalStack },
    JOBS: { screen: LaborJobList },
    INVOICE: { screen: LCINvoiceStack },
    PROFILE: { screen: LCProfileStack }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === "PROPOSAL") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/praposal.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "JOBS") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/jobs-select.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "INVOICE") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/invoce-nav.png")}
              resizeMode="contain"
            />
          );
        } else if (routeName === "PROFILE") {
          return (
            <Image
              style={{ tintColor: tintColor }}
              source={require("./src/assets/profile.png")}
              resizeMode="contain"
            />
          );
        }
      }
    }),
    tabBarOptions: {

      activeTintColor: colors.colorGradient,
      inactiveTintColor: "#D9D6D8",
      showLabel: true,
      lazyLoad: true,
      style: {
        // backgroundColor: colors.colorGradient,
        backgroundColor: '#F0F0F0',
        borderTopWidth: 0,
        height: 55,
        shadowColor: "#0000",
        shadowOffset: { width: 0, height: -2 },
        shadowOpacity: 0.05,
        shadowRadius: 15
      }
    }
  }
);

const drawerTabGC = createDrawerNavigator(
  {
    screen1: {
      screen: TabBarGC
    }
  },
  {
    contentComponent: SlideMenu,
    drawerWidth: 300
  }
);

const drawerTabLC = createDrawerNavigator(
  {
    screen1: {
      screen: TabBarLaborContractor
    }
  },
  {
    contentComponent: SlideMenuLC,
    drawerWidth: 300
  }
);
const drawerTabWorker = createDrawerNavigator(
  {
    screen1: {
      screen: TabBarWorker
    }
  },
  {
    contentComponent: SlideMenuWorker,
    drawerWidth: 300
  }
);