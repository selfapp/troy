import {
  AsyncStorage,
  Platform
} from 'react-native';
// var baseURL = 'http://3.219.179.15';//Local

var baseURL = 'http://3.219.179.15'; // prod
// var baseURL = 'http://192.168.0.130:3000'; // dev
var API = {
  async getToken() {
    return await AsyncStorage.getItem('access_token');
  },
  async getUuid() {
    return await AsyncStorage.getItem('uuid');
  },
  async getHeaders() {
    let access_token = await this.getToken();
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + (access_token ? access_token : null)
    }
  },
  async request(url, method, body, header) {
    let headers = await this.getHeaders();
    return fetch(baseURL + url, {
      method: method,
      headers,
      body: body === null ? null : JSON.stringify(body)
    });
  },

  getRequest(url, method, header) {
    return AsyncStorage.getItem('access_token').then((accesstoken) => {
      // let userObject = JSON.parse(data);
      return fetch(baseURL + url, {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Token token=' + (accesstoken ? accesstoken : null)
        },
      })
    });
  },

  async savePushToken(token) {

    console.log("save push token ")
    console.log(token)
    let headers = await this.getHeaders();
    let uuid = await this.getUuid();
    let body;
    if (Platform.OS === 'ios') {
      body = {
        user: {
          apns_token: token
        }
      };
    } else {
      body = {
        user: {
          gcm_token: token
        }
      };
    }
    body = JSON.stringify(body);

    return fetch(`${baseURL}/users/${uuid}.json`, {
      method: 'PATCH',
      headers,
      body
    });
  },
  getInvoicesLc() {
    return this.request(`/lc/invoices`, 'GET');
  },
  getInvoiceDetailsLc(uuid) {
    return this.request(`/lc/invoices/${uuid}`, 'GET');
  }
};

module.exports = {
  API: API,
  baseURL
};
