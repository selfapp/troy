import React, {Component} from "react";
import {
  Dimensions,
  Image,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Alert,
  View,
  TextInput,
  Keyboard,
  Platform,
  KeyboardAvoidingView
} from "react-native";
import Moment from "moment";
import CheckBox from "react-native-checkbox";
import MyActivityIndicator from '../Components/activity_indicator';
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import colors from "../styles/colors";
import {baseURL} from '../../api';
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

const height = Dimensions.get("window").height;

export default class ProposalAppliedLabor extends Component {
  constructor() {
    super();

    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      checked: true,
      auth_token: "",
      jobs_uuid: "",
      projectList: [],
      workersAddedList: [],
      project_uuid: "",
      display: "none",
      status: "",
      price:'',
      loader:false,
      noOfWorkers:0
    };
  }

  handleClick = () => {
    {
      this.props.navigation.navigate("PROPOSAL");
    }
  };

  componentDidMount() {
    this.setState({noOfWorkers:this.props.navigation.state.params.no_of_worker})
    this._retrieveData();
  }

  changeCheckState(workers_uuid, ischecked) {

    console.log("ischecked ", ischecked)
    if (ischecked === true) {
      if(this.state.workersAddedList.indexOf(workers_uuid) === -1) {
        this.state.workersAddedList.push(workers_uuid);
      }
      // this.state.workersAddedList.push(workers_uuid);
      //this.setState({ projectList: this.state.projectList });
    }else{
      const filteredItems = this.state.workersAddedList.filter(item => item !== workers_uuid)
      this.setState({workersAddedList:filteredItems})
    }
  }

  getListOfWorker() {
    //workersAddedList.
    // this.state.workersAddedList.push(workers_uuid);
    // console.log("workers uuid is " + workers_uuid);

    var myobject;
    for (let i = 0; i < this.state.workersAddedList; i++) {
    }

    return myobject;
  }

  apiCallMarkWokerAgainstJobTest() {

    var myObject2 = {
      job_worker: {
        job_application_id: this.state.jobs_uuid,
        workers: this.state.workersAddedList
      }
    };
  }

  apiCallMarkWokerAgainstJob() {

    console.log("workers list are")
    console.log(this.state.workersAddedList)

    if(this.state.workersAddedList.length != this.state.noOfWorkers){
      const alertMessage = "Please select" + " "+ this.state.noOfWorkers + " " + "worker for this job."
      alert(alertMessage)
    }
   else if(this.state.price.length === 0 || this.state.price === null){

      alert("Please enter price.")
    }else {
      this.setState({loader:true})
      var myObject = {
        job_worker: {
          job_application_id: this.state.jobs_uuid,
          workers: this.state.workersAddedList,
          price: this.state.price
        }
      };
  
      fetch(`${baseURL}/job-workers/`, {
        method: "POST",
  
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        },
        body: JSON.stringify(myObject)
      })
     
        .then(response => response.json())
        .then(responseJson => {
          // this.setState({ ProjectName: "" });
          this.setState({loader:false})
          console.log("response json job worker ")
          console.log(responseJson)
          console.log(responseJson.errors)
          console.log(responseJson.errors.length)
          if(responseJson.errors) {
            Alert.alert(
              "",
              "Succesfully Applied to Job.",
              [
                {
                  text: "OK",
                  onPress: () => this.props.navigation.navigate("PROPOSAL")
                }
              ],
              {cancelable: false}
            );
          } else {
            alert(responseJson.error)
          }
         
  
          
        })
        .catch(e => {
          this.setState({loader:false})
        });  
    }
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const jobs_uuid = await AsyncStorage.getItem("uuid_jobs");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
        this.setState({jobs_uuid: jobs_uuid});

      }

      fetch(`${baseURL}/jobs/${jobs_uuid}/workers`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          console.log("rsposne jsfdlk jsfd")
          console.log(responseJson)
          this.setState({
            projectList: responseJson.workers
          });
          if (this.state.projectList.length == 0) {

            this.setState({
              status: "No worker found, Add some worker at your profile.",
              display: "flex"
            });
          } else {
            this.setState({display: "none"});
          }
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {

    Moment.locale("en");
    return (
      <BackgroundNtScroll>
              {/* <KeyboardAvoidingView behavior="padding" enabled> */}

        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%", alignItems: 'center',
            marginTop: 40,
          }}
        >
          <TouchableOpacity
            style={{width: 30}}
            onPress={this.handleClick.bind(this)}
          >
            <Image
              style={{alignSelf: "flex-start"}}
              source={require("../../src/assets/backnew.png")}
            />
          </TouchableOpacity>

          <View style={{flex: 1, marginTop: 20}}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Worker List
            </Text>
          </View>
        </View>
        <View
          style={{
            marginTop: "1%"
          }}
        >
          <Text style={{
            marginHorizontal:'10%',
            marginTop:10,
            fontSize:16
          }}>
            {`Please select ${this.state.noOfWorkers} worker for this job.`}
          </Text>
          {this.state.loader ? <MyActivityIndicator /> : null}
          <TouchableWithoutFeedback
         style={{}}
          onPress={()=> Keyboard.dismiss()}
          >
          <KeyboardAvoidingView
          keyboardVerticalOffset={Platform.select({ ios: 150, android: 160 })}
          // contentContainerStyle={{flex: 1}}
          behavior='position'
          enabled
        >
          <View>

          <View
            style={{
              borderRadius: 10,
              marginLeft: "10%",
              marginRight: "10%",
              marginTop: "5%",
              width: "80%",
              height: height / 2,
              backgroundColor: colors.white
            }}
          >
           
            <View
              style={{
                flex: 1,
                display: this.state.display,
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  padding: 10,
                  color: "black",
                  fontSize: 14
                }}
              >
                {this.state.status}
              </Text>
            </View>
           
            <FlatList
              style={{marginTop: 1, height: "7.5%"}}
              data={this.state.projectList}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  style={{
                    borderRadius: 10,
                    marginBottom: "4%",
                    width: "100%",
                    borderColor: "red",
                    flexDirection: "column",
                    alignSelf: "center",
                    backgroundColor: colors.gray01
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      height: 40,

                      margin: 10,
                      alignItems: "center"
                    }}
                  >
                    <CheckBox
                      checkedImage={require("../assets/check.png")}
                      label=""
                      // checked={item.assigned}
                      uncheckedImage={require("../assets/uncheck.png")}
                      onChange={checked => this.changeCheckState(item.uuid, checked)}
                    />

                    <Text
                      style={{
                        flex: 2,
                        marginLeft: 12,
                        fontSize: 16,
                        alignContent: "center",
                        fontWeight: "bold"
                      }}
                    >
                      {item.first_name + " " + item.last_name}
                    </Text>
                    <Text style={{flex: 1.4}}>
                      {Moment(item.start_date).format("D MMM YYYY")}
                    </Text>
                  </View>
                  <View
                    style={{
                      height: 1,
                      marginTop: 7,
                      backgroundColor: colors.gray01
                    }}
                  />
                </TouchableOpacity>
              )}
            />

          </View>

      <Text style={{ fontSize: 18,
                     // alignSelf: 'flex-start',
                     height:20,
                     marginTop:10,marginLeft:'10%',
                    }}>Enter Price($)
        </Text>

          <TextInput
              style={{
                height: 40,
                fontSize: 18,
                borderColor: colors.gray05,
                borderWidth: 1,
                alignSelf: "center",
                marginTop:5,
                width: "80%",
                borderRadius: 7,
                paddingLeft:20
              }}
              underlineColorAndroid='transparent'
              //placeholder="Enter Price"
              //placeholderTextColor="#FF242424"
              autoCapitalize="none"
              keyboardType='decimal-pad'
              value={this.state.price}
              onChangeText={value => this.setState({price: value})}
            />
        
        </View>
        </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
        <View
          style={{
            height: 40,
            justifyContent: "center",
            backgroundColor: colors.colorYogi,
            width: "40%",
            borderRadius: 20,
            alignSelf: "center",
            marginTop: 15
          }}
        >
          
          <TouchableOpacity
            onPress={() => {
              //this.props.navigation.navigate("PROPOSAL");
              this.apiCallMarkWokerAgainstJob();
            }}
          >
            <Text
              style={{
                color: "white",
                alignSelf: "center",
                justifyContent: "center",
                fontSize: 14
              }}
            >
              Apply
            </Text>
          </TouchableOpacity>
        </View>
       
        </View>
       
        {/* <KeyboardAvoidingView
          keyboardVerticalOffset={Platform.select({ ios: 10, android: -80 })}
          contentContainerStyle={{flex: 1}}
          behavior='position'
          enabled
        > */}
       
        {/* <TextInput
              style={{
                height: 40,
                fontSize: 18,
                borderColor: colors.gray05,
                borderWidth: 1,
                alignSelf: "center",
                marginTop:15,
                width: "80%",
                borderRadius: 7,
                paddingLeft:20
              }}
              underlineColorAndroid='transparent'
              placeholder="Enter Price"
              placeholderTextColor="#FF242424"
              autoCapitalize="none"
              keyboardType='decimal-pad'
              value={this.state.price}
              onChangeText={value => this.setState({price: value})}
            />
        <View
          style={{
            height: 40,
            justifyContent: "center",
            backgroundColor: colors.colorYogi,
            width: "40%",
            borderRadius: 20,
            alignSelf: "center",
            marginTop: 15
          }}
        >
          
          <TouchableOpacity
            onPress={() => {
              //this.props.navigation.navigate("PROPOSAL");
              this.apiCallMarkWokerAgainstJob();
            }}
          >
            <Text
              style={{
                color: "white",
                alignSelf: "center",
                justifyContent: "center",
                fontSize: 14
              }}
            >
              Apply
            </Text>
          </TouchableOpacity>
        </View> */}
        {/* </KeyboardAvoidingView> */}
        {/* </KeyboardAvoidingView> */}
      </BackgroundNtScroll>
     
    );
  }
}
