import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import Moment from "moment";
import {DrawerActions} from "react-navigation";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import MyActivityIndicator from '../Components/activity_indicator';
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import colors from "../styles/colors";
import Button from "../Components/button";
import {baseURL} from '../../api';

export default class WorkerClockTime extends Component {
  constructor() {
    super();
    this.state = {
      FirstName: "",
      projectListRaw: [],
      LastName: "",
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "State",
      zipcode: "",
      textvalue: "",
      uuid: "",
      auth_token: "",
      avatarSource: "",
      currentDate: "",
      currentTime: "",
      disableClockIn: false,
      disableClockOut: false,
      clockerCheckInTimer: "",
     // color: "Green",
      sizeFont: 33,
      lat:null,
      lng: null,
      loader:false
    };
  }

  getCurrentDate() {
    console.log("current time is -" + Moment(Moment()).format("D MMM YYYY"));

    // Moment(Moment()).format("D MMM YYYY");
    // ("YYYY-MM-DD hh:mm:ss");
    const date = Moment(Moment()).format("D MMM YYYY");

    this.setState({currentDate: date});
    //("YYYY-MM-DD hh:mm:ss"))
  }

  stopTimer() {
    clearInterval(this.timer);
  }

  getRunningClockTime(dateString) {
    console.log("date string is", dateString);
    var moment = require("moment");

    // var dateString = Moment(date2).format("L");
    if (dateString != null) {
      // const ClockinTime = Moment(dateString).format("YYYY-MM-DD YYYY hh:mm a");

      var now = moment(new Date()); //todays date
      var duration = moment.duration(now.diff(dateString));
      var hors = duration.hours();
      var min = duration.minutes();
      var sec = duration.seconds();
      console.log("Duration is ", moment.utc(duration).format());

      this.setState({
        currentTime: hors + ":h " + min + ":m " + sec + ":s"
      });
      this.setState({
        clockerCheckInTimer: dateString
      });
      console.log("running clock is", hors + ":" + min + ":" + sec);

      var date = moment(now, "YYYY-MM-DD hh:mm a")
        .add(hors, "hours")
        .add(sec, "seconds")
        .add(min, "minutes")
        .format("LTS");
      console.log("finaal time after adding hours", date);
      this.startTimer();
    }
  }

  tick() {
    var moment = require("moment");

    var now = moment(new Date()); //todays date
    var duration = moment.duration(now.diff(this.state.clockerCheckInTimer));
    var hors = duration.hours();
    var min = duration.minutes();
    var sec = duration.seconds();
    console.log("Duration is ", moment.utc(duration).format());

    this.setState({
      currentTime: hors + ":h " + min + ":m " + sec + ":s"
    });

    console.log("running clock is", hors + ":" + min + ":" + sec);
    if (sec % 2 == 0) {
      this.setState({
        color: "yellow",
        sizeFont: 38
      });
    } else {
      this.setState({
        color: "white",
        sizeFont: 33
      });
    }
  }

  startTimer() {
    this.timer = setInterval(this.tick.bind(this), 1000);
  }

  alertForClockInClockOut(actionPerform) {
    if(actionPerform === "clockIn"){
      Alert.alert(
        "",
        "Clocked in Successfully.",
        [
          {
            text: "OK"
          }
        ],
        {cancelable: false}
      );
    }else if(actionPerform === "clockOut"){
      Alert.alert(
        "",
        "Clocked out Successfully.",
        [
          {
            text: "OK"
          }
        ],
        {cancelable: false}
      );
    }
  }

  _retrieveStatusData = async (actionPerform) => {
    try {
      this.setState({loader:true})
      const access_token = await AsyncStorage.getItem("access_token");

      this.setState({auth_token: access_token});

      console.log("uuid" + this.state.uuid);
      console.log("access_token" + this.state.auth_token);

      fetch(`${baseURL}/worker/timecards/status`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          console.log("response for time cards .....")
          console.log(responseJson);
          console.log(responseJson.timecards);

          if (responseJson.status == false && responseJson.job == null) {
            this.setState({disableClockIn: false});
            this.setState({disableClockOut: false});
            console.log("no job assigned");
            Alert.alert(
              "",
              "No jobs found Today.",
              [
                {
                  text: "OK"
                  //onPress: () =>
                  //this.props.navigation.navigate("WorkerClockTime")
                }
              ],
              {cancelable: false}
            );
          } else if (
            responseJson.status == false &&
            responseJson.job !== null &&
            responseJson.timecards.length == 0
          ) {
            // this.setState({
            //   projectListRaw: responseJson.timecards
            // });
            this.setState({disableClockIn: false});
            this.setState({disableClockOut: true});
            console.log("first time job is assignedyyy");

            //Rajeev
            this.alertForClockInClockOut(actionPerform)
              Alert.alert(
                "",
                "You have assigned a job.",
                [
                  {
                    text: "OK"
                    //onPress: () =>
                    // this.props.navigation.navigate("WorkerClockTime")
                  }
                ],
                {cancelable: false}
              );
            
           
          } else if (
            responseJson.status == true &&
            responseJson.timecards !== null
          ) {
            //Rajeev
            this.alertForClockInClockOut(actionPerform)
            this.getRunningClockTime(responseJson.in);
            console.log("job in once not out till now");
            this.setState({
              projectListRaw: responseJson.timecards
            });
            this.setState({disableClockIn: true});
            this.setState({disableClockOut: false});
          } else if (responseJson.status == true) {
            //Rajeev
            this.alertForClockInClockOut(actionPerform)
            this.setState({disableClockIn: true});
            this.setState({disableClockOut: false});
          } else if (
            responseJson.status == false &&
            responseJson.job !== null &&
            responseJson.timecards !== null
          ) {
            //Rajeev
            this.alertForClockInClockOut(actionPerform)
            this.setState({
              projectListRaw: responseJson.timecards
            });
            this.setState({disableClockIn: false});
            this.setState({disableClockOut: true});
            console.log("first time job is assignedxx");
          }
        })

        .catch(e => {
          this.setState({loader:false})
          console.log("called error " + e);
        });
    } catch (error) {
      this.setState({loader:false})
      // Error retrieving data
      console.log("called error " + error);
    }
  };

  getCurrentTime() {
    console.log("current time is -" + Moment(Moment()).format("h:mm:ss a"));
    const time = Moment(Moment()).format("h:mm A");
    this.setState({currentTime: time});
  }

  _retrieveData1 = async () => {
    try {
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!
        console.log("uuid" + uuid);
        console.log("access_token" + access_token);

        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        console.log("uuid" + this.state.uuid);
        console.log("access_token" + this.state.access_token);
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  validateFields() {
    console.log("validate fields fun called");
    console.log(
      "first name value" +
      this.state.FirstName +
      this.state.EmailId +
      this.state.LastName +
      this.state.City +
      this.state.State +
      this.state.zipcode +
      this.state.avatarSource
    );
    if (
      this.state.FirstName == "" ||
      this.state.LastName == "" ||
      this.state.EmailId == "" ||
      this.state.StreetAddress == "" ||
      this.state.City == "" ||
      this.state.State == "State" ||
      this.state.zipcode == "" ||
      this.state.avatarSource == ""
    ) {
      Alert.alert("", "License image and fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  componentDidMount() {
    this.getLocation();
    this._retrieveData();
    this.getCurrentDate();
    this.getCurrentTime();
    this._retrieveStatusData("");
  }

  getLocation(){
    navigator.geolocation.getCurrentPosition(
        (position) => {
            this.setState({
                lat: position.coords.latitude,
                lng: position.coords.longitude
            });
        },
        (error) => {
            alert('Please allow access to location services.')
            // alert(error.message)
    },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
    );
}

  fetchImageFromGallery() {
    var ImagePicker = require("react-native-image-picker");

    // More info on all the options is below in the README...just some common use cases shown here
    var options = {
      title: "Select Avatar",
      customButtons: [{name: "fb", title: "Choose Photo from Facebook"}],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info below in README)
     */
    ImagePicker.showImagePicker(response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        let source = {uri: response.uri};

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: response.data
        });
      }
    });
  }

  validate = text => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      // this.setState({ email: text });
      return true;
    } else {
      //this.setState({ email: text });
      console.log("Email is Correct");
      return true;
    }
  };

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!
        console.log("uuid" + uuid);
        console.log("access_token" + access_token);

        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        console.log("uuid" + this.state.uuid);
        console.log("access_token" + this.state.access_token);

        fetch(`${baseURL}/worker/users/${this.state.uuid}`, {
          method: "GET",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            //console.log("state value" + responseJson.user.file);
            this.setState({FirstName: responseJson.user.first_name});
            this.setState({LastName: responseJson.user.last_name});

            this.setState({EmailId: responseJson.user.email});
            this.setState({StreetAddress: responseJson.user.street_address});
            this.setState({City: responseJson.user.city});
            this.setState({State: responseJson.user.state});
            this.setState({zipcode: responseJson.user.zip_code});
            this.setState({role: responseJson.user.role});
            //this.setState({ avatarSource: responseJson.user.file });

            if (responseJson.user.state == null) {
              this.setState({State: "State"});
            }

            console.log("first name response is");
            console.log(
              "first name response is" + responseJson.user.first_name
            );
          })
          .catch(e => {
            this.setState({loader:false})
            console.log("called error " + e);
          });
      }
    } catch (error) {
      // Error retrieving data
      console.log("called error " + e);
    }
  };

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  _ClockIn = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null && this.state.lat !== null ) {
        // We have data!!
        console.log("uuid" + uuid);
        console.log("access_token" + access_token);

        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        console.log("uuid" + this.state.uuid);
        console.log("access_token" + this.state.access_token);

        fetch(`${baseURL}/worker/timecards/in`, {
          method: "POST",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          },
          body:JSON.stringify({lat_in:this.state.lat, lng_in: this.state.lng})
        })
          .then(response => response.json())
          .then(responseJson => {
            console.log("clock in resposne ...")
            console.log(responseJson)
            this.setState({loader:false})
            // Alert.alert(
            //   "",
            //   "Clocked in Successfully.",
            //   [
            //     {
            //       text: "OK"
            //     }
            //   ],
            //   {cancelable: false}
            // );
             this._retrieveStatusData("clockIn");
          })
          .catch(e => {
            this.setState({loader:false})
            console.log("called error " + e);
          });
      }else{
        if(this.state.lat === null){
          alert('Please allow access to location services.')
        }
      }
    } catch (error) {
      // Error retrieving data
      console.log("called error " + e);
    }
  };

  _ClockOut = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null && this.state.lat !== null) {
        // We have data!!
        console.log("uuid" + uuid);
        console.log("access_token" + access_token);

        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        console.log("uuid" + this.state.uuid);
        console.log("access_token" + this.state.access_token);

        fetch(`${baseURL}/worker/timecards/out`, {
          method: "PATCH",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          },
          body:JSON.stringify({lat_out:this.state.lat, lng_out: this.state.lng})
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            console.log(responseJson);
            // Alert.alert(
            //   "",
            //   "Clocked out Successfully.",
            //   [
            //     {
            //       text: "OK"
            //     }
            //   ],
            //   {cancelable: false}
            // );
            this.stopTimer();
             this._retrieveStatusData("clockOut");
          })
          .catch(e => {
            this.setState({loader:false})
            console.log("called error " + e);
          });
      }else{
        if(this.state.lat === null){
          alert('Please allow access to location services.')
        }
      }
    } catch (error) {
      // Error retrieving data
      console.log("called error " + e);
    }
  };

  apiCallUserProfileUpdate = () => {
    if (this.validateFields()) {
      if (this.validate(this.state.EmailId)) {
        console.log("called");
        this.setState({loader:true})
        var myObject = {
          user: {
            first_name: this.state.FirstName,
            last_name: this.state.LastName,
            email: this.state.EmailId,
            street_address: this.state.StreetAddress,
            city: this.state.City,
            state: this.state.State,
            zip_code: this.state.zipcode,
            file: this.state.avatarSource,
            role: "LC"
          }
        };

        //api call for profile update

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "PATCH",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          },
          body: JSON.stringify(myObject)
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            console.log(responseJson);
            Alert.alert(
              "",
              "Profile updated Successfully.",
              [
                {
                  text: "OK",
                  onPress: () => this.props.navigation.navigate("PROPOSAL")
                }
              ],
              {cancelable: false}
            );
          })
          .catch(e => {
            this.setState({loader:false})
            console.log("called error " + e);
          });

        //
      } else {
        Alert.alert("", "Please enter correct Email address.");
      }
    } else {
    }
  };

  handleClick = () => {
    //  this.props.navigation.navigate("People");
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  };
  handlePeople = () => {
  };

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View
          style={{flex: 1, width: "100%", backgroundColor: colors.gray01}}
        >
          {this.state.loader ? <MyActivityIndicator /> : null}
          <Image
            style={{width: "100%"}}
            source={require("../../src/assets/time_clock.png")}
          />
          <View
            style={{
              height: "15%",
              width: "100%",
              position: "absolute",
              zIndex: 100,

              flexDirection: "row",
              marginRight: "5%",
              marginTop: "9%"
            }}
          >
            <TouchableOpacity
              style={{marginTop: 20, marginLeft: 10}}
              onPress={this.handleClick.bind(this)}
            >
              {
                <Image
                  style={{}}
                  source={require("../../src/assets/h-menu.png")}
                />
              }
            </TouchableOpacity>

            {/* <View style={{ flex: 1, marginTop: 20, width: '100%', alignItems: 'center' }}>
              <Text
                style={{
                  flex: 1,
                  color: 'black',
                  fontSize: 24,
                  alignSelf: "center",
                  fontWeight: "400"
                }}
              >
                Time Clock
              </Text>
            </View> */}
          </View>

          <View
            style={{
              height: "5%",
              width: "100%",
              position: "absolute",
              flexDirection: "row",
              marginTop: "9%"
            }}
          >
            <View
              style={{
                flex: 1,
                marginTop: "30%",
                alignSelf: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  marginTop: -30,
                  color: 'black',
                  fontSize: 24,
                  fontWeight: "400",
                  textAlign: 'center'
                }}
              >
                Time Clock
              </Text>
              <Text
                style={{
                  flex: 1,
                  textAlignVertical: "bottom",
                  color: colors.gray01,
                  fontSize: 18,
                  fontWeight: "bold"
                }}
              >
                {this.state.currentDate}
              </Text>
            </View>
          </View>

          <View
            style={{
              height: "10%",
              width: "100%",
              position: "absolute",
              flexDirection: "row",
              marginTop: "9%"
            }}
          >
            <View
              style={{
                flex: 1,
                marginTop: "50%",
                alignSelf: "center",
                alignItems: "center",

              }}
            >
              <Text
                style={{
                  flex: 1,
                  textAlignVertical: "bottom",
                  color: this.state.color,
                  fontSize: this.state.sizeFont,
                  fontWeight: "bold"
                }}
              >
                {this.state.currentTime}
              </Text>
            </View>
          </View>

          <View
            style={{
              alignContent: "center",
              alignItems: "center",
              position: "absolute",
              marginBottom: 30,
              left: "20%",
              top: "46%",
              right: "18%"
            }}
          >
            <Image style={{}} source={require("../../src/assets/Group.png")}/>
          </View>
          <View
            style={{
              marginTop: "1%"
            }}
          >
            <TouchableOpacity
              style={{
                flex: 0.4,
                height: "10%",
                marginTop: "3%"
              }}
              // onPress={this.handleClick.bind(this)}
            />
            <View
              style={{
                borderBottomLeftRadius: 40,
                borderBottomRightRadius: 40,
                width: "100%",
                height: "55%",
                backgroundColor: colors.cloorClockTimeBottom
              }}
            >
              <View
                style={{
                  backgroundColor: colors.gray01,
                  height: "75%",
                  width: "100%",
                  borderBottomLeftRadius: 40,
                  borderBottomRightRadius: 40
                }}
              >
                <View
                  style={{
                    height: "45%"
                  }}
                >
                  <FlatList
                    horizontal
                    style={{
                      marginTop: 1,
                      height: 90
                    }}
                    data={this.state.projectListRaw}
                    horizontal={true}
                    renderItem={({item, index}) => (
                      <View>
                        <View
                          style={{
                            borderRadius: 10,
                            marginLeft: 20,
                            borderColor: "red",
                            height: 70,

                            alignItems: "center",
                            backgroundColor: colors.white,
                            alignSelf: "center"
                          }}
                        >
                          <Text
                            style={{
                              color: "black",

                              justifyContent: "center",
                              alignItems: "center",
                              fontWeight: "bold",
                              paddingLeft: 10,
                              marginTop: 2,
                              textAlignVertical: "top"
                            }}
                          >
                            {item.date}
                          </Text>

                          <View
                            style={{
                              height: "20%",
                              flexDirection: "row",
                              width: "100%"
                            }}
                          >
                            <Text
                              style={{
                                color: "red",
                                flex: 1,

                                fontWeight: "bold",
                                marginLeft: 20,
                                textAlignVertical: "bottom"
                              }}
                            >
                              Time in
                            </Text>
                            <Text
                              style={{
                                color: "red",
                                marginRight: 15,
                                fontWeight: "bold",
                                paddingLeft: 10,
                                marginLeft: 15,
                                textAlignVertical: "bottom"
                              }}
                            >
                              Time Out
                            </Text>
                          </View>
                          <View
                            style={{
                              height: "35%",
                              width: "100%",
                              flexDirection: "row"
                            }}
                          >
                            <Text
                              style={{
                                color: colors.black,
                                margin: 2,
                                flex: 1,
                                justifyContent: "center",
                                alignItems: "center",
                                fontWeight: "bold",
                                paddingLeft: 10,
                                textAlignVertical: "top"
                              }}
                            >
                              {item.in}
                            </Text>
                            <Text
                              style={{
                                color: "black",
                                marginRight: 10,
                                justifyContent: "center",
                                alignItems: "center",
                                fontWeight: "bold",
                                paddingLeft: 10,
                                textAlignVertical: "top"
                              }}
                            >
                              {item.out}
                            </Text>
                          </View>
                        </View>
                      </View>
                    )}
                  />
                </View>
                <View
                  style={{
                    height: "25%",
                    flexDirection: "row",
                    marginTop: 5
                  }}
                >
                  <Button
                    style={{flex: 1}}
                    width={"40%"}
                    disabled={this.state.disableClockIn}
                    buttonName={"Clock in"}
                    onPress={this._ClockIn.bind(this)}
                    backgroundColor={colors.colorSignin}
                    textColor={"white"}
                  />
                  <Button
                    style={{flex: 1}}
                    width={"40%"}
                    disabled={this.state.disableClockOut}
                    buttonName={"Clock out"}
                    onPress={this._ClockOut.bind(this)}
                    backgroundColor={colors.colorSignin}
                    textColor={"white"}
                  />
                </View>
              </View>
            </View>
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  }
});
