import React, {Component} from "react";
import {
  Dimensions,
  Image,
  KeyboardAvoidingView,
  Platform,
  Text,
  Alert,
  View,
  Keyboard
} from "react-native";
import OTPInput from 'react-native-otp';
import {AsyncStorage} from "react-native";
import {NavigationActions, StackActions} from "react-navigation";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import Button from "../Components/button";
import Background from "../Components/Background";
import {API} from '../../api';

const height = Dimensions.get("window").height;

export default class VerificationCode extends Component {
  constructor() {
    super();
    this.state = {
      otp: "",
      phoneNumber: "",
      loader:false,
      // latitude:'',
      // longitude:''
    };
  }

  handleClick = () => {
    this.apiCallSendOtp();
  };

  componentWillMount() {
    // this.fetchCurrentLocation()
    Keyboard.dismiss();
    this._retrieveData();
  }

  fetchCurrentLocation() {
    console.log("fetch current location....");
    this.setState({loader:true})
    navigator.geolocation.getCurrentPosition(
        (position) => {
            console.log("Position latitude is" + position.coords.latitude);
            console.log("Position longitude is" + position.coords.longitude);
          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
          this.setState({loader:false})
        },
        (error) => {
            console.log("login method on call")
        },
        this.setState({loader:false}),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
      );
}

  validateFields() {
    if (this.state.otp === "") {
      Alert.alert("", "Enter otp.");
      return false;
    } else {
      return true;
    }
  }

  _retrieveData = async () => {
    try {
      const PhoneNumber = await AsyncStorage.getItem("phoneNumber");
      this.setState({phoneNumber: PhoneNumber});
    } catch (error) {
      console.log("called error " + e);
    }
  };

  async apiCallSendOtp() {
    if (this.state.otp === "") {
      Alert.alert("", "Enter otp.");
      return;
    } else {

      var myObject = {
        user: {
          country_code: "+91",
          contact_number: this.state.phoneNumber,
          otp: this.state.otp,
          // lat:this.state.latitude ? this.state.latitude : "",
          // lng:this.state.longitude ? this.state.longitude : ""
        }
      };
      console.log("body for otp.....")
      console.log(myObject)
      try {
        this.setState({loader:true})
        let response = await API.request('/verify_otp.json', 'post', myObject, null);
        try {
          response.json().then((responseJson) => {
            console.log("Otp reponse ....")
            console.log(responseJson)
            if (!responseJson.status === false) {
              this.setState({loader:false})
              try {
                AsyncStorage.setItem("uuid", responseJson.user.uuid);
                AsyncStorage.setItem("my_id", responseJson.user.id + "");
                AsyncStorage.setItem(
                  "access_token",
                  responseJson.user.access_token
                );
                if (responseJson.user.role == null) {
                  AsyncStorage.setItem("userType", "not define");
                } else {
                  AsyncStorage.setItem("userType", responseJson.user.role);
                }

              } catch (error) {
                console.log("error while saving async");
              }

              this.setState({otp: ""});
              if (responseJson.user.role === "GC") {
                const resetAction = StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "MainTabNavigatorGC"
                    })
                  ]
                });
                this.props.navigation.dispatch(resetAction);
              } else if (responseJson.user.role === "LC") {
                const resetAction = StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "MainTabNavigatorLabor"
                    })
                  ]
                });
                this.props.navigation.dispatch(resetAction);
              } else if (responseJson.user.role === "WORKER") {
                var resetAction;
                console.log("Email id ===", responseJson.user.email)
                if(responseJson.user.email === null || responseJson.user.email.lenght === 0){
                  resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({routeName: "Worker"})]
                  });
                }else {
                  resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({routeName: "MainTabNavigator"})]
                  });
                }
                this.props.navigation.dispatch(resetAction);
              } else {
                this.props.navigation.navigate("People");
              }
            } else {
              this.setState({loader:false})
              Alert.alert(
                "",
                responseJson.message,
                [
                  {
                    text: "OK"
                  }
                ],
                {cancelable: false}
              );
            }
          });
        } catch (error) {
          this.setState({loader:false})
          console.log(e + "");
          Alert.alert(
            "",
            error + "",
            [
              {
                text: "OK"
              }
            ],
            {cancelable: false}
          );
        }
      } catch (error) {
        Alert.alert(error)
      }

    }
  };


  render() {
    return (
      <Background>
        
        <View>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <KeyboardAvoidingView
            keyboardVerticalOffset={Platform.select({ios: -150, android: -80})}
            contentContainerStyle={{alignItems: "center", flex: 1}}
            behavior="position"
            enabled
          >
            <View style={{flexDirection: "column", height: height / 5}}>
            </View>
            <Image
              style={{alignSelf: "center"}}
              source={require("../../src/assets/logo.png")}
            />
            <View
              style={{
                backgroundColor: "transparent",
                height: 400,
                marginLeft: "5%",
                marginRight: "5%",
                marginTop: "10%",
                width: "90%",
              }}
            >
              <Text style={{textAlign: "center", fontSize: 16}}>
                We have sent a verification code to your registered mobile number. please Enter the OTP below to
                continue.
              </Text>

              <View style={{marginTop: 35, borderBottomColor: colors.green01}}>

                <OTPInput
                  value={this.state.otp}
                  onChange={value => this.setState({otp: value})}
                  tintColor={colors.gray01}
                  offTintColor={colors.gray01}
                  otpLength={4}
                />
                <View style={{
                  marginBottom: 20,
                  height: 20,
                  flexDirection: "row",
                  width: "100%",
                  borderBottomColor: colors.green01
                }}>
                </View>
              </View>

              <Button
                marginTop={"5%"}
                width={"90%"}
                buttonName={"Continue"}
                onPress={this.handleClick.bind(this)}
                backgroundColor={colors.colorYogi}
                textColor={"white"}
              />
            </View>

          </KeyboardAvoidingView>

        </View>
      </Background>
    );
  }
}
