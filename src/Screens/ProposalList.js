
import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import {DrawerActions} from "react-navigation";
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  FlatList,
  Alert,
  Dimensions,
  Platform
} from "react-native";
import MaterialTabs from "react-native-material-tabs";
import Moment from "moment";
import Spinner from "react-native-loading-spinner-overlay";

import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";
import {API, baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class ProposalList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ProjectName: "",
      modalVisible: false,
      projectList: [],
      interestedList: [],
      invitedList: [],
      project_uuid: "",
      FirstName: "",
      LastName: "",
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "State",
      zipcode: "",
      textvalue: "",
      uuid: "",
      auth_token: "",
      avatarSource: "",
      selectedTab: 1,
      date: "",
      myUserId: "",
      spinner: false,
      gcm_token: "",
      profileUpdated: "false",
      display: "none",
      status: "",
      loader:false
    };
  }

  componentWillMount() {
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this.setState({interestedList: []});
      this.setState({invitedList: []});
      this._retrieveData();
    });
  }

  componentDidMount() {
    Moment.locale("en");
    this.setPushConfig();
  }

  setPushConfig() {
    let _this = this;
    var PushNotification = require("react-native-push-notification");


    PushNotification.configure({
      onRegister: function (token) {
        API.savePushToken(token.token);
      },
      onNotification: function (notification) {
        console.log("notification received proposal list ....")
        console.log(notification)

        if (
          notification.foreground === true &&
          notification.userInteraction === false
        ){
            if(Platform.OS === 'ios'){
                Alert.alert("STAFF UP", notification.data.message);
            }
          }
       
        if (
          notification.foreground === true &&
          notification.userInteraction === true
        ) {
          
          if (notification.data.code === "001") {
            // _this.props.navigation.navigate("LaborJobList");
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        } else if (
          notification.foreground === false ||
          notification.foreground === null
        ) {
          if (notification.code === "001") {
            // _this.props.navigation.navigate("LaborJobList");
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        }

        // process the notification

        // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
        //  notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      senderID: "900411929042",
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
      popInitialNotification: true,
      requestPermissions: true
    });
  }

  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  getInterestedList() {
    for (let i = 0; i < this.state.projectList.length; i++) {
      if (this.state.projectList[i].workers === false) {
        this.state.interestedList.push(this.state.projectList[i]);
      }
      // console.log("interested List" + this.state.projectList[i].workers);
    }
    this.setTab(1);
    return this.state.interestedList;
  }

  apiCallUserProfileUpdate = token => {

    this.setState({loader:true})

    var myObject = {
      user: {
        gcm_token: token
      }
    };

    fetch(`${baseURL}/users/${this.state.uuid}`, {
      method: "PATCH",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.auth_token
      },
      body: JSON.stringify(myObject)
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})

        try {
          //  AsyncStorage.setItem("userType", "LC");
        } catch (error) {
          // Error saving data
          // We have data!!
        }
      })
      .catch(e => {
        this.setState({loader:false})
      });

    //
  };

  getInvitedList() {
    for (let i = 0; i < this.state.projectList.length; i++) {
      // console.log("interested List -----" + this.state.projectList[i].workers);
      if (this.state.projectList[i].workers === true) {
        this.state.invitedList.push(this.state.projectList[i]);
      }
      //console.log("interested List" + this.state.projectList[i].workers);
    }
    this.setTab(0);
    return this.state.invitedList;
  }

  setTab(tab) {
    this.setState({selectedTab: tab});
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})

      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const userId = await AsyncStorage.getItem("my_id");
      const profileUpdated = await AsyncStorage.getItem("profileUpdated");
      const token = await AsyncStorage.getItem("gcm_token", value => {
        JSON.parse(value); // boolean false
      });
      this.setState({gcm_token: token});

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
        this.setState({myUserId: userId});
        this.setState({profileUpdated: profileUpdated});

      }
      this.setPubNubOnce();

      fetch(`${baseURL}/job_applications`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log("response proposal list ...")
          console.log(responseJson)
          this.setState({loader:false})

          this.setState({
            projectList: responseJson.job_applications
          });
          //console.log("length is", this.state.projectList.length);
          if (this.state.projectList.length == 0) {

            this.setState({status: "No Proposal found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
          this.getInterestedList();
          this.getInvitedList();
        })
        .catch(e => {
          this.setState({loader:false})

        });
    } catch (error) {
      // Error retrieving data
    }
  };

  validateFields() {
    if (
      this.state.FirstName === "" ||
      this.state.LastName === "" ||
      this.state.EmailId === "" ||
      this.state.StreetAddress === "" ||
      this.state.City === "" ||
      this.state.State === "State" ||
      this.state.zipcode === "" ||
      this.state.avatarSource === ""
    ) {
      Alert.alert("", "License image and fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  

  validate = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // this.setState({ email: text });
      return true;
    } else {
      //this.setState({ email: text });
      return true;
    }
  };

  selectedTab() {
    if (this.state.selectedTab === 0) {
      return (
        <View style={{}}>
          {
            this.state.interestedList.length === 0 ? (
              <View style={{alignItems:'center', justifyContent:'center', marginHorizontal:'10%', height:'80%'}}>
              <Text style={{textAlign:'center'}}>No Interested proposal right now.</Text>
              </View>
            ) : (
              <FlatList
            style={{
              marginTop: 1
            }}
            data={this.state.interestedList}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={{
                  borderRadius: 10,
                  marginBottom: "4%",
                  width: "100%",
                  borderColor: "red",
                  // height: 120,
                  flexDirection: "column",
                  alignSelf: "center",
                  backgroundColor: colors.gray01
                }}
                onPress={() => {
                  this.applyLaborOnJobs(item.uuid, item.no_of_worker);
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center", marginLeft: 5
                  }}
                >
                  <Text
                    style={{
                      flex: 2,
                      alignSelf: "center",
                      fontSize: 15,
                      fontWeight: "bold"
                    }}
                  >
                    {item.title}
                  </Text>
                  <View style={{flex: 1.3, height: 30, alignItems:'center'}}>
                  <Text >
                    {Moment(item.start_date).format("D MMM YYYY")}
                  </Text>
                  <Text>
                   {item.start_time}
                  </Text>
                  </View>
                </View>

                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10}}
                  // numberOfLines={2}
                >
                  {"Description : " + item.description}
                </Text>
                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10}}
                  numberOfLines={2}
                >
                  {"Contact Name : " + item.contact_name}
                </Text>
                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10}}
                  numberOfLines={2}
                >
                  {"Contact Phone Number  : " + item.contact_no}
                </Text>
                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10}}
                  numberOfLines={2}
                >
                  {"Worker required  : " + item.no_of_worker}
                </Text>
                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10, paddingRight: 5}}
                  numberOfLines={2}
                >
                  {"Work address  : " + item.street_address + ", "+item.state+ ", "+item.zip_code}
                </Text>
                <View
                  style={{
                    height: 1,
                    marginTop: 20,
                    backgroundColor: colors.gray01
                  }}
                />
              </TouchableOpacity>
            )}
          />
            )
          }
          
        </View>
      );
    }
    if (this.state.selectedTab === 1) {
      return (
        <View style={{}}>
          {
this.state.invitedList.length === 0 ? (
  <View style={{alignItems:'center', justifyContent:'center', marginHorizontal:'10%', height:'80%'}}>
      <Text style={{textAlign:'center'}}>No Applied proposal right now.</Text>
  </View>
) : (
<FlatList
style={{
  marginTop: 1
}}
data={this.state.invitedList}
renderItem={({item, index}) => (
  <TouchableOpacity
    style={{
      borderRadius: 10,
      marginBottom: "4%",
      width: "100%",
      borderColor: "red",
      // height: 120,
      flexDirection: "column",
      alignSelf: "center",
      backgroundColor: colors.gray01
    }}
    onPress={() => {
      this.setPubNub(index);
    }}
  >
    <View
      style={{
        flexDirection: "row",
        alignItems: "center"
      }}
    >
      <Text
        style={{
          flex: 2,
          alignSelf: "center",
          fontSize: 15,
          fontWeight: "bold",
          marginLeft: 5
        }}
      >
        {item.title}
      </Text>
      <View style={{flex: 1.2, alignItems: 'center'}}>
      <Text >
        {Moment(item.start_date).format("D MMM YYYY")}
      </Text>
      <Text>
        {item.start_time}
      </Text>
      </View>
      <TouchableOpacity
        style={{
          flex: 0.4,
          marginLeft: 4,
          alignItems: "center",
          height: 30,
          backgroundColor: colors.green0s1
        }}
        onPress={() => {
          this.setPubNub(index);
        }}
      >
        <Image
          style={{
            alignSelf: "center",
            alignItems: "center",
            marginTop: 5,
            alignContent: "center"
          }}
          source={require("../../src/assets/chat.png")}
        />
      </TouchableOpacity>
    </View>

    <Text
      style={{fontSize: 16, paddingLeft: 5, paddingTop: 10}}
      // numberOfLines={2}
    >
      {"Description : " + item.description}
    </Text>
    <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10}}
                  numberOfLines={2}
                >
                  {"Contact Name : " + item.contact_name}
                </Text>
                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10}}
                  numberOfLines={2}
                >
                  {"Contact Phone Number  : " + item.contact_no}
                </Text>
                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10}}
                  numberOfLines={2}
                >
                  {"Worker required  : " + item.no_of_worker}
                </Text>
                <Text
                  style={{fontSize: 15, paddingLeft: 5, paddingTop: 10, paddingRight: 5}}
                  numberOfLines={2}
                >
                  {"Work address  : " + item.street_address + ", "+item.state+ ", "+item.zip_code}
                </Text>
    <View
      style={{
        height: 1,
        marginTop: 20,
        backgroundColor: colors.gray01
      }}
    />
  </TouchableOpacity>
)}
/>
)
          }
          
        </View>
      );
    }
  }

  handleClick = () => {
    //  this.props.navigation.navigate("People");
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  };

  gotoChat(channel) {
    var self = this;
    ChatEngineProvider.getChatRoomModel()
      .connect(channel)
      .then(() => {
        setTimeout(() => {

          this.setState({spinner: false});
          this.props.navigation.navigate("ChatStart");
        }, 1000);
      });
  }

  setPubNubOnce() {
    
    setTimeout(() => {
      this.setState({loader:true})

      ChatEngineProvider.connect(
        this.state.myUserId,
        this.state.myUserId
      ).then(async () => {
        try {
          this.setState({loader:false})

          AsyncStorage.setItem(
            ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY,
            JSON.stringify({
              username: "santosh",
              name: "santosh"
            })
          );
          //  this.props.navigation.navigate("JOBS");
          // this.props.navigation.navigate("PROPOSAL");
        } catch (error) {
          this.setState({loader:false})

          // ignore error, this save is just for convenience
        }

        // this.props.navigation.navigate('ChatList', {});
      });
    }, 1000);
  }

  setPubNub(index) {
    this.setState({spinner: true});
    setTimeout(() => {
      var channelName = "";
      if (this.state.invitedList[index].gc_id > this.state.myUserId) {
        channelName =
          this.state.myUserId + "" + this.state.invitedList[index].gc_id;
      } else {
        channelName =
          this.state.invitedList[index].gc_id + "" + this.state.myUserId;
      }

      this.gotoChat(channelName);
    }, 1000);
  }

  applyLaborOnJobs = (job_uuid,noOfWorker) => {

    try {
      AsyncStorage.setItem("uuid_jobs", job_uuid);
    } catch (error) {
      // Error saving data
      // We have data!!
    }
    var noofworkers = 0
    if(noOfWorker === null){
      noofworkers = 1
    }else{
      noofworkers = noOfWorker
    }
    this.props.navigation.navigate("ProposalAppliedLabor",{no_of_worker:noofworkers});
  };

  render() {
    return (
      <BackgroundNtScroll>
        <View style={{flex: 1, alignItems: 'center'}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              height: 50,
              flexDirection: "row",
              marginTop: 20, width: '100%'
            }}
          >
            {
              <TouchableOpacity
                style={{flex: 0.6, marginTop: 30, marginLeft: 10}}
                onPress={this.handleClick.bind(this)}
              >
                {
                  <Image
                    source={require("../../src/assets/h-menu.png")}
                  />
                }
              </TouchableOpacity>
            }
          </View>
          <Text
            style={{
              marginTop: -15,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center', width: 140
            }}
          >
            Proposal
          </Text>
          <Text
            style={{
              color: 'gray',
              fontSize: 12,
              fontWeight: "200",
              textAlign: 'center',
              marginTop:5
            }}
          >
            {this.state.selectedTab === 0 ? "Note :- Click on a proposal to apply for a job.":
            "Note :- Click chat icon to read message."}
          </Text>
          <View
            style={{
              marginTop: ".2%", width: '90%'
            }}
          >

            <View
              style={{
                height: height / 2 + 100,
                backgroundColor: colors.white
              }}
            >
              <View
                style={{
                  // backgroundColor: colors.colorGradient,
                  // borderRadius: 10,
                  // elevation: 10
                }}
              >
                <SafeAreaView style={{marginTop: 10}}>
                  <MaterialTabs
                    items={["Interested", "Applied"]}
                    selectedIndex={this.state.selectedTab}
                    onChange={this.setTab.bind(this)}
                    barColor={colors.white}
                    borderRadius={30}
                    textStyle={{fontSize: 15}}
                    uppercase={false}
                    indicatorColor={colors.colorGradient}
                    activeTextColor={colors.black}
                    inactiveTextColor={colors.gray02}
                  />
                </SafeAreaView>
                <Spinner
                  visible={this.state.spinner}
                  textContent={"Loading chat settings..."}
                  textStyle={styles.spinnerTextStyle}
                />
              </View>
              <View
                style={{
                  padding: 10,
                  paddingBottom: 0,

                  overflow: "visible"
                }}
              >
                {this.selectedTab()}
              </View>
              <View
                style={{
                  height: 50,
                  display: this.state.display,
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{alignSelf: "center", color: "black", fontSize: 18}}
                >
                  {/* {this.state.status} */}
                </Text>
              </View>
            </View>

            <TouchableOpacity
              style={{
                marginTop: "10%",
                borderRadius: 3,
                marginLeft: "10%",
                marginRight: "10%",
                height: 50,
                width: "80%",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  justifyContent: "center",
                  marginTop: "4%",
                  color: colors.white,
                  fontSize: 20,

                  fontWeight: "bold",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  Tabs: {
    borderRadius: 50
  },
  spinnerTextStyle: {
    color: colors.colorGradient,
    backgroundColor: colors.colorSignin,
    height: 100,
    borderRadius: 5,
    padding: 10,
    width: width - 60
  }
});

