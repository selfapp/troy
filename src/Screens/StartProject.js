import React, {Component} from "react";
import {
  FlatList,
  TouchableOpacity,
  Text,
  Alert,
  TextInput,
  Modal,
  AsyncStorage,
  View
} from "react-native";
import {NavigationActions, StackActions} from "react-navigation";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';

export default class StartProject extends Component {
  constructor() {
    super();
    this.state = {
      ProjectName: "",
      modalVisible: true,
      uuid: "",
      auth_token: "",
      projectList: [],
      loader:false
    };
  }

  handleClick = () => {
    {
      this.toggleModal(true);
    }
  };

  validateFields() {

    if (this.state.ProjectName == "") {
      Alert.alert("", "Fill project name.");
      return false;
    } else {
      return true;
    }
  }

  componentDidMount() {
    this._retrieveData();
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});

      }

      fetch(`${baseURL}/projects`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({
            projectList: responseJson.projects
          });
        })
        .catch(e => {
          this.setState({loader:false})
          console.log("called error " + e);
        });
    } catch (error) {
      // Error retrieving data
      console.log("called error " + error);
    }
  };

  apiCallAddProject = () => {
    if (this.validateFields()) {
      this.setState({loader:true})
      console.log("called");
      var myObject = {
        project: {
          name: this.state.ProjectName
        }
      };

      fetch(`${baseURL}/projects`, {
        method: "POST",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        },
        body: JSON.stringify(myObject)
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({ProjectName: ""});
          console.log(responseJson);

          try {
            console.log("start project call project id set", responseJson.project.uuid)

            AsyncStorage.setItem("prjid", responseJson.project.uuid);

            this._retrieveData();
          } catch (error) {
            // Error saving data
            // We have data!!
            console.log("error while saving async");
          }

          Alert.alert(
            "",
            "Project Added Successfully.",
            [
              {
                text: "OK",
                onPress: () => this.callStack()
              }
            ],
            {cancelable: false}
          );
          //this._retrieveData();
        })
        .catch(e => {
          this.setState({loader:false})
          console.log("called error " + e);
        });

      //
    } else {
    }
  };

  callStack() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: "MainTabNavigatorGC"})]
    });
    this.props.navigation.dispatch(resetAction);
  }

  toggleModal(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <BackgroundNtScroll>
        <View
          style={{
            position: "absolute",
            top: 50,
            width: "100%"
          }}
        >
          <Modal
            style={{
              alignItems: "center",
              justifyContent: "center"
            }}
            backdropOpacity={30}
            animationType={"slide"}
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              console.log("Modal has been closed.");
            }}
          >
            {}

            <TouchableOpacity
              onPress={() => {
                //this.toggleModal(!this.state.modalVisible);
              }}
              style={{
                borderRadius: 10,
                marginHorizontal: "5%",
                // marginRight: "5%",
                marginTop: 100,
                backgroundColor: colors.gray01,
                // width: "90%"
              }}
            >
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 20,
                  alignSelf: "center",
                  marginTop: "2%",
                  justifyContent: "center"
                }}
              >
                Add First project
              </Text>
              <TextInput
                style={{
                  height: 40,
                  fontSize: 18,
                  marginVertical: "4%",
                  borderColor: colors.gray05,
                  borderWidth: 1,
                  marginLeft: 10,
                  alignSelf: "center",

                  width: "70%",
                  marginRight: 10,
                  borderRadius: 7
                }}
                underlineColorAndroid={colors.gray01}
                placeholder="Enter project name"
                maxLength={20}
                // placeholderTextColor="#FF242424"
                autoCapitalize="none"
                value={this.state.ProjectName}
                onChangeText={value => this.setState({ProjectName: value})}
              />

              <View
                style={{
                  alignSelf: "center",
                  width: "40%"
                }}
              >
                <Button
                  marginTop={"1%"}
                  onPress={this.apiCallAddProject.bind(this)}
                  buttonName={"Add"}
                  backgroundColor={colors.colorSignin}
                  textColor={"white"}
                />

                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    alignSelf: "center",
                    marginTop: "4%",
                    height: 20,
                    justifyContent: "center"
                  }}
                />
              </View>
            </TouchableOpacity>
          </Modal>
        </View>

        <View
          style={{
            height: 85,
            flexDirection: "row",
            // marginLeft: "5%",
            // marginRight: "5%",
            marginTop: 20,
          }}
        >
          {/* <TouchableOpacity>
                        <Image style={{ alignSelf: 'flex-start' }} source={require("../../src/assets/backnew.png")} />
                    </TouchableOpacity> */}

          <View style={{flex: 1, marginTop: 40}}>
         
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Project List{" "}
            </Text>
          </View>
          {/* <TouchableOpacity onPress={this.handleClick.bind(this)}>
            <Image
              style={{ alignSelf: "flex-end" }}
              source={require("../../src/assets/add.png")}
            />
          </TouchableOpacity> */}
        </View>
        {this.state.loader ? <MyActivityIndicator /> : null}
        <FlatList
          style={{marginTop: 1}}
          data={this.state.projectList}
          renderItem={({item, index}) => (
            <View
              style={{
                marginLeft: "1%",
                marginBottom: "4%",
                marginRight: "1%"
              }}
            >
              <View
                style={{
                  borderRadius: 10,

                  width: "90%",
                  borderColor: "red",
                  height: 70,
                  justifyContent: "center",
                  backgroundColor: colors.white,
                  alignSelf: "center"
                }}
              >
                <Text
                  style={{
                    color: "black",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                    paddingLeft: 10
                  }}
                >
                  {item.name}
                </Text>
              </View>
            </View>
          )}
        />
      </BackgroundNtScroll>
    );
  }
}
