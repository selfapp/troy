GeneralContractor

import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import ModalDropdown from "react-native-modal-dropdown";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  Dimensions,
  ImageBackground,
  StatusBar,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import {NavigationActions, StackActions} from "react-navigation";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import {baseURL} from '../../api';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class GeneralContractor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      emailID: "",
      streetAddress: "",
      city: "",
      spinner: false,
      state: "State",
      zipCode: "",
      uuid: "",
      companyName: "",
      auth_token: "",
      gcm_token: "",
      my_Id: "",
      loader:false,
      checkEditProfile:false
    };
  }

  componentWillMount() {

    if(this.props.navigation.state.params){
      if(this.props.navigation.state.params.editProfile){
        this.setState({checkEditProfile:true})
      }
    }
    this._retrieveData();
  }

  validateFields() {
    let {firstName, lastName, emailID, streetAddress, city, state, zipCode, companyName} = this.state;
    if (
      (firstName === null || firstName === undefined) ||
      (lastName === null || lastName === undefined) ||
      (emailID === null || emailID === undefined) ||
      (streetAddress === null || streetAddress === undefined) ||
      (city === null || city === undefined) ||
      state === "State" ||
      (zipCode === null || zipCode === undefined) ||
      (companyName === null || companyName === undefined)
    ) {
      Alert.alert("", "All fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  validate = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return true;
    } else {
      return true;
    }
  };

  _retrieveData = async () => {

    try {
      this.setState({loader:true})

      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const token = await AsyncStorage.getItem("gcm_token", value => {
        JSON.parse(value); // boolean false
      });
      this.setState({gcm_token: token});

      if (uuid !== null) {
        // We have data!!
        this.setState({
          uuid: uuid,
          access_token: access_token
        });

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})

            this.setState({
              firstName: responseJson.user.first_name,
              lastName: responseJson.user.last_name,
              emailID: responseJson.user.email,
              streetAddress: responseJson.user.street_address,
              city: responseJson.user.city,
              state: responseJson.user.state,
              zipCode: responseJson.user.zip_code,
              role: responseJson.user.role,
              companyName: responseJson.user.company_name
            });

            if (responseJson.user.state == null) {
              this.setState({state: "State"});
            }
          })
          .catch(e => {
            this.setState({loader:false})

          });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  apiCallUserProfileUpdate = () => {
    if (this.validateFields()) {

      if (this.validate(this.state.emailID)) {
        this.setState({loader:true})

        var myObject = {
          user: {
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            email: this.state.emailID,
            street_address: this.state.streetAddress,
            city: this.state.city,
            state: this.state.state,
            zip_code: this.state.zipCode,
            role: "GC",
            gcm_token: this.state.gcm_token,
            company_name: this.state.companyName
          }
        };

        //api call for profile update

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "PATCH",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          },
          body: JSON.stringify(myObject)
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})

            try {
              AsyncStorage.setItem("userType", "GC");
            } catch (error) {
              // Error saving data
              // We have data!!
            }

            Alert.alert(
              "",
              "Profile updated successfully.",
              [
                {
                  text: "OK",
                  onPress: () => {
                    if(this.state.checkEditProfile){
                      this.props.navigation.navigate("PROFILE");
                    }else{
                      const resetAction = StackActions.reset({
                        index: 0,
                        actions: [
                          NavigationActions.navigate({routeName: "MainTabNavigatorGC"})
                        ]
                      });
                      this.props.navigation.dispatch(resetAction);
                    }
                  }
                }
              ],
              {cancelable: false}
            );
          })
          .catch(e => {
            this.setState({loader:false})

          });
      } else {
        Alert.alert("", "Please enter correct Email address.");
      }
    } else {
    }
  };

  handleClick = () => {
    if(this.state.checkEditProfile){
      this.props.navigation.navigate("PROFILE");
    }else{
      this.props.navigation.navigate("People");
    }
  };

  render() {

    var keyboardBehavior = ''
    if(Platform.OS === 'ios'){
      keyboardBehavior = 'padding'
    }else {
      keyboardBehavior = 'height'
    }

    return (
      <ImageBackground
        source={require("../../src/assets/bg-screen.png")}
        resizeMode="stretch"
        style={{flex: 1}}
      >
        <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated/>
       
        <Spinner
          visible={this.state.spinner}
          textContent={"Initializing app setting, please wait..."}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={{
            height: 30,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 50, alignItems: 'center'
          }}
        >
          <TouchableOpacity
            style={{flex: 0.4}}
            onPress={this.handleClick.bind(this)}
          >
            <Image source={require("../../src/assets/backnew.png")}/>
          </TouchableOpacity>

        </View>


        <View style={{height: 40, alignItems: "center", marginTop: -5}}>
          <Text
            style={{
              color: colors.black,
              fontSize: 24,
              fontWeight: "bold"
            }}
          >
            {this.state.checkEditProfile ? "Edit Profile" : "Register"}
            
          </Text>
        </View>
        {this.state.checkEditProfile ? (null) : (
           <View style={{height: 40, alignItems: "center"}}>
           <Text
             style={{
               color: colors.gray02,
               fontSize: 16
             }}
           >
             As a customer
           </Text>
         </View>
        )
        }
       
        <KeyboardAvoidingView behavior={keyboardBehavior} enabled>

        <ScrollView
          scrollEventThrottle={650}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={"handled"}
          keyboardDismissMode={"interactive"}
        >
           {this.state.loader ? <MyActivityIndicator /> : null}
          <View style={{height: 700}}>


            <View
              style={{
                borderRadius: 10,
                marginLeft: "10%",
                marginRight: "10%",
                width: "80%",
                height: height / 2 + 125,
                marginTop: -20
              }}
            >
              <TextInput
                style={{
                  marginTop: 15,
                  height: "12%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10,
                }}
                underlineColorAndroid={colors.gray01}
                placeholder="First Name"
                autoCapitalize="none"
                value={this.state.firstName}
                onChangeText={firstName => this.setState({firstName})}
              />

              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "12%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.lastName}
                onChangeText={lastName => this.setState({lastName})}
                underlineColorAndroid={colors.gray01}
                placeholder="Last Name"
                autoCapitalize="none"
              />

              <TextInput
                style={{
                  marginTop: 5,
                  height: "12%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10,
                }}
                value={this.state.companyName}
                underlineColorAndroid={colors.gray01}
                placeholder="Company Name"
                autoCapitalize="none"
                onChangeText={companyName => this.setState({companyName})}
              />

              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "12%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.emailID}
                onChangeText={emailID => this.setState({emailID})}
                underlineColorAndroid={colors.gray01}
                placeholder="Email Id"
                autoCapitalize="none"
                keyboardType={'email-address'}
              />

              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "12%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.streetAddress}
                onChangeText={streetAddress => this.setState({streetAddress})}
                underlineColorAndroid={colors.gray01}
                placeholder="Street Address"
                autoCapitalize="none"
              />
              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "12%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.city}
                onChangeText={city => this.setState({city})}
                underlineColorAndroid={colors.gray01}
                placeholder="City"
                autoCapitalize="none"
              />

              <View
                style={{
                  marginTop: 5,
                  height: "13%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row"
                  }}
                >
                  <ModalDropdown
                    style={{
                      flex: 0.4,
                      height: height * 0.07,
                      justifyContent: "center"
                    }}
                    options={[
                      "Alabama - AL",
                      "Alaska - AK",
                      "Arizona - AZ",
                      "Arkansas - AR",
                      "California - CA",
                      "Colorado - CO",
                      "Connecticut - CT",
                      "Delaware - DE",
                      "Florida - FL",
                      "Georgia - GA",
                      "Hawaii - HI",
                      "Idaho - ID",
                      "Illinois - IL",
                      "Indiana - IN",
                      "Iowa - IA",
                      "Kansas - KS",
                      "Kentucky - KY",
                      "Louisiana - LA",
                      "Maine - ME",
                      "Maryland - MD",
                      "Massachusetts - MA",
                      "Michigan - MI",
                      "Minnesota - MN",
                      "Mississippi - MS",
                      "Missouri - MO",
                      "Montana - MT",
                      "Nebraska - NE",
                      "Nevada - NE",
                      "New Hampshire - NH",
                      "New Jersey - NJ",
                      "New Mexico - NM",
                      "New York - NY",
                      "North Carolina - NC",
                      "North Dakota - ND",
                      "Ohio - OH",
                      "Oklahoma - OK",
                      "Oregon - OR",
                      "Pennsylvania - PA",
                      "Rhode Island - RI"
                    ]}
                    dropdownStyle={{
                      width: "40%",
                      fontWeight: "bold",
                    }}
                    defaultValue={this.state.state}
                    onSelect={(index, state) => this.setState({state})}
                    textStyle={{
                      paddingLeft: 10,
                      fontSize: 20,
                      color: colors.gray09
                    }}
                  />
                  <View style={{flex: 0.2}}/>
                  <View style={{flex: 0.4}}>
                    <TextInput
                      style={{
                        fontSize: 18,
                        marginLeft: 10,
                        marginRight: 10
                      }}
                      placeholder="Zip Code"
                      autoCapitalize="none"
                      keyboardType='number-pad'
                      maxLength={6}
                      value={this.state.zipCode}
                      onChangeText={zipCode => this.setState({zipCode})}
                    />
                  </View>
                </View>
              </View>

              <View
                style={{
                  height: 1,
                  marginLeft: 13,
                  marginRight: 13
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 0.4, backgroundColor: colors.gray01}}/>
                  <View style={{flex: 0.2}}/>
                  <View style={{flex: 0.4, backgroundColor: colors.gray01}}/>
                </View>
              </View>
            </View>

            <TouchableOpacity
              style={{
                marginTop: "10%",
                borderRadius: 30,
                marginLeft: "10%",
                marginRight: "10%",
                backgroundColor: colors.colorYogi,
                height: 50,
                width: "80%",
                alignItems: "center"
              }}

              // onPress={this.handleClick2.bind(this)}
              onPress={this.apiCallUserProfileUpdate.bind(this)}
            >
              <Text
                style={{
                  justifyContent: "center",
                  marginTop: "4%",
                  color: colors.white,
                  fontSize: 20,
                  height: 50,
                  fontWeight: "bold",
                  alignSelf: "center"
                }}
              >
              {this.state.checkEditProfile ? "Update Profile" : "Create Account"}
              </Text>
            </TouchableOpacity>

          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  spinnerTextStyle: {
    color: colors.colorGradient,
    backgroundColor: colors.colorSignin,
    height: 100,
    borderRadius: 5,
    padding: 10,
    width: width - 60
  }
});

