import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  Dimensions,
} from "react-native";
import {DrawerActions} from "react-navigation";
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";
import colors from "../styles/colors";
import BackgroundApp from "../Components/BackgroundApp";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class Profile extends Component {
  constructor() {
    super();
    this.state = {
      FirstName: "",
      LastName: "",
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "",
      zipcode: "",
      textvalue: "",
      uuid: "",
      CompanyName: "",
      auth_token: "",
      jobsCount: "",
      proposalCount: "",
      loader:false
    };
  }

  componentWillMount() {
    console.log("Component will mount called .....")
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this._retrieveData();
    })
  }

  slideMenu = () => {
    {
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!
        this.setState({
          uuid: uuid,
          access_token: access_token
        });

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "GET",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            this.setState({
              FirstName: responseJson.user.first_name + " " + responseJson.user.last_name,
              EmailId: responseJson.user.email,
              StreetAddress: responseJson.user.street_address,
              City: responseJson.user.city,
              State: responseJson.user.state,
              zipcode: responseJson.user.zip_code,
              role: responseJson.user.role,
              CompanyName: responseJson.user.company_name,
              jobsCount: responseJson.user.count_1,
              proposalCount: responseJson.user.count_2
            });
          })
          .catch(e => {
            this.setState({loader:false})
          });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  validateFields() {
    if (
      this.state.FirstName === "" ||
      this.state.LastName === "" ||
      this.state.EmailId === "" ||
      this.state.StreetAddress === "" ||
      this.state.City === "" ||
      this.state.State === "" ||
      this.state.zipcode === ""
    ) {
      Alert.alert("", "All fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  validate = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      //this.setState({ email: text });
      return true;
    }
  };

  apiCallUserProfileUpdate = () => {
    this.setState({loader:true})
    fetch(`${baseURL}/users/${this.state.uuid}`, {
      method: "GET",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.access_token
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
      })
      .catch(e => {
        this.setState({loader:false})
      });

    //
  };

  gotoChat(channel) {
    var self = this;

    ChatEngineProvider.getChatRoomModel()
      .connect(channel)
      .then(
        () => {
          self.props.navigation.navigate("ChatStart", {
            title: "#123" + channel
          });
        },
        reject => {
          alert(reject);
        }
      );
  }

  handleClick = () => {
    this.props.navigation.navigate("GeneralContractor");
  };

  editProfileAction() {
    console.log("fjhasjkd profile")
    this.props.navigation.navigate("GeneralContractor", {editProfile:true})
  }

  render() {
    return (
      <BackgroundApp>
        <View style={{flex: 1, alignItems: 'center'}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              // height: 50,
              flexDirection: "row",
              marginTop: 55, width: '100%'
            }}
          >
            <TouchableOpacity style={{ marginLeft: 10, width: 45, height: 45}}
                              onPress={this.slideMenu.bind(this)}>
              <Image
                source={require("../../src/assets/h-menu.png")}
              />
            </TouchableOpacity>
          
          <Text
            style={{
              // marginTop: 35,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center',
              width:width - 120
            }}
          >
            PROFILE
          </Text>

          <TouchableOpacity style={{width:60, 
            height:40, 
            // backgroundColor:'red',
            alignItems:'center',
            justifyContent:'center'
            }}onPress={()=> this.editProfileAction()}>
              <Image
              source={require("../../src/assets/edit.png")}
              />
          </TouchableOpacity>
          </View>
          <View
            style={{
              borderTopLeftRadius: 60,
              borderTopRightRadius: 60,
              marginTop: "5%",

              width: "100%",
              height: height - 100,
              backgroundColor: 'white'
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "column", backgroundColor: 'red'
              }}
            >
              <View style={{flex: 1, alignItems: "center", backgroundColor: colors.gray01}}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: 18,
                    marginTop: "5%",
                    paddingLeft: 20,
                    // maxLines: 1,
                    paddingRight: 20,
                    alignItems: "center",
                    justifyContent: "center",
                    fontWeight: "bold"
                  }}
                >
                  {this.state.FirstName}
                </Text>

                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: 18,

                    fontWeight: "bold"
                  }}
                >
                  {this.state.EmailId}
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: 16,
                    fontWeight: "bold"
                  }}
                >
                  {"(" + this.state.CompanyName + ")"}
                </Text>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: 16,
                    fontWeight: "bold"
                  }}
                >
                  {/* General contractor */}
                  Customer
                </Text>

                <View
                  style={{
                    height: "26%",
                    flexDirection: "row",

                    margin: "6%",
                    backgroundColor: colors.gray01
                  }}
                >
                  <View
                    style={{
                      flex: 1,

                      flexDirection: "column"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1,
                        fontSize: 18,
                        color: colors.black,
                        alignSelf: "center"
                      }}
                    >
                      {this.state.jobsCount}
                    </Text>
                    <Text
                      style={{
                        flex: 2,
                        color: colors.black,
                        fontSize: 14,
                        alignSelf: "center"
                      }}
                    >
                      Projects
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 0.02,
                      flexDirection: "column",
                      backgroundColor: colors.gray05
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1,
                        fontSize: 20,
                        color: colors.black,
                        alignSelf: "center"
                      }}
                    >
                      {this.state.proposalCount}
                    </Text>
                    <Text
                      style={{
                        flex: 2,
                        color: colors.black,
                        fontSize: 14,
                        alignSelf: "center"
                      }}
                    >
                      Jobs
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 0.02,
                      display: "none",
                      flexDirection: "column",
                      backgroundColor: colors.gray05
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      display: "none",
                      flexDirection: "column"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1,
                        color: colors.black,
                        fontSize: 20,
                        alignSelf: "center"
                      }}
                    >
                      0
                    </Text>
                    <Text
                      style={{
                        flex: 2,
                        color: colors.black,
                        fontSize: 14,
                        alignSelf: "center"
                      }}
                    >
                      Messages
                    </Text>
                  </View>
                </View>
              </View>
              <View style={{flex: 1.6, backgroundColor: colors.gray01}}>
                <View
                  style={{
                    borderTopLeftRadius: 30,
                    borderTopRightRadius: 30,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 30,
                    marginTop: "3%",
                    margin: "5%",
                    height: "60%",
                    elevation: 1,
                    backgroundColor: colors.white
                  }}
                >
                  <Text
                    style={{
                      alignSelf: "center",
                      fontSize: 18,
                      marginTop: "4%",
                      fontWeight: "bold",
                      color: colors.black
                    }}
                  >
                    Address
                  </Text>

                  <View
                    style={{
                      margin: "2%",

                      flexDirection: "column",

                      height: "40%"
                    }}
                  >
                    <View
                      style={{
                        flex: 0.4,
                        flexDirection: "row",

                        margin: "1%",
                        marginLeft: "3%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 3,

                          alignSelf: "center",
                          alignContent: "center",
                          fontSize: 16,

                          color: colors.black,
                          fontWeight: "bold"
                        }}
                      >
                        Street Address
                      </Text>
                      <Text
                        style={{
                          flex: 1,

                          alignContent: "center",
                          fontSize: 16,
                          alignSelf: "flex-start",
                          fontWeight: "bold",
                          color: colors.black
                        }}
                      >
                        City
                      </Text>
                    </View>

                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",

                        marginRight: "1%",
                        marginLeft: "3%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 3,

                          alignSelf: "baseline",
                          fontSize: 14
                        }}
                      >
                        {this.state.StreetAddress}
                      </Text>
                      <Text
                        style={{
                          flex: 1,

                          alignContent: "center",
                          fontSize: 14,
                          paddingBottom: 4
                        }}
                      >
                        {this.state.City}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      marginLeft: "2%",
                      marginRight: "4%",

                      flexDirection: "column",

                      height: "24%"
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",

                        marginLeft: "4%",
                        marginRight: "2%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 3,

                          alignSelf: "baseline",
                          fontSize: 16,
                          color: colors.black,

                          fontWeight: "bold"
                        }}
                      >
                        State
                      </Text>
                      <Text
                        style={{
                          flex: 1,

                          alignContent: "center",
                          fontSize: 16,
                          marginLeft: "6%",
                          alignSelf: "flex-start",
                          fontWeight: "bold",
                          color: colors.black
                        }}
                      >
                        Zip code
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",

                        marginLeft: "2%",
                        marginRight: "2%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 3.2,

                          alignSelf: "baseline",
                          fontSize: 14,
                          marginLeft: "2%"
                        }}
                      >
                        {this.state.State}
                      </Text>
                      <Text
                        style={{
                          flex: 1,

                          alignContent: "center",
                          fontSize: 14,

                          alignSelf: "flex-start"
                        }}
                      >
                        {this.state.zipcode}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </BackgroundApp>
    );
  }
}
