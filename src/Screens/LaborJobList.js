import React, {Component} from "react";
import {
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Alert,
  View,
  Dimensions
} from "react-native";
import Moment from "moment";
import {DrawerActions} from "react-navigation";
import MyActivityIndicator from '../Components/activity_indicator';

import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class LaborJobList extends Component {
  constructor() {
    super();
    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      auth_token: "",
      projectList: [],
      project_uuid: "",
      state: "",
      display: "none",
      refreshing: false,
      loader:false,
      lat:0,
      lng:0
    };
  }

  handleClick = () => {
    {
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };

  componentDidMount() {
    this.getLocation()
    // this._retrieveData();
  }

  getLocation(){
    navigator.geolocation.getCurrentPosition(
        (position) => {
            this.setState({
                lat: position.coords.latitude,
                lng: position.coords.longitude
            });
            console.log('lat ', position.coords.latitude, ' lng ', position.coords.longitude)
            this._retrieveData();
        },
        (error) => {
            alert('Please allow access to location services.')
            // alert(error.message)
    },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000},
    );
}

  apiCallMarkInterested(position) {

    this.setState({loader:true})
    var myObject = {
      job_application: {
        job_id: this.state.projectList[position].uuid,
        lc_id: this.state.uuid,
        status: "INTERESTED"
      }
    };
    console.log("Interested job body", myObject)

    fetch(`${baseURL}/job_applications`, {
      method: "POST",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.auth_token
      },
      body: JSON.stringify(myObject)
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log("Job mark interested ...........", responseJson)
        this.setState({loader:false})
        // this.setState({ ProjectName: "" });

        try {
          // AsyncStorage.setItem("prjid", responseJson.project.uuid);
        } catch (error) {
          // Error saving data
          // We have data!!
        }

        Alert.alert(
          "",
          "Job marked as Interested, Check your Proposals.",
          [
            {
              text: "OK",
              onPress: () => this._retrieveData()
            }
          ],
          {cancelable: false}
        );
      })
      .catch(e => {
        this.setState({loader:false})
      });

    //
  }

  handleRefresh = () => {
    this._retrieveData();
  };

  apiCallMarkPass(position) {
    this.setState({loader:true})
    fetch(
      `${baseURL}/job_applications/${this.state.projectList[position].uuid}/pass`,
      {
        method: "POST",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
        //body: JSON.stringify(myObject)
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        // this.setState({ ProjectName: "" });
        try {
          // AsyncStorage.setItem("prjid", responseJson.project.uuid);
        } catch (error) {
          // Error saving data
          // We have data!!
        }

        Alert.alert(
          "",
          "Marked as passed job.",
          [
            {
              text: "OK",
              onPress: () => this._retrieveData()
            }
          ],
          {cancelable: false}
        );
      })
      .catch(e => {
        this.setState({loader:false})
      });

    //
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
      }
      fetch(`${baseURL}/projects?lat=${this.state.lat}&lng=${this.state.lng}`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          console.log("rposne djfkjhdfjk")
          console.log(responseJson)
          this.setState({
            projectList: responseJson.job_list
          });
          if (this.state.projectList.length == 0) {

            this.setState({status: "No Jobs found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View
          style={{
            height: 85,
            flexDirection: "row",
            marginTop: 20,
          }}
        >
          {
            <TouchableOpacity style={{flex: 0.6, marginTop: 30, marginLeft: 10}} onPress={this.handleClick.bind(this)}>
              <Image
                style={{alignSelf: "flex-start"}}
                source={require("../../src/assets/h-menu.png")}
              />
            </TouchableOpacity>
          }

          <View style={{flex: 1, marginTop: 40}}>
          
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                fontWeight: "400"
              }}
            >
              Jobs
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            display: this.state.display,
            justifyContent: "center"
          }}
        >
          <Text style={{alignSelf: "center", color: "black", fontSize: 18}}>
            {this.state.status}
          </Text>
        </View>
        {this.state.loader ? <MyActivityIndicator /> : null}
        {/* {console.log(JSON.stringify(this.state.projectList))} */}
        <FlatList
          style={{marginTop: 1}}
          data={this.state.projectList}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          renderItem={({item, index}) => (
            <View
              style={{
                borderRadius: 10,
                marginBottom: "4%",
                width: DEVICE_WIDTH-40,
                borderColor: "red",
                // height: 300,
                alignSelf: "center",
                backgroundColor: colors.gray01
              }}
            >
              <Text style={styles.dateText}>
                {Moment(item.start_date).format("D MMM YYYY") +
                "  to  " +
                Moment(item.end_date).format("D MMM YYYY")}
              </Text>
              <Text style={styles.dateText}>
                {item.start_time +
                "  to  " +
                item.end_time}
              </Text>
              <Text style={styles.titleText}>{item.title}</Text>
              <Text
                style={{fontSize: 16, paddingLeft: 10, paddingTop: 10}}
                // numberOfLines={2}
              >
                {item.description}
              </Text>
              <View style={{flexDirection: "row"}}>
                <Text style={{fontSize: 16, paddingLeft: 10, paddingTop: 10}}>
                  Customer:
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    paddingLeft: 10,
                    paddingTop: 10,
                    color: "red"
                  }}
                >
                  {item.name}
                </Text>
              </View>
              <View style={{flexDirection: "row"}}>
                <Text style={{fontSize: 16, paddingLeft: 10, paddingTop: 10}}>
                  {/* General Contractor: */}
                  Contact Name:
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    paddingLeft: 10,
                    paddingTop: 10,
                    color: "red"
                  }}
                >
                  {item.contact_name}
                </Text>
              </View>
              <View style={{flexDirection: "row"}}>
                <Text style={{fontSize: 16, paddingLeft: 10, paddingTop: 10}}>
                  Contact Phone Number:
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    paddingLeft: 10,
                    paddingTop: 10,
                    color: "red"
                  }}
                >
                  {item.contact_no}
                </Text>
              </View>
              <View style={{flexDirection: "row"}}>
                <Text style={{fontSize: 16, paddingLeft: 10, paddingTop: 10}}>
                  Skill Level:
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    paddingLeft: 10,
                    paddingTop: 10,
                    color: "red"
                  }}
                >
                  {item.skill_level}
                </Text>
              </View>
              <View style={{flexDirection: "row"}}>
                <Text style={{fontSize: 16, paddingLeft: 10, paddingTop: 10}}>
                  Worker required:
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    paddingLeft: 10,
                    paddingTop: 10,
                    color: "red"
                  }}
                >
                  {item.no_of_worker}
                </Text>
              </View>
              <View style={{flexDirection: "row", marginRight:10,
              //  backgroundColor:'yellow'
               }}>
                <Text style={{fontSize: 16, paddingLeft: 10, paddingTop: 10,
                // backgroundColor:'gray'
                }}>
                  Work address:
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    paddingLeft: 10,
                    paddingTop: 10,
                    color: "red",
                    marginRight:10,
                    width:DEVICE_WIDTH-160
                  }}
                >
                  {item.street_address + ", "+item.state+ ", "+item.zip_code}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  marginLeft: 5,
                  marginRight: 5
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.apiCallMarkInterested(index);
                  }}
                  style={{
                    backgroundColor: "black",
                    alignSelf: "center",
                    flex: 1,
                    marginTop: 30,

                    height: 40,
                    borderRadius: 3,
                    justifyContent: "center"
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.apiCallMarkInterested(index);
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        alignSelf: "center",
                        justifyContent: "center",
                        fontSize: 14
                      }}
                    >
                      Interested
                    </Text>
                  </TouchableOpacity>
                </TouchableOpacity>

                <View
                  style={{
                    alignSelf: "center",
                    flex: 0.1,
                    marginTop: 30,

                    height: 40,
                    borderRadius: 3,
                    justifyContent: "center"
                  }}
                />

                <TouchableOpacity
                  onPress={() => {
                    this.apiCallMarkPass(index);
                  }}
                  style={{
                    backgroundColor: "black",
                    alignSelf: "center",
                    flex: 1,
                    marginTop: 30,

                    height: 40,
                    borderRadius: 3,
                    justifyContent: "center"
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.apiCallMarkPass(index);
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        alignSelf: "center",
                        justifyContent: "center",
                        fontSize: 14
                      }}
                    >
                      {/* Not interested */}
                      Can not fill 
                    </Text>
                  </TouchableOpacity>
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 14,
    paddingTop: 5,
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 16,
    paddingLeft: 10
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});