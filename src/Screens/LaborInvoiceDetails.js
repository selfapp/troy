import React, {Component} from "react";
import {
  Dimensions,
  Image,
  FlatList,
  ScrollView,
  Alert,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import Moment from "moment";

import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {API, baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';
import { NavigationActions, StackActions } from "react-navigation";

const height = Dimensions.get("window").height;
const DEVICE_WIDTH = Dimensions.get("window").width;

export default class LaborInvoiceDetails extends Component {
  constructor() {
    super();
    this.state = {
      calenderSelectedValue: [],
      currentDateForCalender: "",
      currentDate: "",
      dateInitailNumber: "",
      jobTitle: "",
      workerName: "",
      workingHours: "",
      approvedStatus: "",
      dateMiddleText: "",
      dateLastText: "",
      scheduleData: [],
      DataJsonColored: "",
      selectedGcName: "",
      selectedEmailAddress: "",
      selectedAddress: "",
      selectedWorkingHours: "",
      firstSelectedDate: "",
      modalVisible: false,
      approveDisplay: "none",
      approveButtonDisplay: "none",
      uuid: "",
      auth_token: "",
      SelectProjectData: [],
      projectListRaw: [],
      projectList: [],
      status: "",
      ProjectNameAsUUID: "",
      ProjectName: "Select Project",
      project_uuid: "",
      timecardsArray: [],
      billAmount: "",
      amountPrice:'',
      loader:false,
      showHome:false
    };
  }

  componentDidMount() {
    if(this.props.navigation.state.params){
      this.setState({showHome:true})
    }else{
      this.setState({showHome:false})
    }
    this._retrieveData();
  }

  _retrieveData = async index => {
    let invoice_uuid = await AsyncStorage.getItem('uuid_invoice');
    try {
      API.getInvoiceDetailsLc(invoice_uuid)
        .then(resp => resp.json())
        .then(jsonResp => {
          this.setState({projectList: jsonResp.invoice});
          if (jsonResp.invoice.workers.length > 0) {

            console.log("response ljfsljlf")
            console.log(jsonResp)
            this.setState({
              status: "",
              approveDisplay: "flex",
              approveButtonDisplay: "flex",
              timecardsArray: jsonResp.invoice.workers
            }, () => {
              if (this.state.projectList.status === "Unpaid") {
                this.setState({
                  approveButtonDisplay: "flex"
                });
              }
              this.getCurrentDateParams();

            });
          } else {
            this.setState({
              status: "No invoice found.",
              approveDisplay: "none"
            });
          }
        });
    } catch (error) {
    }
  };

  handleClick = () => {
    this.props.navigation.navigate("LaborInvoices");
  };

  _rejectInvoice = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const uuid_invoice = await AsyncStorage.getItem("uuid_invoice");
      if (uuid !== null) {
        // We have data!!
        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        fetch(`${baseURL}/lc/invoices/${uuid_invoice}`, {
          method: "DELETE",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            Alert.alert(
              "",
              "Invoice Declined Successfully.",
              [
                {
                  text: "OK"
                }
              ],
              {cancelable: false}
            );
            this._retrieveData();
          })
          .catch(e => {
            this.setState({loader:false})
          });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  _acceptInvoice = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const uuid_jobs = await AsyncStorage.getItem("job_uuid");
      const uuid_invoice = await AsyncStorage.getItem("uuid_invoice");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({access_token: access_token});


        fetch(
          `${baseURL}/gc/invoices/${uuid_jobs}/${uuid_invoice}`,
          {
            method: "POST",

            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Token token=" + this.state.access_token
            }
          }
        )
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            Alert.alert(
              "",
              "Paid Successfully.",
              [
                {
                  text: "OK"
                }
              ],
              {cancelable: false}
            );
            this._retrieveData();
          })
          .catch(e => {
            this.setState({loader:false})
          });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  getCurrentDateParams() {
    this.setState({
      jobTitle: this.state.projectList.title,
      workerName: this.state.projectList.title,
      workingHours: this.state.projectList.value,
      approvedStatus: this.state.projectList.status,
      billAmount: this.state.projectList.total_hours,
      amountPrice: this.state.projectList.amount
    });
  }

  goToHome(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: "MainTabNavigatorLabor"})
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <BackgroundNtScroll>
        
        <View
          style={{
            height: 85,
            flexDirection: "row",
            // marginLeft: "5%",
            // marginRight: "5%",
            marginTop: 20
          }}
        >
          {
            <TouchableOpacity
              style={{width: 50, marginTop: 30,marginLeft: 10}}
              onPress={this.handleClick.bind(this)}
            >
              <Image
                style={{alignSelf: "flex-start"}}
                source={require("../../src/assets/backnew.png")}
              />
            </TouchableOpacity>
          }

          <View style={{width:DEVICE_WIDTH-110, marginTop: 40,
          alignItems:'center'}}>
         
            <Text
              style={{
                color: colors.black,
                fontSize: 22,
                alignSelf: "center",
                // marginTop: 40
              }}
            >
              Invoice Details
            </Text>
          </View>
          {
            this.state.showHome ? (
              <TouchableOpacity
            style={{width: 50, 
              marginRight:-15,
          justifyContent:'center',alignItems:'center'}}
            onPress={()=>this.goToHome(this)}
          >
            <Text style={{fontWeight:'bold', marginTop:7}}>
              HOME
            </Text>
          </TouchableOpacity>
            ) : (null)
          }
          
        </View>

        <ScrollView style={{flex: 1}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomLeftRadius: 15,
              borderBottomRightRadius: 15,
              marginTop: "3%",
              margin: "5%",
              height: height / 2 + 230,
              elevation: 1,
              backgroundColor: colors.white
            }}
          >
            <View
              style={{
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  marginTop: 10,
                  marginLeft: 20,
                  width: 20,
                  backgroundColor: colors.white,
                  height: 60,
                  borderRadius: 35,
                  alignContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    alignContent: "center",
                    alignItems: "center",
                    fontSize: 20,

                    marginTop: "25%",
                    fontWeight: "bold",
                    color: colors.colorClipTop
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 0.9
                }}
              >
                <View
                  style={{
                    flex: 1.8,
                    marginLeft: 10
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      alignSelf: "center",
                      textAlignVertical: "bottom",
                      marginTop: 18,
                      fontWeight: "bold",
                      color: colors.colorClipTop
                    }}
                  >
                    {Moment(this.state.projectList.from).format("D MMM YYYY") +
                    "  to  " +
                    Moment(this.state.projectList.to).format("D MMM YYYY")}
                  </Text>
                </View>

                <View style={{flex: 1}}>
                  <Text
                    style={{
                      alignSelf: "center",
                      fontSize: 22,
                      marginTop: "1%",
                      fontWeight: "bold",
                      color: colors.black
                    }}
                  >
                    {this.state.workingHours}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                marginTop: 22,
                backgroundColor: colors.gray01,
                height: 1
              }}
            />
            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                height: "13%"
              }}
            >
              <View
                style={{
                  flex: 0.6,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/worker-icon.png")}
                  />
                </View>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Job Title
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.7,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.jobTitle}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 3,
                height: "20%"
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/worker-icon.png")}
                  />
                </View>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Worker List
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",
                  marginLeft: "3%"
                }}
              >
                <FlatList
                  style={{
                    marginTop: 1,
                    marginLeft: "12%",
                    borderTopLeftRadius: 15,
                    borderTopRightRadius: 15,
                    borderBottomLeftRadius: 15,
                    borderBottomRightRadius: 15,
                    height: height / 7,
                    width: 120,
                    elevation: 1,
                    backgroundColor: colors.white,
                    display: this.state.approveDisplay
                  }}
                  data={this.state.timecardsArray}
                  renderItem={({item, index}) => (
                    <View style={{height: 30, margin: 10}}>
                      <Text
                        style={{
                          fontSize: 14,

                          color: colors.colorClipTop,
                          fontWeight: "bold",

                          textAlignVertical: "center"
                        }}
                      >
                        {index +
                        1 +
                        ") Worker " +
                        item.name +
                        " worked on " +
                        this.state.jobTitle +
                        "."}
                      </Text>
                    </View>
                  )}
                />
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 60,
                height: 70
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/clock.png")}
                  />
                </View>
                <View style={{flex: 0.5}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Working hours
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.billAmount}
                  </Text>
                </View>
              </View>
            </View>


            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 5,
                height: 70
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/price.png")}
                  />
                </View>
                <View style={{flex: 0.5}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      fontSize: 16,
                      color: colors.colorClipTop,
                      fontWeight: "bold",
                      textAlignVertical: "center"
                    }}
                  >
                    Amount
                  </Text>
                </View>
                <View style={{flex: 0.4}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      fontSize: 16,
                      color: colors.colorClipTop,
                      fontWeight: "bold",
                      textAlignVertical: "center"
                    }}
                  >
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      height: 20,
                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.amountPrice}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 0,
                height: 70
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/time-clock.png")}
                  />
                </View>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Status
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      height: 20,
                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.approvedStatus}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <View style={{justifyContent: "center"}}>
          <Text style={{alignSelf: "center", color: "black"}}>
            {this.state.status}
          </Text>
        </View>
      </BackgroundNtScroll>
    );
  }
}
