import React, { Component } from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import Moment from "moment";

import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import { baseURL } from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';
import { NavigationActions, StackActions } from "react-navigation";

const height = Dimensions.get("window").height;

export default class HistoryLCDetails extends Component {
  constructor() {
    super();

    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      checked: true,
      auth_token: "",
      jobs_uuid: "",
      projectList: [],
      invoiceDetails: [],
      project_uuid: "",
      display: "none",
      status: "",
      loader:false
    };
  }

  handleClick = () => {
    {
      this.props.navigation.navigate("HistoryLCJobListAllType");
    }
  };

  componentDidMount() {
    this._retrieveData();
  }

  goToHome(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: "MainTabNavigatorLabor"})
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})

      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const jobs_uuid = await AsyncStorage.getItem("uuid_jobs");

      console.log("job uui d is ", jobs_uuid)
      console.log(uuid)
      if (uuid !== null) {
        // We have data!!

        this.setState({ uuid: uuid });
        this.setState({ auth_token: access_token });
        this.setState({ jobs_uuid: jobs_uuid });

      }
      console.log(' Invice api', `${baseURL}/lc/invoices/${jobs_uuid}`);
      fetch(`${baseURL}/lc/invoices/${jobs_uuid}`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})

          console.log('Invice Details', JSON.stringify(responseJson));
          this.setState({
            projectList: responseJson.invoice,
            invoiceDetails: [responseJson]
          });

          if (this.state.projectList.length == 0) {
            this.setState({
              status: "No invoice has been generated yet.",
              display: "flex"
            });
          } else {
            this.setState({ display: "none" });
          }
        })
        .catch(e => {
          this.setState({loader:false})

        });
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
       
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 40, alignItems: 'center'
          }}
        >
          <TouchableOpacity
            style={{ width: 30 }}
            onPress={this.handleClick.bind(this)}
          >
            <Image
              style={{ alignSelf: "flex-start" }}
              source={require("../../src/assets/backnew.png")}
            />
          </TouchableOpacity>

          <View style={{ flex: 1, marginTop: 20 }}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Invoice Details
            </Text>
          </View>
          <TouchableOpacity
            style={{width: 50, height:40, 
              marginRight:-15,
          justifyContent:'center',alignItems:'center'}}
            onPress={()=>this.goToHome(this)}
          >
            <Text style={{fontWeight:'bold', marginTop:7}}>
              HOME
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            marginTop: "1%"
          }}
        >

{this.state.loader ? <MyActivityIndicator /> : null}

          <View
            style={{
              borderRadius: 10,
              marginLeft: "10%",
              marginRight: "10%",
              marginTop: "10%",
              width: "80%",
              height: height / 2 + 70,
              backgroundColor: colors.gray01
            }}
          >
            <View
              style={{
                flex: 1,
                display: this.state.display,
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  alignSelf: "center",
                  padding: 10,
                  color: "black",
                  fontSize: 14
                }}
              >
                {this.state.status}
              </Text>
            </View>
            <FlatList
              style={{}}
              data={this.state.projectList}
              renderItem={({ item, index }) => (
                <View
                  style={{
                    borderRadius: 10,

                    marginBottom: "4%",
                    width: "100%",

                    borderColor: "red",
                    height: height / 2,
                    flexDirection: "column",
                    alignSelf: "center",
                    backgroundColor: colors.gray01
                  }}
                  onPress={() => {
                    // this.applyLaborOnJobs(item.uuid);
                  }}
                >
                  <View
                    style={{
                      borderTopRightRadius: 5,
                      borderTopLeftRadius: 5,
                      flexDirection: "row",
                      alignItems: "center",
                      height: "20%",
                      backgroundColor: colors.gray04
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.1,
                        alignSelf: "center",
                        fontSize: 16,
                        marginLeft: 10,
                        fontWeight: "bold",
                        color: colors.white
                      }}
                    >
                      {this.state.invoiceDetails[0].title}
                    </Text>
                    <Text
                      style={{ flex: 1.6, fontSize: 12, color: colors.white }}
                    >
                      {Moment(item.from).format("D MMM YYYY") +
                        " to " +
                        Moment(item.to).format("D MMM YYYY")}
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: colors.gray01,
                      padding: 5
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "2%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 1.5,
                          alignSelf: "center",
                          fontSize: 16,
                          fontWeight: "bold"
                        }}
                      >
                        Value
                      </Text>
                      <Text
                        style={{
                          flex: 1.6,
                          fontSize: 18,
                          color: colors.colorGradient
                        }}
                      >
                        {item.value}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      <Text
                        style={{
                          flex: 1.5,
                          alignSelf: "center",
                          fontSize: 16,
                          fontWeight: "bold",
                          marginTop: "4%"
                        }}
                      >
                        Total time
                      </Text>
                      <Text
                        style={{
                          flex: 1.6,
                          marginTop: "2%",
                          fontSize: 18,
                          color: colors.colorGradient
                        }}
                      >
                        {item.total_time}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "2%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 1.5,
                          alignSelf: "center",
                          fontSize: 16,
                          marginTop: "2%",
                          fontWeight: "bold"
                        }}
                      >
                        Invoice status :
                      </Text>
                      <Text
                        style={{
                          flex: 1.6,
                          fontSize: 18,
                          color: colors.colorGradient
                        }}
                      >
                        {item.status}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "1%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 1.5,
                          alignSelf: "center",
                          fontSize: 16,
                          fontWeight: "bold"
                        }}
                      >
                        {/* GC name : */}
                        Customer name :
                      </Text>
                      <Text
                        style={{
                          flex: 1.6,
                          fontSize: 18,
                          marginTop: "2%",
                          color: colors.colorGradient
                        }}
                      >
                        {this.state.invoiceDetails[0].gc.name}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "2%"
                      }}
                    >
                      <Text
                        style={{
                          flex: 1.5,
                          alignSelf: "center",
                          fontSize: 16,
                          fontWeight: "bold"
                        }}
                      >
                        {/* GC contact no: */}
                        Customer contact no:
                      </Text>
                      <Text
                        style={{
                          flex: 1.6,
                          fontSize: 18,
                          color: colors.colorGradient
                        }}
                      >
                        {this.state.invoiceDetails[0].gc.contact_number}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginTop: "2%"
                      }}
                    >
                      <Text style={{ flex: 1.6 }} />
                    </View>
                  </View>

                  <View
                    style={{
                      height: 0,
                      backgroundColor: colors.gray05
                    }}
                  />
                </View>
              )}
            />
          </View>
        </View>

        <View
          style={{
            height: "7%",
            justifyContent: "center",
            backgroundColor: colors.colorSignin,
            width: "40%",
            borderRadius: 5,
            display: "none",
            alignSelf: "center",
            marginTop: 25
          }}
        >
          <TouchableOpacity
            onPress={() => {
              //this.props.navigation.navigate("PROPOSAL");
              //  this.apiCallMarkWokerAgainstJob();
            }}
          >
            <Text
              style={{
                color: "white",
                alignSelf: "center",
                justifyContent: "center",
                fontSize: 14
              }}
            />
          </TouchableOpacity>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 14,
    paddingTop: 5,
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 16,
    paddingLeft: 10
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
