import React, {Component} from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import {DrawerActions} from "react-navigation";
import Moment from "moment";
import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;

export default class JobList extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
      uuid: "",
      auth_token: "",
      SelectProjectData: [],
      projectListRaw: [],
      projectList: [],
      ProjectNameAsUUID: "",
      ProjectName: "Select Project",
      project_uuid: "",
      gcm_token: "",
      profileUpdated: "false",
      project_id_fetch: "",
      display: "none",
      status: "",
      refreshing: false,
      loader:false
    };
  }

  handleRefresh = () => {
    this._retrieveDataForProjectList();
  };

  handleClick = () => {
    {
      this.props.navigation.navigate("AddJobs");
    }
  };

  slideMenu = () => {
    {
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };

  _retrieveDataForProjectList = async () => {
    try {
      this.setState({loader:true})
      const access_token = await AsyncStorage.getItem("access_token");

      this.setState({auth_token: access_token});

      fetch(`${baseURL}/projects`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + access_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({
            projectListRaw: responseJson.projects
          });

          if (this.state.projectListRaw == null) {
            this.props.navigation.navigate("StartProject");
          } else {
            try {
              //const project_id = await AsyncStorage.getItem("prjid");

              //console.log("project id is", prjid);

              // AsyncStorage.setItem("prjid", this.state.psuccessrojectListRaw[0].uuid);
              this._retrieveData();

            } catch (error) {
              this.setState({loader:false})
              // Error saving data
              // We have data!!
            }
          }
        })
        .catch(e => {
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  _retrieveData = async index => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");

      const gcmtoken = await AsyncStorage.getItem("gcm_token");

      const ProfileUpdated = await AsyncStorage.getItem("profileUpdated");

      // const project_id_get = await AsyncStorage.getItem("prjid");

      // console.log("my gc token is", gcmtoken);

      const access_token = await AsyncStorage.getItem("access_token");
      const project_id = await AsyncStorage.getItem("prjid");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({profileUpdated: ProfileUpdated});

        this.setState({auth_token: access_token});
        this.setState({gcm_token: gcmtoken});

        if (project_id == null && this.state.projectListRaw.length != 0) {
          this.props.navigation.navigate("ProjectSelection");
        }
      }

      fetch(`${baseURL}/projects/${project_id}/jobs`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          // responseJson.jobs.from_date.substring(0, 5);
          this.setState({
            projectList: responseJson.jobs
          });
          if (this.state.projectList.length == 0) {

            this.setState({status: "No Jobs found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
        })

        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  apiCallUserProfileUpdate = token => {
    this.setState({loader:true})
    var myObject = {
      user: {
        gcm_token: token
      }
    };

    fetch(`${baseURL}/users/${this.state.uuid}`, {
      method: "PATCH",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.auth_token
      },
      body: JSON.stringify(myObject)
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        try {
          // AsyncStorage.setItem("userType", "LC");
        } catch (error) {
          // Error saving data
          // We have data!!
        }
      })
      .catch(e => {
        this.setState({loader:false})
      });

    //
  };

  componentWillMount() {
    this._retrieveDataForProjectList();
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this._retrieveDataForProjectList();
    });
  }

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View style={{flex: 1, alignItems: 'center'}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              height: 55,

              flexDirection: "row",
              // marginLeft: "0%",
              // marginRight: "5%",
              marginTop: 20, width: '100%'
            }}
          >
            <TouchableOpacity style={{flex: 0.95, marginTop: 35, marginLeft: 10}}
                              onPress={this.slideMenu.bind(this)}>
              <Image
                source={require("../../src/assets/h-menu.png")}
              />
            </TouchableOpacity>

            {/* <View style={{ flex: 1, marginTop: 40 }}>
              <Text
                style={{
                  flex: 1,
                  color: colors.black,
                  fontSize: 24,
                  // alignSelf: "center",
                  fontWeight: "400"
                }}
              >
                Job List
            </Text>
            </View> */}
            <TouchableOpacity style={{marginTop: 35}}
                              onPress={this.handleClick.bind(this)}>
              <Image
                style={{alignSelf: "flex-end"}}
                source={require("../../src/assets/add.png")}
              />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              marginTop: -10,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center', width: 150
            }}
          >
            {/* Job List */}
            Work order
          </Text>
          <View
            style={{
              borderRadius: 10,
              marginLeft: "10%",
              marginRight: "10%",
              marginTop: "3%",
              width: "80%",
              height: height / 1.5,
              backgroundColor: colors.white
            }}
          >
            <View
              style={{
                flex: 1,
                display: this.state.display,
                justifyContent: "center"
              }}
            >
              <Text style={{alignSelf: "center", color: "black", fontSize: 18}}>
                {this.state.status}
              </Text>
            </View>

            <FlatList
              style={{marginTop: 1}}
              data={this.state.projectList}
              refreshing={this.state.refreshing}
              onRefresh={this.handleRefresh}
              // ListHeaderComponent={this.renderHeader}
              renderItem={({item, index}) => (
                <View
                  style={{
                    borderRadius: 10,
                    marginBottom: "4%",
                    width: "100%",
                    borderColor: "red",
                    height: 100,
                    alignSelf: "center",
                    backgroundColor: colors.gray01
                  }}
                >
                  <Text style={styles.dateText}>
                    {Moment(item.start_date).format("D MMM YYYY") +
                    "  to  " +
                    Moment(item.end_date).format("D MMM YYYY")}
                  </Text>
                  <Text style={styles.titleText}>{item.title}</Text>
                  <Text style={{fontSize: 15, paddingLeft: 10, paddingTop: 10}}>
                    {item.description}
                  </Text>
                  <View
                    style={{
                      height: 1,
                      marginTop: 10,
                      backgroundColor: colors.gray01
                    }}
                  />
                </View>
              )}
            />
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 12,
    paddingTop: 9,
    fontWeight: "bold",
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 18,
    paddingLeft: 10
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
