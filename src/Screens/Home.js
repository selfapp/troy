import React, { Component } from "react";
import {Platform, StyleSheet, Text, View, TouchableOpacity, Dimensions} from "react-native";
import { NavigationActions, StackActions } from 'react-navigation'
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export default class Home extends Component {



  static navigationOptions = {
    header: null
  };


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Coming Soon</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
