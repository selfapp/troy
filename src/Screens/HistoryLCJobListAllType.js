
import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  FlatList,
  Alert,
  Dimensions,
} from "react-native";
import MaterialTabs from "react-native-material-tabs";
import Moment from "moment";
import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';
import { NavigationActions, StackActions } from "react-navigation";

const height = Dimensions.get("window").height;

export default class HistoryLCJobListAllType extends Component {
  constructor() {
    super();
    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      auth_token: "",
      projectList: [],
      interestedList: [],
      invitedList: [],
      hiredList: [],
      incompleteList: [],
      completeList: [],
      project_uuid: "",
      FirstName: "",
      LastName: "",
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "State",
      zipcode: "",
      textvalue: "",
      avatarSource: "",
      selectedTab: 0,
      date: "",
      myUserId: "",
      spinner: false,
      gcm_token: "",
      profileUpdated: "false",
      displayOfHiredJobs: "none",
      statusOfHiredJobs: "",
      displayOfIncompleteJobs: "none",
      statusOfIncompleteJobs: "",
      displayOfCompleteJobs: "none",
      statusOfCompleteJobs: "",
      loader:false
    };
  }

  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  apiCallUserProfileUpdate = token => {
    this.setState({loader:true})
    var myObject = {
      user: {
        gcm_token: token
      }
    };
    fetch(`${baseURL}/users/${this.state.uuid}`, {
      method: "PATCH",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.auth_token
      },
      body: JSON.stringify(myObject)
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        try {
          //  AsyncStorage.setItem("userType", "LC");
        } catch (error) {
          // Error saving data
        }
      })
      .catch(e => {
        this.setState({loader:false})
      });

    //
  };

  getInvitedList() {
    for (let i = 0; i < this.state.projectList.length; i++) {
      // console.log("interested List -----" + this.state.projectList[i].workers);
      if (this.state.projectList[i].workers === true) {
        this.state.invitedList.push(this.state.projectList[i]);
      }
      //console.log("interested List" + this.state.projectList[i].workers);
    }
    this.setTab(0);
    return this.state.invitedList;
  }

  setTab(tab) {
    this.setState({selectedTab: tab});
  }

  _retrieveDataForHiredJobs = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});

      }

      fetch(`${baseURL}/lc/jobs/hired`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log("Hired job response")
          console.log(responseJson.jobs)
          this.setState({loader:false})
          this.setState({
            hiredList: responseJson.jobs
          });
          if (this.state.hiredList.length == 0) {
            this.setState({
              statusOfHiredJobs: "No jobs found.",
              displayOfHiredJobs: "flex"
            });
          } else {
            this.setState({displayOfHiredJobs: "none"});
          }
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  validateFields() {
    if (
      this.state.FirstName === "" ||
      this.state.LastName === "" ||
      this.state.EmailId === "" ||
      this.state.StreetAddress === "" ||
      this.state.City === "" ||
      this.state.State === "State" ||
      this.state.zipcode === "" ||
      this.state.avatarSource === ""
    ) {
      Alert.alert("", "License image and fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  componentWillMount() {
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this._retrieveDataForHiredJobs();
    });
  }

  validate = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // this.setState({ email: text });
      return true;
    } else {
      //this.setState({ email: text });
      return true;
    }
  };

  goToHome(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: "MainTabNavigatorLabor"})
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }

  selectedTab() {
    if (this.state.selectedTab == 0) {
      return (
        <View style={{}}>
          <View
            style={{
              height: 50,
              display: this.state.displayOfHiredJobs,
              justifyContent: "center"
            }}
          >
            <Text style={{alignSelf: "center", color: "black", fontSize: 18}}>
              {this.state.statusOfHiredJobs}
            </Text>
          </View>
          <FlatList
            style={{
              marginTop: 0,

              height: height / 2 + 30,
            }}
            data={this.state.hiredList}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={{
                  width: "100%",
                  height: height / 2.2,

                  flexDirection: "column",
                  backgroundColor: colors.gray01
                }}
                onPress={() => {
                  this.fetchJobsInvoices(item.uuid);
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    height: "15%",
                    backgroundColor: colors.gray04
                  }}
                >
                  <Text
                    style={{
                      flex: .4,
                      alignSelf: "center",
                      fontSize: 16,
                      marginLeft: 10,
                      fontWeight: "bold",
                      color: colors.white
                    }}
                  >
                    {item.title}
                  </Text>
                  <Text
                    style={{flex: .7, fontSize: 11, color: colors.white}}
                  >
                    {Moment(item.start_date).format("D MMM YYYY") +
                    " to " +
                    Moment(item.end_date).format("D MMM YYYY")}
                  </Text>
                </View>
                <View
                  style={{
                    padding: 5,
                    marginTop: 15
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: "2%"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 16,
                        fontWeight: "bold"
                      }}
                    >
                      {/* GC email */}
                      Customer email
                    </Text>
                    <Text style={{flex: 1.9}}>{item.email}</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: "2%",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 16,
                        fontWeight: "bold"
                      }}
                    >
                      Job description
                    </Text>
                    <Text style={{flex: 1.6}}>{item.description}</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: "2%"

                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 16,
                        fontWeight: "bold",
                      }}
                    >
                      {/* Gc contact */}
                      Customer contact
                    </Text>
                    <Text style={{flex: 1.6, marginTop: "2%"}}>{item.contact_number}</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: "1%"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 16,

                        fontWeight: "bold"
                      }}
                    >
                      Job address
                    </Text>
                    <Text style={{flex: 1.6, marginTop: "2%",}}>
                      {item.city + " " + item.street_address}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      );
    }
    if (this.state.selectedTab == 1) {
      return (
        <View style={{}}>
          <View
            style={{
              height: 50,
              display: this.state.displayOfCompleteJobs,
              justifyContent: "center"
            }}
          >
            <Text style={{alignSelf: "center", color: "black", fontSize: 18}}>
              {this.state.statusOfCompleteJobs}
            </Text>
          </View>
          <FlatList
            style={{
              marginTop: 1
            }}
            data={this.state.completeList}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={{
                  marginBottom: "4%",
                  width: "100%",
                  borderColor: "red",

                  flexDirection: "column",
                  alignSelf: "center",
                  backgroundColor: colors.white
                }}
                onPress={() => {
                  this.applyLaborOnJobs(item.uuid);
                }}
              >
                <View
                  style={{
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 5,
                    flexDirection: "row",
                    alignItems: "center",
                    backgroundColor: colors.gray04
                  }}
                >
                  <Text
                    style={{
                      flex: 1.5,
                      alignSelf: "center",
                      fontSize: 22,
                      marginLeft: 10,
                      fontWeight: "bold",
                      color: colors.white
                    }}
                  >
                    {item.title}
                  </Text>
                  <Text
                    style={{flex: 1.6, fontSize: 13, color: colors.white}}
                  >
                    {Moment(item.start_date).format("D MMM YYYY") +
                    " to " +
                    Moment(item.end_date).format("D MMM YYYY")}
                  </Text>
                </View>
                <View
                  style={{
                    padding: 5
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: "2%"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 20,
                        fontWeight: "bold"
                      }}
                    >
                      Job email
                    </Text>
                    <Text style={{flex: 1.6}}>{item.email}</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: "2%",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 20,
                        fontWeight: "bold"
                      }}
                    >
                      Job description:
                    </Text>
                    <Text style={{flex: 1.6}}>{item.description}</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 20,
                        fontWeight: "bold",
                        marginTop: "2%"
                      }}
                    >
                      {/* Lc contact : */}
                      Sc contact :
                    </Text>
                    <Text style={{flex: 1.6}}>{item.contact_number}</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: "2%"
                    }}
                  >
                    <Text
                      style={{
                        flex: 1.5,
                        alignSelf: "center",
                        fontSize: 20,
                        fontWeight: "bold"
                      }}
                    >
                      {/* Lc address : */}
                      Sc address :
                    </Text>
                    <Text style={{flex: 1.6}}>
                      {item.city + " " + item.city}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: "2%"
                    }}
                  >
                    <Text style={{flex: 1.6}}/>
                  </View>
                </View>

                <View
                  style={{
                    height: 1,
                    marginTop: 20
                  }}
                />
              </TouchableOpacity>
            )}
          />
        </View>
      );
    }
  }

  handleClick = () => {
    // const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [NavigationActions.navigate({ routeName: "MainTabNavigatorGC" })]
    // });
    // this.props.navigation.dispatch(resetAction);
    this.props.navigation.navigate("MainTabNavigatorLabor");
    //this.props.navigation.dispatch(DrawerActions.openDrawer());
  };

  fetchJobsInvoices = job_uuid => {
    console.log("selcted job uuid",job_uuid)
    try {
      AsyncStorage.setItem("uuid_jobs", job_uuid);
    } catch (error) {
      // Error saving data
      // We have data!!
    }
    // this.props.navigation.navigate("HistoryLCDetails");
    this.props.navigation.navigate("LaborInvoices", {jobUuid:job_uuid});
  };

  handlePeople = () => {
  };

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View style={{flex: 1, alignItems: 'center'}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 50
          }}
        >
          <TouchableOpacity
            style={{width: 30}}
            onPress={this.handleClick.bind(this)}
          >
            <Image
              style={{alignSelf: "flex-start"}}
              source={require("../../src/assets/backnew.png")}
            />
          </TouchableOpacity>

          <View style={{flex: 1, marginTop: 20, height: 35, alignItems: 'center'}}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Jobs status
            </Text>
          </View>
          <TouchableOpacity
            style={{width: 50, height:40, 
              marginRight:-15,
          justifyContent:'center',alignItems:'center'}}
            onPress={()=>this.goToHome(this)}
          >
            <Text style={{fontWeight:'bold', marginTop:7}}>
              HOME
            </Text>
          </TouchableOpacity>
        </View>
        <Text
            style={{
              marginBottom:15,
              color: 'gray',
              fontSize: 12,
              fontWeight: "200",
              textAlign: 'center',
              marginHorizontal:20
            }}
          >
            Note :- Click on job to see invoice list.
          </Text>
          <View
            style={{
              marginTop: ".2%", width: '92%'
            }}
          >

            <View
              style={{
                borderRadius: 10,
                marginLeft: "10%",
                marginRight: "10%",
                marginTop: "3%",
                width: "80%",
                height: height / 2 + 100,
                backgroundColor: colors.gray01
              }}
            >
              <View
                style={{
                  backgroundColor: colors.colorGradient,
                  borderRadius: 10
                }}
              >
                <SafeAreaView style={{marginTop: 10}}>
                  <MaterialTabs
                    items={["Applied jobs"]}
                    selectedIndex={this.state.selectedTab}
                    onChange={this.setTab.bind(this)}
                    barColor={colors.colorGradient}
                    borderRadius={30}
                    textStyle={{fontSize: 15}}
                    uppercase={false}
                    indicatorColor={colors.colorGradient}
                    activeTextColor={colors.white}
                    inactiveTextColor="#33212D"
                  />
                </SafeAreaView>
              </View>
              <View
                style={{
                  paddingBottom: 0,

                  overflow: "visible"
                }}
              >
                {this.selectedTab()}
              </View>
            </View>

            <TouchableOpacity
              style={{
                marginTop: "10%",
                borderRadius: 3,
                marginLeft: "10%",
                marginRight: "10%",
                height: 50,
                width: "80%",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  justifyContent: "center",
                  marginTop: "4%",
                  color: colors.white,
                  fontSize: 20,
                  fontWeight: "bold",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}

