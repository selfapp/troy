
import React, { Component } from "react";
import {
  Image,
  FlatList,
  TouchableOpacity,
  Text,
  Alert,
  TextInput,
  Modal,
  AsyncStorage,
  View
} from "react-native";
import CheckBox from "react-native-checkbox";
import { NavigationActions, StackActions } from "react-navigation";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import Button from "../Components/button";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import { baseURL } from '../../api';

export default class ProjectSelection extends Component {
  constructor() {
    super();
    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      savedPrjId: "",
      stateVar: [],
      auth_token: "",
      projectList: [],
      checked: true,
      loader:false,
      selectedIndex:0,
      checkEdit:false,
      editItem:null,
      modalLoader:false
    };
  }

  componentDidMount() {
    this._retrieveData();
    AsyncStorage.getItem('prjIndex').then((value)=>{
      
      console.log("Project index", value)
      console.log(JSON.parse(value))
      this.setState({selectedIndex:JSON.parse(value)})
      console.log("selected index ....", this.state.selectedIndex)
    })

  }


  handleClick(edit, item) {
    console.log("Check edit ", edit)
      if(edit){
         this.setState({checkEdit:true, editItem:item, ProjectName:item.name})
      }else{
        this.setState({checkEdit:false, editItem:null, ProjectName:''})
      }
      this.toggleModal(true);
  }

  validateFields() {

    if (this.state.ProjectName === "") {
      Alert.alert("", "Fill project name.");
      return false;
    } else {
      return true;
    }
  }

  projectCheckEnabled() {
    this.state.stateVar = new Array(this.state.projectList.length);
    for (let i = 0; i < this.state.stateVar.length; i++) {
      if (this.state.projectList[i].uuid === this.state.savedPrjId) {
        this.state.stateVar[i] = true;
        console.log("Project name called ,.,c,.sa", this.state.projectList[i].name)

        AsyncStorage.setItem("prjName", this.state.projectList[i].name);
      } else {
        this.state.stateVar[i] = false;
      }
    }
    this.setState({ stateVar: this.state.stateVar });
  }

  projectListCheck(index) {
    //this.state.stateVar(this.state.ProjectList.length);
    // console.log("Project list .....")
    // console.log(this.state.projectList)
    // console.log(this.state.stateVar)

    this.state.stateVar = new Array(this.state.projectList.length);

    for (let i = 0; i < this.state.stateVar.length; i++) {
      if (i == index) {
        this.state.stateVar[i] = true;
        try {
          console.log("project list check project id", this.state.projectList[i].uuid)

          AsyncStorage.setItem("prjid", this.state.projectList[i].uuid);
          AsyncStorage.setItem("prjName", this.state.projectList[i].name);
          AsyncStorage.setItem("prjIndex", JSON.stringify(index));
          this.setState({selectedIndex:index})
        } catch (error) {
          // Error saving data
          // We have data!!
        }
      } else {
        this.state.stateVar[i] = false;
      }
    }
    this.getSelectedStateChecked(this.state.stateVar);
  }

  getSelectedStateChecked(array) {
    console.log("selected var ")
    console.log(array)

    this.setState({ stateVar: array });
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const prjid = await AsyncStorage.getItem("prjid");
console.log("retreive data project id", prjid)
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        this.setState({
          uuid: uuid,
          auth_token: access_token,
          savedPrjId: prjid
        });
      }

      fetch(`${baseURL}/projects`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log("List of projects")
          console.log(responseJson.projects)
          this.setState({loader:false})
          this.setState({
            projectList: responseJson.projects
          });
          this.projectCheckEnabled();
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
    }
  };

  moveToDashboard() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "MainTabNavigatorGC" })]
    });
    this.props.navigation.dispatch(resetAction);
  }

  apiCallAddProject() {
    if (this.validateFields()) {
      this.setState({loader:true})
      var myObject = {
        project: {
          name: this.state.ProjectName
        }
      };

      fetch(`${baseURL}/projects`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        },
        body: JSON.stringify(myObject)
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({ ProjectName: "" });
          console.log("api call add data project id", responseJson.project.uuid)

          try {
            console.log("project selction api call project id set", responseJson.project.uuid)

            AsyncStorage.setItem("prjid", responseJson.project.uuid);
            this._retrieveData();
          } catch (error) {
          }

          Alert.alert(
            "",
            "Project Added Successfully.",
            [
              {
                text: "OK",
                onPress: () => this.setState({ modalVisible: false, modalLoader:false })
              }
            ],
            { cancelable: false }
          );
          this._retrieveData();
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } else {
    }
  };

  apiCallEditProject() {

    this.setState({ modalLoader:true})
    console.log("Edit project called .....")
    console.log(this.state.editItem)
    console.log(this.state.editItem.uuid)

    if (this.validateFields()) {
      // this.setState({loader:true})
      var myObject = {
        project: {
          name: this.state.ProjectName
        }
      };

      fetch(`${baseURL}/projects/${this.state.editItem.uuid}`, {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        },
        body: JSON.stringify(myObject)
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({modalLoader:false})
          this.setState({ ProjectName: "", checkEdit:false, editItem:null});
          console.log("api call edit data project id", responseJson.project.uuid)
          try {
            console.log("project selction api call project id set", responseJson.project.uuid)
            // AsyncStorage.setItem("prjid", responseJson.project.uuid);
            this._retrieveData();
          } catch (error) {
          }

          Alert.alert(
            "",
            "Project Edited Successfully.",
            [
              {
                text: "OK",
                onPress: () => this.setState({ modalVisible: false, modalLoader:false })
              }
            ],
            { cancelable: false }
          );
          this._retrieveData();
        })
        .catch(e => {
          this.setState({modalLoader:false})
        });
    } else {
    }
  };

  toggleModal(visible) {
    this.setState({ modalVisible: visible});
  }

  render() {
    return (
      <BackgroundNtScroll>
        <View
          style={{
            position: "absolute",
            top: 50,
            width: "100%"
          }}
        >
          <Modal
            style={{
              alignItems: "center",
              justifyContent: "center"
            }}
            backdropOpacity={30}
            animationType={"slide"}
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => this.setState({modalVisible: false, modalLoader:false})}
          >
              {this.state.modalLoader ? <MyActivityIndicator /> : null}

            <TouchableOpacity
              onPress={() => {
                this.toggleModal(!this.state.modalVisible);
              }}
              style={{
                borderRadius: 10,
                marginLeft: "5%",
                marginRight: "5%",
                marginTop: 50,
                backgroundColor: colors.gray01,
                width: "90%"
              }}
            >
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 20,
                  alignSelf: "center",
                  marginTop: "2%",
                  justifyContent: "center"
                }}
              >
                {this.state.checkEdit ? "Edit project" : "Add project"}
              </Text>
              <TextInput
                style={{
                  height: 40,
                  fontSize: 18,
                  marginVertical: "4%",
                  borderColor: colors.gray05,
                  borderWidth: 1,
                  marginLeft: 10,
                  alignSelf: "center",
                  width: "70%",
                  marginRight: 10,
                  borderRadius: 7
                }}
                underlineColorAndroid={colors.gray01}
                placeholder="Enter project name"
                maxLength={20}
                autoCapitalize="none"
                value={this.state.ProjectName}
                onChangeText={value => this.setState({ ProjectName: value })}
              />

              <View
                style={{
                  alignSelf: "center",
                  width: "40%"
                }}
              >
                <Button
                  marginTop={"1%"}
                  onPress={()=> this.state.checkEdit ? this.apiCallEditProject() : this.apiCallAddProject()}
                  buttonName={this.state.checkEdit ? "Save" : "Add"}
                  backgroundColor={colors.colorYogi}
                  textColor={"white"}
                />

                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    alignSelf: "center",
                    marginTop: "4%",
                    height: 20,
                    justifyContent: "center"
                  }}
                />
              </View>
            </TouchableOpacity>
          </Modal>
        </View>

        <View
          style={{
            height: 85,
            flexDirection: "row",
            marginTop: 20
          }}
        >

          <View style={{ flex: 1, marginTop: 50 }}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Choose Project
            </Text>
          </View>
          <TouchableOpacity style={{ marginTop: 25 }} onPress={()=>this.handleClick(false, '')}>
            <Image
              style={{ alignSelf: "flex-end" }}
              source={require("../../src/assets/add.png")}
            />
          </TouchableOpacity>
        </View>
        <View>
        {this.state.loader ? <MyActivityIndicator /> : null}

          <FlatList
            style={{
              marginTop: 10,
              height: "70%"
            }}
            data={this.state.projectList}
            renderItem={({ item, index }) => (
              <View
                style={{
                  marginLeft: "1%",
                  marginBottom: "4%",
                  marginRight: "1%"
                }}
              >
                <View
                  style={{
                    borderRadius: 10,
                    alignContent: "center",
                    width: "90%",
                    alignItems: "center",
                    borderColor: "red",
                    height: 70,
                    flexDirection: "row",
                    justifyContent: "center",
                    backgroundColor: colors.gray01,
                    alignSelf: "center"
                  }}
                >
                  {/* <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                    Project -
                  </Text> */}
                  <Text
                    style={{
                      color: "black",
                      // justifyContent: "center",
                      // alignItems: "center",
                      fontWeight: "bold",
                      flex: 0.95,
                      paddingLeft: 0
                    }}
                  >
                    {item.name}
                  </Text>
                  <TouchableOpacity style={{
                    width:50,
                    height:40,
                    alignItems:'center',
                    justifyContent:'center',
                    marginRight:3
                  }}onPress={()=> this.handleClick(true, item)}>
                     <Image style={{marginRight:3}}
                      source={require("../../src/assets/edit.png")}
                      />
                  </TouchableOpacity>
                  <CheckBox
                    checkedImage={require("../assets/check.png")}
                    label=""
                    checked={this.state.selectedIndex === index ? true : false}
                    uncheckedImage={require("../assets/uncheck.png")}
                    onChange={checked => this.projectListCheck(index)}
                  />
                </View>
              </View>
            )}
          />
          <View
            style={{
              width: "40%",
              alignContent: "center",
              alignSelf: "center"
            }}
          >
            <Button
              marginTop={"3%"}
              width={"100%"}
              buttonName={"Done"}
              onPress={() => this.moveToDashboard()}
              backgroundColor={colors.colorYogi}
              textColor={"white"}
            />
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}