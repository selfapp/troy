








import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import ModalDropdown from "react-native-modal-dropdown";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  Dimensions,
  ImageBackground,
  StatusBar,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import {API, baseURL} from '../../api';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class Worker extends Component {
  constructor() {
    super();
    this.state = {
      FirstName: "",
      LastName: "",
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "State",
      zipcode: "",
      textvalue: "",
      uuid: "",
      spinner: false,
      auth_token: "",
      avatarSource: "",
      gcm_token: "",
      my_Id: "",
      loader:false,
      checkEditProfile:false,
      checkImageChange:false
    };
  }

  componentWillMount() {
    if(this.props.navigation.state.params){
      if(this.props.navigation.state.params.editProfile){
        this.setState({checkEditProfile:true})
      }
    }
    this._retrieveData();
  }

  validateFields() {
    let {FirstName, LastName, EmailId, StreetAddress, City, State, zipcode, avatarSource} = this.state;
  
    if (
      (FirstName === null || FirstName === undefined) ||
      (LastName === null || LastName === undefined) ||
      (EmailId === null || EmailId === undefined) ||
      (StreetAddress === null || StreetAddress === undefined) ||
      (City === null || City === undefined) ||
      State === "State" ||
      (zipcode === null || zipcode === undefined) ||
      (avatarSource === "" || avatarSource === undefined)
    ){
      Alert.alert("", "Fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  componentDidMount() {
    this.setPushConfig();
  }

  fetchImageFromGallery() {
    var ImagePicker = require("react-native-image-picker");
    ImagePicker.showImagePicker(response => {
      console.log("Image picker response ....")
      console.log(response)
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let data = `data:${response.mime};base64,${response.data}`

        this.setState({
          avatarSource: data,
          checkImageChange:true
        });
      }
    });
  }

  validate = text => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return true;
    } else {
      return true;
    }
  };

  setPushConfig() {
    let _this = this;
    var PushNotification = require("react-native-push-notification");

    PushNotification.configure({
      onRegister: function (token) {
        try {
          AsyncStorage.setItem(
            "gcm_token",
            JSON.parse(JSON.stringify(token.token))
          );
          AsyncStorage.setItem("profileUpdated", "true");
          _this.setState({gcm_token: token.token});
          API.savePushToken(token.token);
        } catch (error) {
        }
      },
      onNotification: function (notification) {
        console.log("notification received worker....")
        console.log(notification)
       
        if (
          notification.foreground === true &&
          notification.userInteraction === false
        ){
            if(Platform.OS === 'ios'){
                Alert.alert("STAFF UP", notification.data.message);
            }
          }
        if (
          notification.foreground === true &&
          notification.userInteraction === true
        ) {
          if (notification.data.code === "001") {
            // _this.props.navigation.navigate("LaborJobList");
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        } else if (
          notification.foreground === false ||
          notification.foreground === null
        ) {
          if (notification.code === "001") {
            // _this.props.navigation.navigate("LaborJobList");
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        }

        // process the notification

        // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
        //  notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      senderID: "900411929042",
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
      popInitialNotification: true,
      requestPermissions: true
    });
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!
        this.setState({
          uuid: uuid,
          access_token: access_token
        });

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "GET",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            this.setState({
              FirstName: responseJson.user.first_name,
              LastName: responseJson.user.last_name,
              EmailId: responseJson.user.email,
              StreetAddress: responseJson.user.street_address,
              City: responseJson.user.city,
              State: responseJson.user.state,
              zipcode: responseJson.user.zip_code,
              role: responseJson.user.role
            });
            if (responseJson.user.file != null) {
              this.setState({avatarSource: responseJson.user.file});
            }

            if (responseJson.user.state == null) {
              this.setState({State: "State"});
            }
          })
          .catch(e => {
            this.setState({loader:false})
          });
      }
    } catch (error) {
      // Error retrieving data
      console.log("called error " + e);
    }
  };

  navigate() {
    if(this.state.checkEditProfile){
      this.props.navigation.navigate("WorkerProfile");
    }else{
      this.props.navigation.navigate("WorkerSchedule");
    }
    this.setState({spinner: false});
  }

  apiCallUserProfileUpdate = () => {
    if (this.validateFields()) {
      if (this.validate(this.state.EmailId)) {
        console.log("called");
        this.setState({spinner: true});
        var myObject;
        if(this.state.checkImageChange){
           myObject = {
            user: {
              first_name: this.state.FirstName,
              last_name: this.state.LastName,
              email: this.state.EmailId,
              street_address: this.state.StreetAddress,
              city: this.state.City,
              state: this.state.State,
              zip_code: this.state.zipcode,
              file: this.state.avatarSource,
              role: "WORKER",
              gcm_token: this.state.gcm_token
            }
          };
        }else{
          myObject = {
            user: {
              first_name: this.state.FirstName,
              last_name: this.state.LastName,
              email: this.state.EmailId,
              street_address: this.state.StreetAddress,
              city: this.state.City,
              state: this.state.State,
              zip_code: this.state.zipcode,
              role: "WORKER",
              gcm_token: this.state.gcm_token
            }
          };
        }

        

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "PATCH",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          },
          body: JSON.stringify(myObject)
        })
          .then(response => response.json())
          .then(responseJson => {
            try {
              AsyncStorage.setItem("userType", "WK");
            } catch (error) {
              // We have data!!
            }


            Alert.alert(
              "",
              "Profile updated Successfully.",
              [
                {
                  text: "OK",
                  onPress: () =>
                    this.navigate()
                }
              ],
              {cancelable: false}
            );
          })
          .catch(e => {
            this.setState({spinner: false});
            Alert.alert(
              "",
              e,
              [
                {
                  text: "OK",
                }
              ],
              {cancelable: false}
            );
          });

        //
      } else {
        Alert.alert("", "Please enter correct Email address.");
      }
    } else {
    }
  };

  handleClick = () => {
    if(this.state.checkEditProfile){
      this.props.navigation.navigate("WorkerProfile");
    }else{
      this.props.navigation.navigate("People");
    }
  };

  render() {

    var keyboardBehavior = ''
    if(Platform.OS === 'ios'){
      keyboardBehavior = 'padding'
    }else {
      keyboardBehavior = 'height'
    }
    
    return (
      <ImageBackground
        source={require("../../src/assets/bg-screen.png")}
        resizeMode="stretch"
        style={{flex: 1}}
      >
        <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated/>
        <Spinner
            visible={this.state.spinner}
            textContent={"Updating profile and setting, please wait..."}
            textStyle={styles.spinnerTextStyle}
          />
        <View
          style={{
            height: 30,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 50, alignItems: 'center'
          }}
        >
          <TouchableOpacity
            style={{flex: 0.4}}
            onPress={this.handleClick.bind(this)}
          >
            <Image style={{}} source={require("../../src/assets/backnew.png")}/>
          </TouchableOpacity>
        </View>

        <View style={{height: 40, alignItems: "center", marginTop: -5}}>
          <Text
            style={{
              color: colors.black,
              fontSize: 24,
              fontWeight: "bold"
            }}
          >
            {this.state.checkEditProfile ? "Edit Profile" : "Register"}
          </Text>
        </View>
        {this.state.checkEditProfile ? (null) : (
          <View style={{height: 40, alignItems: "center",}}>
            <Text
              style={{
                color: colors.gray02,
                fontSize: 16,
              }}
            >
              As a worker
            </Text>
          </View>
        )}

        <KeyboardAvoidingView behavior={keyboardBehavior} enabled>
         
        <ScrollView
          scrollEventThrottle={700}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={"handled"}
          keyboardDismissMode={"interactive"}
        >
                  {this.state.loader ? <MyActivityIndicator /> : null}

          <View style={{height: 850}}>

            <View
              style={{
                borderRadius: 10,
                marginLeft: "10%",
                marginRight: "10%",
                width: "80%",
                height: height / 2 + 100,
                marginTop: -20
              }}
            >
              <TextInput
                style={{
                  marginTop: 15,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10,
                }}
                underlineColorAndroid={colors.gray01}
                placeholder="First Name"
                autoCapitalize="none"
                value={this.state.FirstName}
                onChangeText={value => this.setState({FirstName: value})}
              />
              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.LastName}
                onChangeText={value => this.setState({LastName: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="Last Name"
                autoCapitalize="none"
              />
              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.EmailId}
                onChangeText={value => this.setState({EmailId: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="Email Id"
                autoCapitalize="none"
                keyboardType={'email-address'}
              />

              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.StreetAddress}
                onChangeText={value => this.setState({StreetAddress: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="Street Address"
                autoCapitalize="none"
              />
              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.City}
                onChangeText={value => this.setState({City: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="City"
                autoCapitalize="none"
              />

              <View
                style={{
                  marginTop: 5,
                  height: "11%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row"
                  }}
                >
                  <ModalDropdown
                    style={{
                      flex: 0.4,
                      height: height * 0.07,
                      justifyContent: "center"
                    }}
                    options={[
                      "Alabama - AL",
                      "Alaska - AK",
                      "Arizona - AZ",
                      "Arkansas - AR",
                      "California - CA",
                      "Colorado - CO",
                      "Connecticut - CT",
                      "Delaware - DE",
                      "Florida - FL",
                      "Georgia - GA",
                      "Hawaii - HI",
                      "Idaho - ID",
                      "Illinois - IL",
                      "Indiana - IN",
                      "Iowa - IA",
                      "Kansas - KS",
                      "Kentucky - KY",
                      "Louisiana - LA",
                      "Maine - ME",
                      "Maryland - MD",
                      "Massachusetts - MA",
                      "Michigan - MI",
                      "Minnesota - MN",
                      "Mississippi - MS",
                      "Missouri - MO",
                      "Montana - MT",
                      "Nebraska - NE",
                      "Nevada - NE",
                      "New Hampshire - NH",
                      "New Jersey - NJ",
                      "New Mexico - NM",
                      "New York - NY",
                      "North Carolina - NC",
                      "North Dakota - ND",
                      "Ohio - OH",
                      "Oklahoma - OK",
                      "Oregon - OR",
                      "Pennsylvania - PA",
                      "Rhode Island - RI"
                    ]}
                    dropdownStyle={{
                      width: "40%",
                      fontWeight: "bold",
                    }}
                    defaultValue={this.state.State}
                    onSelect={(index, value) => this.setState({State: value})}
                    textStyle={{
                      paddingLeft: 10,
                      fontSize: 20,
                      color: colors.gray09
                    }}
                  />
                  <View style={{flex: 0.2}}/>
                  <View style={{flex: 0.4}}>
                    <TextInput
                      style={{
                        fontSize: 18,
                        marginLeft: 10,
                        marginRight: 10
                      }}
                      placeholder="Zip code"
                      keyboardType='number-pad'
                      autoCapitalize="none"
                      maxLength={6}
                      value={this.state.zipcode}
                      onChangeText={value => this.setState({zipcode: value})}
                    />
                  </View>
                </View>
              </View>

              <View
                style={{
                  height: 1,
                  marginLeft: 13,
                  marginRight: 13
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 0.4, backgroundColor: colors.gray01}}/>
                  <View style={{flex: 0.2}}/>
                  <View style={{flex: 0.4, backgroundColor: colors.gray01}}/>
                </View>
              </View>
              {
                this.state.avatarSource.length > 0 ? (
                  <Image style={{
                      marginTop:10,
                      height:120,
                      width:120,  
                      marginLeft:width/2 - 100     
                  }} source={{uri:this.state.avatarSource}}>
                  </Image>
                ) : (null)
              }
              <View
                style={{
                  height: "10%",
                  justifyContent: "center",
                  backgroundColor: colors.gray02,
                  width: "60%",
                  borderRadius: 30,
                  alignSelf: "center",
                  marginTop: 10
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.fetchImageFromGallery();
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      alignSelf: "center",
                      justifyContent: "center",
                      fontSize: 14
                    }}
                  >
                    Upload license
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <TouchableOpacity
              style={{
                marginTop: 80,
                borderRadius: 30,
                marginLeft: "10%",
                marginRight: "10%",
                backgroundColor: colors.colorYogi,
                height: 50,
                width: "80%",
                alignItems: "center",
                marginBottom:30
              }}
              onPress={this.apiCallUserProfileUpdate.bind(this)}
            >
              <Text
                style={{
                  justifyContent: "center",
                  marginTop: "4%",
                  height: 50,
                  color: colors.white,
                  fontSize: 20,
                  fontWeight: "bold",
                  alignSelf: "center"
                }}
              >
                {this.state.checkEditProfile ? "Update Profile" : "Create Account"}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  spinnerTextStyle: {
    color: colors.colorGradient,
    backgroundColor: colors.colorSignin,
    height: 100,
    borderRadius: 5,
    padding: 10,
    width: width - 60
  }
});

