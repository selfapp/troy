import React, {Component} from "react";
import {
  Dimensions,
  Image,
  KeyboardAvoidingView,
  Text,
  Alert,
  TextInput,
  BackHandler,
  View,
  Keyboard,
  TouchableOpacity
} from "react-native";
import {AsyncStorage} from "react-native";

import colors from "../styles/colors";
import Button from "../Components/button";
import Background from "../Components/Background";
import {API} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: "",
      loader: false
    };
  }

  componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  handleClick = () => {
    // Alert.alert("", "Handle click called")
    this.apiCallSendOtp();
  };

  validateFields() {
    if (this.state.phoneNumber === "") {
      Alert.alert("oijoij", "Enter you mobile number.");
      return false;
    } else {
      return true;
    }
  }

  backPressed = () => {
    Alert.alert(
      "Exit App",
      "Do you want to exit?",
      [
        {
          text: "No",
          onPress: () => {
          },
          style: "cancel"
        },
        {text: "Yes", onPress: () => BackHandler.exitApp()}
      ],
      {cancelable: false}
    );
    return true;
  };

  // Login Method
   apiCallSendOtp = async () => {
    // Alert.alert("asas", "apiCallSendOtp called")
    //  var _this = this;
    if (this.state.phoneNumber === "") {
      Alert.alert("", "Enter you mobile number.");
    } else {

      // Alert.alert("iuhiuh","Enter into else")
      Keyboard.dismiss();
      // this.state({ loader: true })
      let myObject = {
        user: {
          country_code: "91",
          contact_number: this.state.phoneNumber
        }
      };
     
      try {
        this.setState({loader:true})
        let response = await API.request('/send_otp.json', 'post', myObject, null);
        console.log("Login response ....")
        console.log(response)
        try {
          response.json().then((responseJson) => {
            this.setState({loader:false})
            if (!responseJson.status == false) {
              try {
                // this.state({ loader: false })
                AsyncStorage.setItem("phoneNumber", this.state.phoneNumber);
              } catch (error) {
                // this.state({ loader: false })
              }
              this.props.navigation.navigate("VerificationCode");
            } else {
              Alert.alert(
                "",
                responseJson.message,
                [
                  {
                    text: "OK"
                  }
                ],
                {cancelable: false}
              );
            }
          });
        } catch (error) {
          this.setState({loader:false})
          Alert.alert(
            "",
            error + "",
            [
              {
                text: "OK"
              }
            ],
            {cancelable: false}
          );
        }
      } catch (error) {
       
        this.setState({loader:false})
        // this.state({ loader: false })
        Alert.alert(error)
      }
    }
  }

  render() {
    return (
      <Background>
        {this.state.loader ? <MyActivityIndicator /> : null}
        <KeyboardAvoidingView
          // keyboardVerticalOffset={Platform.select({ ios: 10, android: -80 })}
          contentContainerStyle={{flex: 1}}
          behavior="position"
          enabled
        >
          {/* {this.state.loader ? <MyActivityIndicator /> : null} */}

          <View style={{flexDirection: "column", height: height / 3.5}}>
          </View>
          <View style={{flexDirection: "column",}}>

            <View style={{flexDirection: "column", flex: .3}}>
            </View>
            <View style={{flexDirection: "column", flex: .6}}>

              <Image
                style={{alignSelf: "center"}}
                source={require("../../src/assets/logo.png")}
              />
            </View>

          </View>

          <View>


            <View
              style={{

                borderRadius: 10,
                backgroundColor: "transparent",
                margin: 15,
                borderColor: "transparent",
                paddingVertical: 15
              }}
            >
              <Text style={{marginLeft: 10, textAlign: "left", marginTop: "5%"}}>
                Mobile no.
              </Text>
              <TextInput
                style={{
                  marginTop: 5,
                  height: 45,
                  fontSize: 18,
                  borderColor: "transparent",
                  borderWidth: 1,
                  marginLeft: 10,
                  marginRight: 10
                }}
                underlineColorAndroid="transparent"
                placeholderTextColor="#FF242424"
                keyboardType={"numeric"}
                maxLength={10}
                autoCapitalize="none"
                value={this.state.phoneNumber}
                onChangeText={phoneNumber => this.setState({phoneNumber: phoneNumber.toString()})}
              />
              <View style={{backgroundColor: '#FF242424', height: 1, width: '100%'}}/>
            </View>
          </View>

          <Button
            marginTop={"15%"}
            width={"90%"}
            buttonName={"Request verification code"}
            
            // onPress={() => this.handleClick.bind(this)}
            onPress={() => this.apiCallSendOtp()}
            backgroundColor={colors.colorYogi}
            textColor={"white"}
          />
           {/* <TouchableOpacity
      onPress={() => this.apiCallSendOtp()}
      // disabled={props.disabled}
      style={{
        marginTop: "15%",
        borderRadius: 30,
        marginLeft: "5%",
        marginRight: "5%",
        backgroundColor: colors.colorYogi,
        height: 50,
        width: "90%",
      }}
    >
          <View style={{ alignItems: "center", justifyContent: 'center', height: 50 }}>
            <Text
              style={{
                color: "white",
                
                fontSize: 16,
                justifyContent: "center",
                alignSelf: "center"
              }}
            >
              Request verification code
            </Text>
          </View>
        
    </TouchableOpacity> */}
          <View
            style={{
              height: 70,
              flexDirection: "row",
              marginTop: "5%",
              width: width
            }}
          />
        </KeyboardAvoidingView>
      </Background>
    );
  }
}
