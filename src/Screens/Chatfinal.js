import React, { Component } from "react";
import {
  Dimensions,
  Platform,
  Image,
  ImageBackground,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Text,
  View
} from "react-native";
import colors from "../styles/colors";
const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;
import Background from "../Components/Background";
import MessageList from "../lib/ce-view-messagelist";
import styles from "../lib/ce-theme-style";

import MessageEntry from "../lib/ce-view-messagelist";
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";

export default class Chatfinal extends React.Component {
  constructor(props) {
    super(props);
  }

  renderContents() {
    const chatRoomModel = ChatEngineProvider.getChatRoomModel();
    let self = this;

    return (
      <View>
        <MessageList
          ref="MessageList"
          navigation={self.props.navigation}
          now={ChatEngineProvider.getChatRoomModel().state.now}
          chatRoomModel={ChatEngineProvider.getChatRoomModel()}
        />
        <MessageEntry
          chatRoomModel={ChatEngineProvider.getChatRoomModel()}
          typingIndicator
          keyboardVerticalOffset={80}
        />
      </View>
    );
  }

  render() {
    if (Platform.OS === "ios") {
      return (
        <SafeAreaView style={styles.container}>
          <KeyboardAvoidingView behavior="padding" enabled>
            {this.renderContents()}
          </KeyboardAvoidingView>
        </SafeAreaView>
      );
    }

    return <View style={styles.container}>{this.renderContents()}</View>;
  }
}
