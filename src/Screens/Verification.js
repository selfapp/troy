import React, { Component } from "react";
import {
    Dimensions,
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput, TouchableOpacity,
    View
} from "react-native";
import colors from "../styles/colors";
import Button from "../Components/button";
import Background from '../Components/Background';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;


export default class Verification extends Component {
    handleClick = () => {
        this.props.navigation.navigate('VerificationCode');
    }


    render() {

        return (
            <Background>
                <KeyboardAvoidingView
                    keyboardVerticalOffset={Platform.select({ ios: 60, android: -80 })}
                    contentContainerStyle={{ alignItems: 'center', flex: 1 }} behavior='position' enabled>
                    <Image
                        style={{
                            justifyContent: "center",
                            marginTop: "8%",
                            alignItems: "center"
                        }}
                        source={require("../../src/assets/staffup-logo.png")}
                    />

                    <View
                        style={{
                            borderRadius: 10,
                            margin: '15%',
                            width: '80%', height: '65%',
                            backgroundColor: colors.white
                        }}
                    >
                        <Image
                            style={{
                                justifyContent: "center",
                                marginTop: "8%",
                                alignSelf: "center"
                            }}
                            source={require("../../src/assets/circular.png")}
                        />


                        <Text style={{ textAlign: 'center', marginTop: '10%', fontSize: 16, color: 'grey' }}>
                            We have sent verification code{"\n"} to recover your account on your{"\n"}
                            phone.For any further help,{"\n"} contact <Text style={{ color: 'black' }}>
                                support@troy.com
                            </Text>

                        </Text>
                        <TouchableOpacity onPress={this.handleClick.bind(this)}>
                            <View style={styles.ButtonView}>
                                <Text style={{ color: '#FFFFFF', fontSize: 18 }}>OK</Text>
                            </View>
                        </TouchableOpacity>
                    </View>


                    <View style={{
                        flexDirection: 'row',
                        marginTop: '10%',
                        width: width
                    }}>



                    </View>

                </KeyboardAvoidingView>
            </Background>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF"
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5
    },
    ButtonView: {
        backgroundColor: 'red',
        height: 35,
        marginHorizontal: '20%',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
});
