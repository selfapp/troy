import React, {Component} from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import colors from "../styles/colors";
import Moment from "moment";
import MyActivityIndicator from '../Components/activity_indicator';
const height = Dimensions.get("window").height;
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';

export default class SelectJobForInvoice extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
      uuid: "",
      auth_token: "",
      SelectProjectData: [],
      projectListRaw: [],
      projectList: [],
      ProjectNameAsUUID: "",
      ProjectName: "Select Project",
      project_uuid: "",
      spinner: false,
      display: "none",
      status: "",
      loader:false
    };
  }

  handleClick = () => {
    {
      this.props.navigation.navigate("TimeSheetOrInvoice");
    }
  };

  slideMenu = () => {
    {
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };

  _retrieveDataForProjectList = async () => {
    try {
      this.setState({loader:true})
      const access_token = await AsyncStorage.getItem("access_token");

      this.setState({auth_token: access_token});

      fetch(`${baseURL}/projects`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + access_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({
            projectListRaw: responseJson.projects
          });
          if (this.state.projectListRaw == null) {
            this.props.navigation.navigate("StartProject");
          } else {
            try {
              console.log("select job for invoice api call project id set", this.state.projectListRaw[0].uuid)
//rajeev
             // AsyncStorage.setItem("prjid", this.state.projectListRaw[0].uuid);
              this._retrieveData();
            } catch (error) {
            }
          }
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
    }
  };

  _retrieveData = async index => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");

      const access_token = await AsyncStorage.getItem("access_token");
      const project_id = await AsyncStorage.getItem("prjid");
      if (uuid !== null) {
        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
      }
      fetch(`${baseURL}/projects/${project_id}/jobs`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({
            projectList: responseJson.jobs
          });
          if (this.state.projectList.length === 0) {
            this.setState({status: "No Jobs found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount() {
    this._retrieveDataForProjectList();
    this._retrieveData();
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this._retrieveData();
    });
  }

  selectedJob(position) {
    try {
      AsyncStorage.setItem("job_uuid", this.state.projectList[position].uuid);
      this.props.navigation.navigate("SelectedInvoicesGC");
      //  this._retrieveData();
    } catch (error) {
      // Error saving data
      // We have data!!
    }
  }

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View style={{flex: 1, alignItems: 'center'}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              height: 50,
              flexDirection: "row",
              marginTop: 20, width: '100%'
            }}
          >
            {
              <TouchableOpacity
                style={{marginTop: 30, marginLeft: 10}}
                onPress={this.handleClick.bind(this)}
              >
                <Image
                  // style={{ alignSelf: "flex-start" }}
                  source={require("../../src/assets/backnew.png")}
                />
              </TouchableOpacity>
            }
          </View>
          <Text
            style={{
              marginTop: -5,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center', width: 150
            }}
          >
            Select Job
          </Text>
          <View
            style={{
              borderRadius: 10,
              marginLeft: "10%",
              marginRight: "10%",
              marginTop: "3%",
              width: "80%",
              height: height / 2 + 100,
              backgroundColor: 'white'
            }}
          >
            <View
              style={{
                flex: 1,
                display: this.state.display,
                justifyContent: "center"
              }}
            >
              <Text style={{alignSelf: "center", color: "black", fontSize: 18}}>
                {this.state.status}
              </Text>
            </View>
            {/* <View
            style={{
              height: "10%",
              borderTopRightRadius: 10,
              borderTopLeftRadius: 10,

              flexDirection: "row",
              backgroundColor: colors.colorSignin
            }}
          >
            <ModalDropdown
              style={{
                marginTop: 5,
                flex: 2,

                borderColor: "transparent",
                marginLeft: 10,
                marginRight: 10,
                borderWidth: 1,

                justifyContent: "center"
              }}
              options={this.state.SelectProjectData}
              dropdownStyle={{
                width: "40%",
                height: "25%",
                fontWeight: "bold",
                placeholderTextColor: colors.white
              }}
              defaultValue={this.state.ProjectName}
              onSelect={(index, value) => this._retrieveData(index)}
              // onChangeText={this._retrieveData()}
              textStyle={{
                paddingLeft: 10,
                fontSize: 20,
                height: 30,
                fontWeight: "bold",
                placeholderTextColor: colors.blue,
                color: colors.white
              }}
            />

            <TouchableOpacity
              style={{
                flex: 0.3,
                marginTop: "2%",
                height: 30
              }}
            >
              <Image
                style={{
                  alignSelf: "center",
                  marginTop: 15
                }}
                source={require("../../src/assets/down-arrow.png")}
              />
            </TouchableOpacity>
          </View> */}

            <FlatList
              style={{marginTop: 1}}
              data={this.state.projectList}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  style={{
                    borderRadius: 10,
                    marginBottom: "4%",
                    width: "100%",
                    borderColor: "red",
                    height: 100,
                    alignSelf: "center",
                    backgroundColor: colors.gray01
                  }}
                  onPress={() => {
                    this.selectedJob(index);
                  }}
                >
                  <Text style={styles.dateText}>
                    {Moment(item.start_date).format("D MMM YYYY") +
                    "  to  " +
                    Moment(item.end_date).format("D MMM YYYY")}
                  </Text>
                  <Text style={styles.titleText}>{item.title}</Text>
                  <Text style={{fontSize: 15, paddingLeft: 10, paddingTop: 10}}>
                    {item.description}
                  </Text>
                  <View
                    style={{
                      height: 1,
                      marginTop: 10,
                      backgroundColor: colors.gray01
                    }}
                  />
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 12,
    paddingTop: 9,
    fontWeight: "bold",
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 18,
    paddingLeft: 10
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
