import React, {Component} from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Alert,
  View
} from "react-native";
import Moment from "moment";
import CheckBox from "react-native-checkbox";

import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import colors from "../styles/colors";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;

export default class ProposalApplicantHireWorker extends Component {
  constructor() {
    super();

    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      checked: true,
      auth_token: "",
      jobs_uuid: "",
      jobs_uuid_application: "",
      projectList: [],
      workersAddedList: [],
      project_uuid: "",
      loader:false
      // projectList = [
      //   { name: "Project 1" },
      //   { name: "Project 2" },
      //   { name: "Project 3" },
      //   { name: "Project 4" },
      //   { name: "Project 5" },
      //   { name: "Project 6" }
      // ]
    };
  }

  handleClick = () => {
    {
      this.props.navigation.navigate("JobApplicantLaborConList");
    }
  };

  componentDidMount() {
    this._retrieveData();
  }

  changeCheckState(workers_uuid, ischecked) {
    //workersAddedList.
    if ((ischecked = true)) {
      this.state.workersAddedList.push(workers_uuid);
      //this.setState({ projectList: this.state.projectList });
    }
  }

  getListOfWorker() {
    //workersAddedList.
    // this.state.workersAddedList.push(workers_uuid);
    // console.log("workers uuid is " + workers_uuid);

    var myobject;
    for (let i = 0; i < this.state.workersAddedList; i++) {
    }
    return myobject;
  }

  apiCallMarkWokerAgainstJobTest() {
    var myObject2 = {
      job_worker: {
        job_application_id: this.state.jobs_uuid,
        workers: this.state.workersAddedList
      }
    };
  }

  apiCallMarkWokerAgainstJob() {

    if(this.state.workersAddedList.length > 0){
      console.log("Workers uuid list ")
      console.log(this.state.workersAddedList)
      this.setState({loader:true})
      var myObject = {
        job_worker: {
          // job_application_id: this.state.jobs_uuid_application,
          workers: this.state.workersAddedList
        }
      };
  
      fetch(`${baseURL}/job-applications/${this.state.jobs_uuid_application}/workers`,
        {
          method: "PATCH",
  
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.auth_token
          },
          body: JSON.stringify(myObject)
        }
      )
        .then(response => response.json())
        .then(responseJson => {

          console.log("response hire worker")
          console.log(responseJson)
          this.setState({loader:false})
          // this.setState({ ProjectName: "" });
  
          try {
            // AsyncStorage.setItem("prjid", responseJson.project.uuid);
          } catch (error) {
            // Error saving data
            // We have data!!
          }
  
          Alert.alert(
            "",
            "Hired successfully.",
            [
              {
                text: "OK",
                onPress: () =>
                  this.props.navigation.navigate("JobApplicantLaborConList")
              }
            ],
            {cancelable: false}
          );
        })
        .catch(e => {
          this.setState({loader:false})
        });
    }else{
      alert("Please select atleast one worker.")
    }
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const jobs_uuid_application = await AsyncStorage.getItem(
        "uuid_jobs_application"
      );

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
        this.setState({jobs_uuid_application: jobs_uuid_application});
      }

      fetch(`${baseURL}/job-applications/${jobs_uuid_application}/workers`,
        {
          method: "GET",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.auth_token
          }
        }
      )
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          console.log("workers list...")
          console.log(responseJson.workers)
          //      this.state.workersAddedList.push(workers_uuid);

          if(responseJson.workers.length) {

            var workerUUID = []
            responseJson.workers.map((item)=>{
            workerUUID.push(item.uuid)
            }).then(
              this.setState({workersAddedList:workerUUID, projectList: responseJson.workers})
            )
            // this.setState({
            //   projectList: responseJson.workers,
            // });
          }
          
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
       
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 40, alignItems: 'center'
          }}
        >
          <TouchableOpacity
            style={{width: 30}}
            onPress={this.handleClick.bind(this)}
          >
            <Image
              style={{alignSelf: "flex-start"}}
              source={require("../../src/assets/backnew.png")}
            />
          </TouchableOpacity>

          <View style={{flex: 1, marginTop: 20}}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Hire workers
            </Text>
          </View>
        </View>

        <View
          style={{
            marginTop: "1%"
          }}
        >

{this.state.loader ? <MyActivityIndicator /> : null}

          <View
            style={{
              borderRadius: 10,
              marginLeft: "10%",
              marginRight: "10%",
              marginTop: "4%",
              width: "80%",
              height: height / 2 + 40,
              backgroundColor: colors.white
            }}
          >
            <FlatList
              style={{marginTop: 1, height: "10%"}}
              data={this.state.projectList}
              renderItem={({item, index}) => (
                <TouchableOpacity 
                  style={{
                    borderRadius: 10,
                    marginBottom: "4%",
                    width: "100%",
                    borderColor: "red",
                    flexDirection: "column",
                    alignSelf: "center",
                    backgroundColor: colors.gray01,

                  }} disabled={true}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      height: 40,
                      margin: 5,
                      alignItems: "center",
                      justifyContent: 'center'
                    }}
                  >
                    {/* <CheckBox
                    isDisabled = {true}
                      // checkedImage={require("../assets/check.png")}
                      checkedImage={require("../assets/check.png")}
                      label=""
                      // checked={item.assigned}
                      uncheckedImage={require("../assets/uncheck.png")}
                      onChange={checked => this.changeCheckState(item.uuid)}
                    /> */}
                    <Image source={require('../assets/check.png')}>
                    </Image>

                    <Text
                      style={{
                        flex: 2,
                        marginLeft: 12,
                        fontSize: 16,
                        alignContent: "center",
                        fontWeight: "bold"
                      }}
                    >
                      {item.first_name}
                    </Text>
                    <Text style={{flex: 1.8}}>
                      {Moment(item.start_date).format("D MMM YYYY")}
                    </Text>
                  </View>
                  {/* <View
                    style={{
                      height: 1,
                      marginTop: 7,
                      backgroundColor: colors.gray01
                    }}
                  /> */}
                </TouchableOpacity>
              )}
            />
          </View>
        </View>

        <View
          style={{
            height: 40,
            justifyContent: "center",
            backgroundColor: colors.colorYogi,
            width: "40%",
            borderRadius: 20,
            alignSelf: "center",
            marginTop: 25
          }}
        >
          <TouchableOpacity
            onPress={() => {
              //this.props.navigation.navigate("PROPOSAL");

              this.apiCallMarkWokerAgainstJob();
            }}
          >
            <Text
              style={{
                color: "white",
                alignSelf: "center",
                justifyContent: "center",
                fontSize: 16
              }}
            >
              Hire
            </Text>
          </TouchableOpacity>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 14,
    paddingTop: 5,
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 16,
    paddingLeft: 10
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
