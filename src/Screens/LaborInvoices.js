import React, {Component} from "react";
import {
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View,
  Dimensions
} from "react-native";
import {DrawerActions} from "react-navigation";
import Moment from "moment";

import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {API} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';
import { NavigationActions, StackActions } from "react-navigation";
import {baseURL} from '../../api';

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class LaborInvoices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      auth_token: "",
      projectList: [],
      project_uuid: "",
      status: "",
      display: "none",
      loader:false,
      jobUuid:''
    };
  }

  getInvoiceId = invoice_uuid => {
    try {
      AsyncStorage.setItem("uuid_invoice", invoice_uuid);
    } catch (error) {}

    if(this.state.jobUuid.length > 0){
      this.props.navigation.navigate("LaborInvoiceDetails" ,{showHome:true});
    }else{
      this.props.navigation.navigate("LaborInvoiceDetails");
    }
    
  };

  handleClick = () => {
    {
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };
  handleBack = () => {
      this.props.navigation.goBack();
    }

  _retrieveData = async () => {
    this.setState({loader:true})
    const uuid = await AsyncStorage.getItem("uuid");
    const access_token = await AsyncStorage.getItem("access_token");

    if (uuid !== null) {
      this.setState({uuid: uuid});
      this.setState({auth_token: access_token});
    }

    if(this.props.navigation.state.params){

      this.setState({jobUuid:this.props.navigation.state.params.jobUuid})

      fetch(`${baseURL}/lc/invoices/job/${this.props.navigation.state.params.jobUuid}`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log("Hired job response")
          console.log(responseJson.jobs)
          this.setState({loader:false})
          this.setState({
            projectList: responseJson.invoices
          });
          if (this.state.projectList.length === 0) {
            this.setState({status: "No invoices found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
        })
        .catch(e => {
          this.setState({loader:false})
        });


    }else{

      API.getInvoicesLc()
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        this.setState({
          projectList: responseJson.invoices
        }, () => {
          if (this.state.projectList.length === 0) {
            this.setState({status: "No invoices found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
        });
      })
      .catch(e => {this.setState({loader:false})});
    }
    
   
  };

  componentDidMount() {
    this._retrieveData();
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this._retrieveData();
    });
  }

  goToHome(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: "MainTabNavigatorLabor"})
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View
          style={{
            height: 85,
            flexDirection: "row",
            marginTop: 20,
          }}
        >
          {
            this.state.jobUuid.length > 0 ? (
              <TouchableOpacity style={{width: 50, marginTop: 30, marginLeft: 10}}
              onPress={this.handleBack.bind(this)}>
<Image

source={require("../../src/assets/backnew.png")}
/>
</TouchableOpacity>
            ) : (
              <TouchableOpacity style={{width: 50, marginTop: 30, marginLeft: 10}}
              onPress={this.handleClick.bind(this)}>
<Image

source={require("../../src/assets/h-menu.png")}
/>
</TouchableOpacity>
            )
           
          }

          <View style={{width:DEVICE_WIDTH-110, marginTop: 40,
          alignItems:'center',
          }}>
          
            <Text
              style={{
                // flex: 1,
                color: colors.black,
                fontSize: 24,
                fontWeight: "400"
              }}
            >
              Invoice
            </Text>
          </View>
          {
            this.state.jobUuid.length > 0 ? (
              <TouchableOpacity
              style={{width: 50, 
                marginRight:-15,
            justifyContent:'center',alignItems:'center'}}
              onPress={()=>this.goToHome(this)}
            >
              <Text style={{fontWeight:'bold', marginTop:7}}>
                HOME
              </Text>
            </TouchableOpacity>
            ) : (null)
          }
         
          
        </View>
        <Text
            style={{
              marginTop: 5,
              color: 'gray',
              fontSize: 11,
              fontWeight: "200",
              marginHorizontal:15,
              textAlign: 'center', 
              marginBottom: 10
            }}
          >
            Note :- Click invoice to see invoice details.
          </Text>
         

        <View
          style={{
            flex: 1,
            display: this.state.display,
            justifyContent: "center"
          }}
        >
          <Text style={{alignSelf: "center", color: "black", fontSize: 18}}>
            {this.state.status}
          </Text>
        </View>
        {this.state.loader ? <MyActivityIndicator /> : null}
        <FlatList
          style={{marginTop: 1}}
          data={this.state.projectList}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => {
                this.getInvoiceId(item.uuid);
              }}
              style={{
                borderRadius: 10,
                marginBottom: "4%",
                elevation: 10,
                width: "90%",
                borderColor: "red",
                height: 100,
                alignSelf: "center",
                backgroundColor: colors.gray01
              }}
            >
              <Text style={styles.dateText}>
                {Moment(item.from).format("D MMM YYYY") +
                "  to  " +
                Moment(item.to).format("D MMM YYYY")}
              </Text>
              <View
                style={{flexDirection: "row", alignContent: "flex-start"}}
              >
                <Text style={styles.titleText}>Job title:</Text>
                <Text
                  style={{
                    fontSize: 14,
                    paddingLeft: 2,
                    paddingTop: 10,
                    alignContent: "flex-start"
                  }}
                >
                  {item.job}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 12,
    paddingTop: 9,
    fontWeight: "bold",
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 15,
    paddingLeft: 10,
    flex: 0.4,

    paddingTop: 10
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
