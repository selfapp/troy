import React, {Component} from "react";
import {NavigationActions, StackActions} from "react-navigation";
import {
  View,
  Image,
  AsyncStorage,
  StatusBar,
  ImageBackground
} from "react-native";

export default class Login extends Component {
  componentDidMount() {
    this._retrieveData();
    console.disableYellowBox = true;
  }

  _retrieveData = async () => {
    try {
      const type = await AsyncStorage.getItem("userType");
      this.handleClick(type);
    } catch (error) {
      // Error retrieving data
    }
  };

  handleClick(userType) {
    console.log("Usertype is", userType)
    let resetAction;
    if (userType === "GC") {
      resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({routeName: "MainTabNavigatorGC"})
        ]
      });
    } else if (userType === "LC") {
      resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({routeName: "MainTabNavigatorLabor"})
        ]
      });
    } else if (userType === "WORKER") {
      resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: "MainTabNavigator"})]
      });
    } else {
      resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: "Otp"})]
      });
    }
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <ImageBackground
        source={require("../../src/assets/bg-screen.png")}
        resizeMode="stretch"
        style={{flex: 1}}
      >
        <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated/>
        <View style={{flexDirection: "column", flex: 1}}>

          <View style={{flexDirection: "column", flex: .3}}>


          </View>
          <View style={{flexDirection: "column", flex: .6}}>

            <Image
              style={{alignSelf: "center"}}
              source={require("../../src/assets/logo.png")}
            />
          </View>
        </View>
      </ImageBackground>
    );
  }
}
