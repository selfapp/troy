import React, {Component} from "react";
import {
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import {DrawerActions} from "react-navigation";
import Moment from "moment";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';

export default class SelectedInvoicesGC extends Component {
  constructor() {
    super();
    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      auth_token: "",
      projectList: [],
      project_uuid: "",
      status: "",
      display: "none",
      loader:false
    };
  }

  getInvoiceId = invoice_uuid => {
    try {
      AsyncStorage.setItem("uuid_invoice", invoice_uuid);
    } catch (error) {
    }
    this.props.navigation.navigate("AprroveRejectInvoice");
  };

  handleClick = () => {
    {
      this.props.navigation.navigate("SelectedInvoice");
    }
  };

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const project_id = await AsyncStorage.getItem("prjid");
      const job_id = await AsyncStorage.getItem("job_uuid");

      if (uuid !== null) {
        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
      }

      fetch(`${baseURL}/gc/invoices/${job_id}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          // responseJson.jobs.from_date.substring(0, 5);
          this.setState({
            projectList: responseJson.invoices
          });
          if (this.state.projectList.length === 0) {
            console.log("length is xxx", this.state.projectList.length);

            this.setState({status: "No invoices found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount() {
    this._retrieveData();
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this._retrieveData();
    });
  }

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
       
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 40, alignItems: 'center'
          }}
        >
          <TouchableOpacity
            style={{width: 30}}
            onPress={this.handleClick.bind(this)}
          >
            <Image
              style={{alignSelf: "flex-start"}}
              source={require("../../src/assets/backnew.png")}
            />
          </TouchableOpacity>

          <View style={{flex: 1, marginTop: 20}}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Select Invoice
            </Text>
          </View>
        </View>

        <View
          style={{
            flex: 1,
            display: this.state.display,
            justifyContent: "center"
          }}
        >
          <Text style={{alignSelf: "center", color: "black", fontSize: 20}}>
            {this.state.status}
          </Text>
        </View>
        {this.state.loader ? <MyActivityIndicator /> : null}
        <FlatList
          style={{marginTop: 1}}
          data={this.state.projectList}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => {
                this.getInvoiceId(item.uuid);
              }}
              style={{
                borderRadius: 10,
                marginBottom: "4%",
                width: "90%",
                borderColor: "red",
                height: 100,
                alignSelf: "center",
                backgroundColor: colors.gray01,
                marginTop: 20
              }}
            >
              <Text style={styles.dateText}>
                {Moment(item.from).format("D MMM YYYY") +
                "  to  " +
                Moment(item.to).format("D MMM YYYY")}
              </Text>
              <View
                style={{flexDirection: "row", alignContent: "flex-start"}}
              >
                <Text style={styles.titleText}>Job title :</Text>
                <Text
                  style={{
                    fontSize: 14,
                    paddingLeft: 2,
                    paddingTop: 10,
                    alignContent: "flex-start"
                  }}
                >
                  {item.job}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 12,
    paddingTop: 9,
    fontWeight: "bold",
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 15,
    paddingLeft: 10,
    flex: 0.4,

    paddingTop: 10
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
