import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import ModalDropdown from "react-native-modal-dropdown";
import {
  StyleSheet,
  View,
  Modal,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  Dimensions,
  ImageBackground,
  StatusBar
} from "react-native";
import DatePicker from "react-native-datepicker";

import colors from "../styles/colors";
import Button from "../Components/button";
import {baseURL} from '../../api';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'


const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class AddJobs extends Component {
  constructor() {
    super();
    this.state = {
      Title: "",
      skill: "",
      Desc: "",
      Mobile: "",
      EmailId: "",
      FromDate: "",
      ToDate: "",
      startTime:"",
      endTime: "",
      contactPhone: "",
      contactName: "",
      spinner: false,
      Street: "",
      City: "",
      savedPrj_id: "",
      State: "State",
      ProjectList: [],
      SelectProjectData: [],
      ZipCode: "",
      ProjectName: "Select Project",
      ProjectUuid: "",
      uuid: "",
      modalVisible: false,
      auth_token: "",
      project_uuid: "no_Project"
    };
  }

  validateFields() {
    if (
      this.state.Title === "" ||
      this.state.Desc === "" ||
      this.state.noOfWorker === "" ||
      this.state.StreetAddress === "" ||
      this.state.City === "" ||
      this.state.State === "State" ||
      this.state.FromDate === "" ||
      this.state.ToDate === "" ||
      this.state.startTime === "" ||
      this.state.contactName === "" ||
      this.state.contactPhone === "" ||
      (this.state.zipcode === null || this.state.zipcode === undefined)
    ) {
      Alert.alert("", "All fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  getInterestedList() {
    for (let i = 0; i < this.state.projectList.length; i++) {
      this.state.SelectProjectData.push(this.state.projectList[i].name);
    }

    return this.state.interestedList;
  }

  componentWillMount() {
    this._retrieveData();
    this._retrieveDataForProjectList();
  }

  validate = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // this.setState({ email: text });
      return true;
    } else {
      //this.setState({ email: text });
      return true;
    }
  };

  _retrieveData = async () => {
    try {
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const project_id = await AsyncStorage.getItem("prjid");
      if (uuid !== null) {
        // We have data!!
        this.setState({uuid: uuid});
        this.setState({access_token: access_token});
        //this.setState({ project_uuid: project_id });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  navigateTo() {
    // this.props.navigation.navigate("JobsList")
    // this.props.navigation.navigate("JobsApplicantList");
    this.props.navigation.navigate("SelectContractorForJob", {jobId:this.state.ProjectUuid});

    this.setState({spinner: false});
  }

  _retrieveDataForProjectList = async () => {
    try {
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const project_id = await AsyncStorage.getItem("prjid");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
        this.setState({savedPrj_id: project_id});
      }

      fetch(`${baseURL}/projects`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            projectList: responseJson.projects
          }),
            //  console.log(this.state.SelectProjectData);
            this.getInterestedList();
        })
        .catch(e => {
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  apiCallUserProfileUpdate = () => {
    if (this.validateFields()) {
      if (this.validate(this.state.EmailId)) {
        this.setState({spinner: true});

        var myObject = {
          job: {
            title: this.state.Title,
            description: this.state.Desc,
            start_date: this.state.FromDate,
            end_date: this.state.ToDate,
            no_of_worker: this.state.noOfWorker,
            street_address: this.state.StreetAddress,
            city: this.state.City,
            state: this.state.State,
            zip_code: this.state.zipcode,
            skill_level: this.state.skill,
            start_time: this.state.startTime,
            end_time: this.state.endTime,
            contact_name: this.state.contactName,
            contact_no: this.state.contactPhone
          }
        };

        //api call for profile update
        fetch(
          `${baseURL}/projects/${this.state.savedPrj_id}/jobs`,
          {
            method: "POST",

            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Token token=" + this.state.access_token
            },
            body: JSON.stringify(myObject)
          }
        )
          .then(response => response.json())
          .then(responseJson => {
            console.log("response adding job ....")
            console.log(responseJson)

            this.setState({ProjectUuid:responseJson.job.uuid})
            if (responseJson.message == "Project not found.") {
              Alert.alert(
                "",
                "First add or select a project.",
                [
                  {
                    text: "OK"
                    // onPress: () => this.props.navigation.navigate("ProjectList")
                  }
                ],
                {cancelable: false}
              );
            }
            if (responseJson.message == "Job is created successfully.") {

              Alert.alert(
                "",

                "Jobs added Successfully.",
                [
                  {
                    text: "OK",
                    onPress: () =>
                      this.navigateTo()
                  }
                ],
                {cancelable: false}
              );
            }
          })
          .catch(e => {
            this.setState({spinner: falses});
          });

        //
      } else {
        Alert.alert("", "Please enter correct Email address.");
      }
    } else {
    }
  };
  selectProject = () => {
    {
      // <TouchableOpacity onPress={this.handleClick.bind(this)}></TouchableOpacity>
      this.toggleModal(true);
    }
  };

  handleClick = () => {
    console.log("back button called ....")
    this.props.navigation.navigate("JobsApplicantList");
  };

  handlePeople = () => {
  };

  toggleModal(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <ImageBackground
        source={require("../../src/assets/bg-screen.png")}
        resizeMode="stretch"
        style={{flex: 1}}
      >
        <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated/>

        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginTop: 20, width: '100%'
          }}
        >
          <TouchableOpacity
            style={{marginTop: 30, marginLeft: 10}}
            onPress={this.handleClick.bind(this)}
          >
            <Image style={{}} source={require("../../src/assets/backnew.png")}/>
          </TouchableOpacity>
        </View>
        <Text
          style={{
            marginTop: -5,
            color: 'black',
            fontSize: 24,
            fontWeight: "400",
            textAlign: 'center'
          }}
        >
          Add job
        </Text>

        <Modal
          style={{
            alignItems: "center",
            justifyContent: "center"
          }}
          backdropOpacity={30}
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
          }}
        >

          <TouchableOpacity
            onPress={() => {
              this.toggleModal(!this.state.modalVisible);
            }}
            style={{
              borderRadius: 10,
              marginLeft: "5%",
              marginRight: "5%",
              marginTop: "15%",
              backgroundColor: 'white',
              width: "90%"
            }}
          >
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 20,
                alignSelf: "center",
                marginTop: "2%",
                justifyContent: "center"
              }}
            >
              Add project
            </Text>
            <TextInput
              style={{
                height: 40,
                fontSize: 18,
                marginVertical: "4%",
                borderColor: colors.white,
                borderWidth: 1,
                marginLeft: 10,
                alignSelf: "center",

                width: "70%",
                marginRight: 10,
                borderRadius: 7
              }}
              underlineColorAndroid='transparent'
              placeholder="Enter project name"
              maxLength={20}
              autoCapitalize="none"
              value={this.state.ProjectName}
              onChangeText={value => this.setState({ProjectName: value})}
            />
            <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>


            <View
              style={{
                alignSelf: "center",
                width: "40%"
              }}
            >
              <Button
                marginTop={"1%"}
                //onPress={this.apiCallAddProject.bind(this)}
                buttonName={"Add"}
                backgroundColor={colors.colorSignin}
                textColor={"white"}
              />

              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 20,
                  alignSelf: "center",
                  marginTop: "4%",
                  height: 20,
                  justifyContent: "center"
                }}
              />
            </View>
          </TouchableOpacity>
        </Modal>

        <KeyboardAwareScrollView
          // scrollEventThrottle={650}
          // showsVerticalScrollIndicator={false}
          // keyboardShouldPersistTaps={"handled"}
          // keyboardDismissMode={"interactive"}
        >
          <View style={{flex: 1, alignItems: 'center'}}>
            <View
              style={{
                borderRadius: 10,
                // marginLeft: "7%",
                // marginRight: "7%",
                marginTop: "5%",
                // width: "80%",
                width: width - 60,
                backgroundColor: colors.white
              }}
            >
              
              <Spinner
                visible={this.state.spinner}
                textContent={"Adding job, please wait..."}
                textStyle={styles.spinnerTextStyle}
              />
              <TextInput
                style={{
                  marginTop: "5%",

                  fontSize: 18,
                  borderColor: colors.white,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                  // borderWidth: 1
                }}
                maxLength={24}
                value={this.state.Title}
                underlineColorAndroid='transparent'
                placeholder="Job Title"
                autoCapitalize="none"
                onChangeText={value => this.setState({Title: value})}
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>
              
              <TextInput
                style={{
                  marginTop: "5%",
                  fontSize: 18,
                  borderColor: colors.white,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 5,
                }}
                maxLength={24}
                value={this.state.skill}
                underlineColorAndroid='transparent'
                placeholder="Skill Level"
                autoCapitalize="none"
                onChangeText={value => this.setState({skill: value})}
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>

              <TextInput
                style={{
                  marginTop: "7%",
                  marginBottom: 5,
                  // height: "14%",
                  fontSize: 18,
                  borderColor: colors.white,
                  // borderWidth: 1,
                  marginLeft: 10,
                  marginRight: 10
                }}
                maxLength={80}
                numberOfLines={3}
                value={this.state.Desc}
                onChangeText={value => this.setState({Desc: value})}
                underlineColorAndroid='transparent'
                placeholder="Description"
                autoCapitalize="none"
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>

              <TextInput
                style={{
                  marginTop: "5%",
                  marginBottom: 5,
                  display: "none",
                  height: "10%",
                  fontSize: 18,
                  borderColor: colors.white,
                  borderWidth: 1,
                  marginLeft: 10,
                  marginRight: 10
                }}
                maxLength={12}
                keyboardType="numeric"
                value={this.state.Mobile}
                onChangeText={value => this.setState({Mobile: value})}
                underlineColorAndroid='transparent'
                placeholder="Mobile"
                autoCapitalize="none"
              />
              {/* <View style={{ backgroundColor: colors.gray03, height: 1, width: '100%' }} /> */}

              <TextInput
                style={{
                  marginTop: "5%",
                  marginBottom: 5,
                  display: "none",
                  height: "10%",
                  fontSize: 18,
                  borderColor: colors.white,
                  borderWidth: 1,
                  marginLeft: 10,
                  marginRight: 10
                }}
                maxLength={40}
                value={this.state.EmailId}
                onChangeText={value => this.setState({EmailId: value})}
                underlineColorAndroid='transparent'
                placeholder="Email"
                autoCapitalize="none"
              />
              {/* <View style={{ backgroundColor: colors.gray03, height: 1, width: '100%' }} /> */}

              <View
                style={{
                  marginTop: 5,

                  height: "8%",

                  fontSize: 18,
                  borderColor: colors.white,
                  borderWidth: 1,
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 5, marginTop: 10, marginBottom: 10}}>
                    <DatePicker
                      date={this.state.FromDate}
                      mode="date"
                      placeholder="Start date"
                      format="YYYY-MM-DD"
                      minDate={new Date(Date.now())}
                      // maxDate="2020-06-01"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 4,

                          marginLeft: 0
                        },
                        dateInput: {
                          borderColor: colors.white,
                          marginLeft: 3
                        }
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={date => {
                        this.setState({FromDate: date});
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 2,

                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        justifyContent: "center",
                        alignSelf: "center",
                        fontWeight: "bold",
                        fontSize: 16
                      }}
                    >
                      to
                    </Text>
                  </View>
                  <View style={{flex: 5, marginTop: 10, marginBottom: 10}}>
                    <DatePicker
                      style={{}}
                      date={this.state.ToDate}
                      mode="date"
                      placeholder="End date"
                      format="YYYY-MM-DD"
                      minDate={new Date(Date.now())}
                      // maxDate="2020-06-01"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 4,
                          marginLeft: 0
                        },
                        dateInput: {
                          marginLeft: 3,
                          borderColor: "transparent"
                        }
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={date => {
                        this.setState({ToDate: date});
                      }}
                    />
                  </View>
                </View>

              </View>
              <View
                style={{
                  height: 1,
                  backgroundColor: 'white',
                  // borderColor: colors.white,
                  // borderWidth: 1,
                  marginTop: 20,
                  marginLeft: 0,
                  marginRight: 0
                }}
              >
                <View
                  style={{
                    flex: 12,
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 6, backgroundColor: colors.gray03,}}/>
                  <View style={{flex: 2}}/>
                  <View style={{flex: 6, backgroundColor: colors.gray03,}}/>
                </View>
              </View>

 {/* Time */}
            <View
                style={{
                  marginTop: 5,
                  height: "8%",
                  fontSize: 18,
                  borderColor: colors.white,
                  borderWidth: 1,
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 5, marginTop: 10, marginBottom: 10}}>
                    <DatePicker
                      date={this.state.startTime}
                      mode="time"
                      placeholder="Start time"
                      format="hh:mm A"
                      // minDate={new Date(Date.now())}
                      // maxDate="2020-06-01"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconComponent={<Image source={require('../assets/clock.png')} style={{height: 25, width: 25, tintColor: colors.colorYogi, position: "absolute",
                      left: 3,
                      top: 6,
                      marginLeft: 0}}/>}
                      customStyles={{
                        // dateIcon: {
                        //   position: "absolute",
                        //   left: 0,
                        //   top: 4,
                        //   marginLeft: 0
                        // },
                        dateInput: {
                          borderColor: colors.white,
                          marginLeft: 3
                        }
                      }}
                      onDateChange={time => {
                        this.setState({startTime: time});
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 2,

                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        justifyContent: "center",
                        alignSelf: "center",
                        fontWeight: "bold",
                        fontSize: 16
                      }}
                    >
                      to
                    </Text>
                  </View>
                  <View style={{flex: 5, marginTop: 10, marginBottom: 10}}>
                    <DatePicker
                      style={{}}
                      date={this.state.endTime}
                      mode="time"
                      placeholder="End time"
                      format="hh:mm A"
                      // minDate={new Date(Date.now())}
                      // maxDate="2020-06-01"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconComponent={<Image source={require('../assets/clock.png')} style={{height: 25, width: 25, tintColor: colors.colorYogi, position: "absolute",
                      left: 3,
                      top: 6,
                      marginLeft: 0}}/>}
                      customStyles={{
                        // dateIcon: {
                        //   position: "absolute",
                        //   left: 0,
                        //   top: 4,
                        //   marginLeft: 0
                        // },
                        dateInput: {
                          marginLeft: 3,
                          borderColor: "transparent"
                        }
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={time => {
                        this.setState({endTime: time});
                      }}
                    />
                  </View>
                </View>

              </View>


{/* End Time */}
              <View
                style={{
                  height: 1,
                  backgroundColor: 'white',
                  // borderColor: colors.white,
                  // borderWidth: 1,
                  marginTop: 20,
                  marginLeft: 0,
                  marginRight: 0
                }}
              >
                <View
                  style={{
                    flex: 12,
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 6, backgroundColor: colors.gray03,}}/>
                  <View style={{flex: 2}}/>
                  <View style={{flex: 6, backgroundColor: colors.gray03,}}/>
                </View>
              </View>
              <TextInput
                style={{
                  marginTop: "7%",
                  marginBottom: 5,
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.noOfWorker}
                keyboardType='number-pad'
                onChangeText={value => this.setState({noOfWorker: value})}
                underlineColorAndroid='transparent'
                placeholder="Number of worker required"
                autoCapitalize="none"
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>
{/* Contact Detail */}
       <TextInput
                style={{
                  marginTop: "7%",
                  marginBottom: 5,
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                // maxLength={40}
                value={this.state.contactName}
                onChangeText={value => this.setState({contactName: value})}
                underlineColorAndroid='transparent'
                placeholder="Contact Name"
                autoCapitalize="none"
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>

              <TextInput
                style={{
                  marginTop: "7%",
                  marginBottom: 5,
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                 maxLength={10}
                value={this.state.contactPhone}
                onChangeText={value => this.setState({contactPhone: value})}
                underlineColorAndroid='transparent'
                keyboardType='phone-pad'
                placeholder="Contact Phone Number"
                autoCapitalize="none"
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>

              <TextInput
                style={{
                  marginTop: "7%",
                  marginBottom: 5,
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                // maxLength={40}
                value={this.state.StreetAddress}
                onChangeText={value => this.setState({StreetAddress: value})}
                underlineColorAndroid='transparent'
                placeholder="Street Address"
                autoCapitalize="none"
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>

              <TextInput
                style={{
                  marginTop: "7%",
                  marginBottom: 5,
                  // height: "13%",
                  fontSize: 18,
                  // borderColor: colors.white,
                  // borderWidth: 1,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.City}
                // maxLength={20}
                onChangeText={value => this.setState({City: value})}
                underlineColorAndroid='transparent'
                placeholder="City"
                autoCapitalize="none"
              />
              <View style={{backgroundColor: colors.gray03, height: 1, width: '100%'}}/>

              <View
                style={{
                  flexDirection: "row",
                  marginTop: 20,
                  fontSize: 18,
                }}
              >
                <ModalDropdown
                  style={{
                    flex: 1,
                    backgroundColor: "white",
                    // height: height * 0.06,

                    justifyContent: "center",
                  }}
                  options={[
                    "Alabama - AL",
                    "Alaska - AK",
                    "Arizona - AZ",
                    "Arkansas - AR",
                    "California - CA",
                    "Colorado - CO",
                    "Connecticut - CT",
                    "Delaware - DE",
                    "Florida - FL",
                    "Georgia - GA",
                    "Hawaii - HI",
                    "Idaho - ID",
                    "Illinois - IL",
                    "Indiana - IN",
                    "Iowa - IA",
                    "Kansas - KS",
                    "Kentucky - KY",
                    "Louisiana - LA",
                    "Maine - ME",
                    "Maryland - MD",
                    "Massachusetts - MA",
                    "Michigan - MI",
                    "Minnesota - MN",
                    "Mississippi - MS",
                    "Missouri - MO",
                    "Montana - MT",
                    "Nebraska - NE",
                    "Nevada - NE",
                    "New Hampshire - NH",
                    "New Jersey - NJ",
                    "New Mexico - NM",
                    "New York - NY",
                    "North Carolina - NC",
                    "North Dakota - ND",
                    "Ohio - OH",
                    "Oklahoma - OK",
                    "Oregon - OR",
                    "Pennsylvania - PA",
                    "Rhode Island - RI"
                  ]}
                  dropdownStyle={{
                    width: "40%",
                    fontWeight: "bold",
                  }}
                  defaultValue={this.state.State}
                  onSelect={(index, value) => this.setState({State: value})}
                  //onChangeText={value => this.setState({ City: value })}
                  textStyle={{
                    paddingLeft: 10,
                    fontSize: 20,
                    color: colors.gray09
                  }}
                />
                <View style={{flex: 0.2}}/>
                <View style={{flex: 1}}>
                  <TextInput
                    style={{
                      fontSize: 18,
                      marginLeft: 10,
                      paddingBottom: 5,
                      marginRight: 10
                    }}
                    placeholder="Zip code"
                    keyboardType='number-pad'
                    autoCapitalize="none"
                    maxLength={5}
                    value={this.state.zipcode}
                    onChangeText={value => this.setState({zipcode: value})}
                  />
                </View>
              </View>

              <View
                style={{
                  height: 1,
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 0.4, backgroundColor: colors.gray03}}/>
                  <View style={{flex: 0.2}}/>
                  <View style={{flex: 0.4, backgroundColor: colors.gray03}}/>
                </View>
              </View>
            </View>

            <TouchableOpacity
              style={{
                marginTop: "10%",
                borderRadius: 30,
                marginLeft: "10%",
                marginRight: "10%",
                backgroundColor: colors.colorYogi,
                height: 50,
                width: "80%",
                alignItems: "center"
              }}
              onPress={this.apiCallUserProfileUpdate.bind(this)}
            >
              <Text
                style={{
                  justifyContent: "center",
                  color: colors.white,
                  fontSize: 20,
                  paddingTop: 10,
                  height: 50,
                  fontWeight: "bold",
                  alignSelf: "center"
                }}
              >
                Save
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginTop: "4%",

                height: 50,

                width: "80%",
                alignItems: "center"
              }}
              onPress={this.apiCallUserProfileUpdate.bind(this)}
            />

          </View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  spinnerTextStyle: {
    color: colors.colorGradient,
    backgroundColor: colors.colorSignin,
    height: 100,
    borderRadius: 5,
    padding: 10,
    width: width - 60
  }
});
