import React, {Component} from "react";
import {
  Text,
  View,
} from "react-native";
import {Keyboard} from "react-native";

import colors from "../styles/colors";
import RoundedButton from "../Components/RoundedButton";

export default class People extends Component {
  constructor() {
    super();
    this.state = {
      WorkerDisplay: "",
      OtherDisplay: ""
    };
  }

  handleClick = () => {
    this.props.navigation.navigate("GeneralContractor");
  };

  componentDidMount() {
    Keyboard.dismiss();
  }

  saveUserType = (userType, propsType) => {
    try {
      if (propsType === "GC") {
        this.props.navigation.navigate("GeneralContractor");
      } else if (propsType === "LC") {
        this.props.navigation.navigate("LaborContractor");
      } else if (propsType === "WK") {
        this.props.navigation.navigate("Worker");
      }
    } catch (error) {
    }
  };

  render() {
    return (
      <View style={{backgroundColor: colors.gray01, flex: 1}}
      >
        <View style={{alignItems: "center", marginTop: 10}}>
          <Text
            style={{
              justifyContent: "center",
              marginTop: "25%",
              alignItems: "center",
              fontSize: 20,
              fontWeight: "bold"
            }}

          >Create your profile</Text>
        </View>

        <View
          style={{
            marginTop: "15%",
            margin: "5%",
            height: "30%",

            flexDirection: "row"
          }}
        >
          <View
            style={{
              flex: 1,
              margin: 10,
              alignContent: "center"
            }}
          >
            <RoundedButton
              onPress={() => {
                this.saveUserType("GC", "GC");
              }}
              buttonName={"CUSTOMER"}
              showImage
              width={"100%"}
              backgroundColor={colors.white}
              textColor={"white"}
              imageSource={require("../../src/assets/customer.png")}
            />
          </View>

          <View
            style={{
              flex: 1,
              margin: 10
            }}
          >
            <RoundedButton
              onPress={() => {
                this.saveUserType("LC", "LC");
              }}
              buttonName={"STAFFING COMPANY"}
              showImage
              backgroundColor={colors.white}
              textColor={"white"}
              imageSource={require("../../src/assets/staffing-company.png")}
            />
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            marginTop: "5%",
            height: "30%",
          }}
        >
          <View
            style={{
              flex: 1,
              alignContent: "center"
            }}
          >
          </View>
          <View
            style={{
              flex: 1.3,
              alignContent: "center"
            }}
          >
            <RoundedButton
              onPress={() => {
                this.saveUserType("WK", "WK");
              }}
              buttonName={"WORKER"}
              showImage
              backgroundColor={colors.white}
              textColor={"white"}
              imageSource={require("../../src/assets/worker.png")}
            />
          </View>
          <View
            style={{
              flex: 1,
              alignContent: "center"

            }}
          >
          </View>
        </View>
      </View>
    );
  }
}
