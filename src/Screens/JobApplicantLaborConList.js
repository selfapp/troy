import React, { Component } from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Alert,
  View
} from "react-native";
import Moment from "moment";
import Spinner from "react-native-loading-spinner-overlay";
import colors from "../styles/colors";
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class JobApplicantLaborConList extends Component {
  constructor() {
    super();

    this.state = {
      ProjectName: "",
      modalVisible: false,
      uuid: "",
      checked: true,
      auth_token: "",
      jobs_uuid: "",
      projectList: [],
      workersAddedList: [],
      project_uuid: "",
      myUserId: "",
      spinner: false,
      loader:false
    };
  }

  componentDidMount() {
    this._retrieveData();
  }
  
  handleClick = () => {
    {
      this.props.navigation.navigate("JobsApplicantList");
    }
  };
 
  changeCheckState(workers_uuid, ischecked) {
    //workersAddedList.
    if ((ischecked = true)) {
      this.state.workersAddedList.push(workers_uuid);
      //this.setState({ projectList: this.state.projectList });
    }
  }

  getListOfWorker() {
    var myobject;
    for (let i = 0; i < this.state.workersAddedList; i++) { }
    return myobject;
  }

  apiCallMarkWokerAgainstJobTest() {
    var myObject2 = {
      job_worker: {
        job_application_id: this.state.jobs_uuid,
        workers: this.state.workersAddedList
      }
    };
  }

  apiCallMarkWokerAgainstJob() {
    this.setState({loader:true})
    var myObject = {
      job_worker: {
        job_application_id: this.state.jobs_uuid,
        workers: this.state.workersAddedList
      }
    };

    fetch(`${baseURL}/job-workers/`, {
      method: "POST",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.auth_token
      },
      body: JSON.stringify(myObject)
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        // this.setState({ ProjectName: "" });

        try {
          // AsyncStorage.setItem("prjid", responseJson.project.uuid);
        } catch (error) {
          // Error saving data
          // We have data!!
        }

        Alert.alert(
          "",
          "Successfully marked on Job as Interested.",
          [
            {
              text: "OK",
              onPress: () => this.props.navigation.navigate("PROPOSAL")
            }
          ],
          { cancelable: false }
        );
      })
      .catch(e => {
        this.setState({loader:false})
      });

    //
  }
  gotoChat(channel) {
    var self = this;
    this.setState({ spinner: false });

    ChatEngineProvider.getChatRoomModel()
      .connect(channel)
      .then(
        () => {
          self.props.navigation.navigate("ChatStart", {
            title: "#123" + channel
          });
        },
        reject => {
          alert(reject);
        }
      );
  }

  setPubNubOnce() {
    this.setState({ spinner: true });

    setTimeout(() => {
      ChatEngineProvider.connect(
        this.state.myUserId,
        this.state.myUserId
      ).then(async () => {
        try {

          AsyncStorage.setItem(
            ChatEngineProvider.ASYNC_STORAGE_USERDATA_KEY,
            JSON.stringify({
              username: "santosh",
              name: "santosh"
            })
          );
          this.setState({ spinner: false });

          //  this.props.navigation.navigate("Jobs");
          // this.props.navigation.navigate("PROPOSAL");
        } catch (error) {
          // ignore error, this save is just for convenience
        }

        // this.props.navigation.navigate('ChatList', {});
      });
    }, 1000);
  }

  setPubNub(index) {
    this.setState({ spinner: true });

    var channelName = "";
    if (this.state.projectList[index].lc_id > this.state.myUserId) {
      channelName =
        this.state.myUserId + "" + this.state.projectList[index].lc_id;
    } else {
      channelName =
        this.state.projectList[index].lc_id + "" + this.state.myUserId;
    }
    this.gotoChat(channelName);
  }

  sendApplicantDataLc = job_application_uuid => {

    try {
      AsyncStorage.setItem("uuid_jobs_application", job_application_uuid);
    } catch (error) {
      // Error saving data
      // We have data!!
    }
    this.props.navigation.navigate("JobApplicantHireWorker");
  };

  _retrieveData = async () => {
    try {
      // this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const jobs_uuid = await AsyncStorage.getItem("uuid_jobs");
      const myUserid = await AsyncStorage.getItem("my_id");

      if (uuid !== null) {
        // We have data!!

        this.setState({ uuid: uuid });
        this.setState({ auth_token: access_token });
        this.setState({ jobs_uuid: jobs_uuid });
        this.setState({ myUserId: myUserid });
      }
      this.setPubNubOnce();

      fetch(`${baseURL}/jobs/${jobs_uuid}/lcs`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          // this.setState({loader:false})
          console.log("repsosne jkdfld")
          console.log(responseJson)
          this.setState({
            projectList: responseJson.job_applications
          });
        })
        .catch(e => {
          // this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };
  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
       
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 40, alignItems: 'center'
          }}
        >
          <TouchableOpacity onPress={this.handleClick.bind(this)}>
            <Image
              style={{ alignSelf: "flex-start" }}
              source={require("../../src/assets/backnew.png")}
            />
          </TouchableOpacity>

          <View style={{ flex: 1, marginTop: 20 }}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Contractor list
            </Text>
          </View>
        </View>
        <Text
            style={{
              marginTop:15,
              color: 'gray',
              fontSize: 12,
              fontWeight: "200",
              textAlign: 'center',
              marginHorizontal:20
            }}
          >
            Note :- Click offer to accept or click chat icon to read message.
          </Text>
        <View
          style={{
            marginTop: "0.5%"
          }}
        >

{this.state.loader ? <MyActivityIndicator /> : null}

          <View
            style={{
              borderRadius: 10,
              marginLeft: "10%",
              marginRight: "10%",
              marginTop: "10%",
              width: "80%",
              height: height / 2 + 100,
              backgroundColor: colors.white
            }}
          >
            <Spinner
              visible={this.state.spinner}
              textContent={"Loading chat settings..."}
              textStyle={styles.spinnerTextStyle}
            />
            <FlatList
              style={{ marginTop: 1, height: "10%" }}
              data={this.state.projectList}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  onPress={() => {
                    this.sendApplicantDataLc(item.uuid);
                  }}
                  style={{
                    borderRadius: 10,
                    marginBottom: "4%",
                    width: "100%",
                    borderColor: "red",
                    flexDirection: "column",
                    alignSelf: "center",
                    backgroundColor: colors.gray01
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      height: 70,

                      margin: 10,

                      alignItems: "center"
                    }}
                  >
                    <View
                      style={{
                        flex: 2.5,

                        flexDirection: "column"
                      }}
                    >
                      <Text
                        style={{
                          flex: 0.6,
                          fontSize: 19,
                          fontWeight: "bold"
                        }}
                      >
                        {item.name}
                      </Text>
                      <Text
                        style={{
                          flex: 0.4
                        }}
                      >
                        {item.location}
                      </Text>
                      <Text
                        style={{
                          flex: 0.4
                        }}
                      >
                        ${item.price}/hr per person
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,

                        flexDirection: "row"
                      }}
                    >
                      <TouchableOpacity
                        style={{
                          flex: 0.5,
                          marginTop: 8,
                          height: 30
                        }}
                      >
                        <Image
                          style={{
                            alignSelf: "center",
                            alignItems: "center",
                            alignContent: "center"
                          }}
                          source={require("../../src/assets/green-check.png")}
                        />
                      </TouchableOpacity>
                      <View
                        style={{
                          flex: 0.03,
                          backgroundColor: colors.gray01
                        }}
                      />
                      <TouchableOpacity
                        style={{
                          flex: 0.4,
                          marginLeft: 4,
                          marginTop: 6,
                          height: 30
                        }}
                        onPress={() => {
                          this.setPubNub(index);
                        }}
                      >
                        <Image
                          style={{
                            alignSelf: "center",
                            alignItems: "center",
                            alignContent: "center"
                          }}
                          source={require("../../src/assets/chat.png")}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View
                    style={{
                      height: 1,
                      marginTop: 7,
                      backgroundColor: colors.gray01
                    }}
                  />
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 14,
    paddingTop: 5,
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 16,
    paddingLeft: 10
  },
  spinnerTextStyle: {
    color: colors.colorGradient,
    backgroundColor: colors.colorSignin,
    height: 100,
    borderRadius: 5,
    padding: 10,
    width: width - 60
  },
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
