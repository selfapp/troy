import React, {Component} from "react";
import {
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import {DrawerActions} from "react-navigation";
import Moment from "moment";
import MyActivityIndicator from '../Components/activity_indicator';

import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';

export default class JobApplicantJobList extends Component {
  constructor() {
    super();
    this.state = {
      projectName: "",
      modalVisible: false,
      uuid: "",
      auth_token: "",
      projectList: [],
      project_uuid: "",
      display: "none",
      status: "",
      refreshing: false,
      loader:false
    };
  }

  componentWillMount() {
    this._retrieveData();
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this._retrieveData();
    });
  }

  getListLCs = (job_uuid, jobApplicantStatus, Jobstatus) => {

    console.log("jobApplicantStatus is", jobApplicantStatus)
    if(jobApplicantStatus && Jobstatus === "Incomplete"){
      try {
        AsyncStorage.setItem("uuid_jobs", job_uuid);
      } catch (error) {
        // Error saving data
        // We have data!!
      }
      this.props.navigation.navigate("JobApplicantLaborConList");
    }else if(Jobstatus === "Hired"){
      alert('Applicants already hired for this job.')
    }
    else {
      alert("No applicant applied for this job.")
    }

    
  };

  handleClick = () => {
    {
      // this.props.navigation.navigate("SelectContractorForJob");

      this.props.navigation.navigate("AddJobs");
    }
  };

  handleRefresh = () => {
    this._retrieveData();
  };
  SlideMenu = () => {
    {
      // this.props.navigation.navigate("People");

      //  this.props.navigation.navigate("People");
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };
  _retrieveData = async () => {
    console.log("retriev data called ....")
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const project_id = await AsyncStorage.getItem("prjid");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
        // this.setState({loader:false})
      }
      // : {{ base_url  }}/projects/{{ project_id }}/jobs.json (GET)

      // fetch(`${baseURL}/jobs/workers-added`, {

      console.log("Project id ..", project_id)
      if(project_id){

        AsyncStorage.getItem("prjName").then((projName)=>{
          if(projName){
            this.setState({projectName:projName})
          }
        })

        fetch(`${baseURL}/projects/${project_id}/jobs`, {
          method: "GET",
  
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.auth_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
  
            console.log("job applicant response ")
            console.log(responseJson)
            this.setState({loader:false})
            // responseJson.jobs.from_date.substring(0, 5);
            this.setState({
              projectList: responseJson.jobs
            });
  
            if (this.state.projectList.length == 0) {
              this.setState({status: "No jobs found.", display: "flex"});
            } else {
              this.setState({display: "none"});
            }
          })
          .catch(e => {
            this.setState({loader:false})
          });

      }else{
        this.setState({status: "No Project is selected. Please select project.",
         display: "flex", loader:false});
      }
        
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
   // Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View style={{flex: 1, alignItems: 'center'}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
            <View
            style={{
              height: 55,
              flexDirection: "row",
              marginTop: 20, width: '100%'
            }}
          >
            <TouchableOpacity style={{flex: 0.95, marginTop: 35, marginLeft: 10}}
                              onPress={this.SlideMenu.bind(this)}>
              <Image
                source={require("../../src/assets/h-menu.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 45}}
                              onPress={this.handleClick.bind(this)}>
              <Image
                style={{alignSelf: "flex-end"}}
                source={require("../../src/assets/add-job.png")}
              />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              marginTop: -5,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center', width: 200
            }}
          >
            Jobs
          </Text>
          <Text
            style={{
              color: 'gray',
              fontSize: 12,
              fontWeight: "200",
              textAlign: 'center',
              marginTop:5
            }}
          >
            Note :- Click on a job to make a selection.
          </Text>
          <View
            style={{
              flex: 1,
              display: this.state.display,
              justifyContent: "center"
            }}
          >
            <Text style={{alignSelf: "center", color: "black", fontSize: 18}}>
              {this.state.status}
            </Text>
          </View>

          <FlatList
            style={{marginTop: 10, width: '86%'}}
            data={this.state.projectList}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => {
                  this.getListLCs(item.uuid, item.job_application_status, item.status);
                }}
                style={{
                  borderRadius: 10,
                  marginBottom: "4%",
                  width: "100%",
                  borderColor: "red",
                  alignSelf: "center",
                  backgroundColor: colors.gray01
                }}
              >
                <Text style={styles.dateText}>
                  {Moment(item.start_date).format("D MMM YYYY") +
                  "  to  " +
                  Moment(item.end_date).format("D MMM YYYY")}
                </Text>
                <Text style={styles.detailText}>
                   Project -
                  <Text style={{fontWeight:'bold'}}>
                      {" "+this.state.projectName}
                    </Text>
                </Text>
                <Text style={styles.detailText}>
                   Job Title -
                  <Text style={{fontWeight:'100'}}>
                      {" "+item.title}
                    </Text>
                </Text>
                {/* <Text style={styles.titleText}>{item.title}</Text> */}
                <Text style={styles.detailText}>
                   Description -
                  <Text style={{fontWeight:'100'}}>
                      {" "+item.description}
                    </Text>
                </Text>
                <Text style={styles.detailText}>
                  Contact person -
                 <Text style={{fontWeight:'100'}}>
                      {" "+item.contact_name}
                    </Text>
                </Text>
                <Text style={styles.detailText}>
                  Phone number - 
                    <Text style={{fontWeight:'100'}}>
                      {" "+item.contact_no}
                    </Text>
                </Text>
                <Text style={styles.detailText}>
                  Worker required -
                    <Text style={{fontWeight:'100'}}>
                      {" "+item.no_of_worker}
                    </Text>
                </Text>
                <Text style={styles.detailText}>
                    Job Address - 
                    <Text style={{fontWeight:'100'}}>
                      {" "+item.street_address +","+ item.city + "," + item.zip_code}
                    </Text>
                </Text>
                <Text style={{fontSize: 14, paddingLeft: 10, paddingTop: 5, paddingBottom:10, color:'red', fontWeight:'bold'}}>
                 {item.job_application_status && item.status ==='Incomplete' ? "List of Applicants" :
                 (item.status ==='Hired' ? "Applicants Hired" : "No Applicants")}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  dateText: {
    color: "black",
    alignSelf: "flex-end",
    fontSize: 12,
    paddingTop: 9,
    fontWeight: "bold",
    paddingRight: 10
  },
  titleText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 15,
    paddingLeft: 10
  },
  detailText:{fontSize: 14, 
    paddingLeft: 10,
    paddingTop: 5,
    paddingRight: 10,
    fontWeight:'bold'},
  ButtonView: {
    backgroundColor: "red",
    height: 35,
    marginHorizontal: "20%",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  }
});
