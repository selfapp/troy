import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import {
  StyleSheet,
  View,
  Text,
  Modal,
  TextInput,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Alert,
  Dimensions,
} from "react-native";
import {DrawerActions} from "react-navigation";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import Button from "../Components/button";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class WorkerProfile extends Component {
  constructor() {
    super();
    this.state = {
      WorkerFirstName: "",
      WokerLastName: "",
      WorkerMobileNumber: "",
      enableScrollViewScroll: true,
      FirstName: "",
      LastName: "",
      WorkerList: [],
      modalVisible: false,
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "",
      zipcode: "",
      textvalue: "",
      uuid: "",
      auth_token: "",
      avatar: "",
      jobsCount: "",
      proposalCount: "",
      loader:false
    };
  }

  componentWillMount() {
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this._retrieveData();
    })
    this._retrieveWorkerList();
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "GET",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            console.log(responseJson);
            this.setState({
              FirstName:
                responseJson.user.first_name + " " + responseJson.user.last_name
            });
            this.setState({EmailId: responseJson.user.email});
            this.setState({StreetAddress: responseJson.user.street_address});
            this.setState({City: responseJson.user.city});
            this.setState({State: responseJson.user.state});
            this.setState({zipcode: responseJson.user.zip_code});
            this.setState({role: "Worker"});
            this.setState({jobsCount: responseJson.user.count_1});

            this.setState({proposalCount: responseJson.user.count_2});

            this.setState({
              avatar: responseJson.user.file
            });

            console.log("image is " + responseJson.user.file);
            console.log(this.state.avatar);
          })
          .catch(e => {
            this.setState({loader:false})
            console.log("called error " + e);
          });
      }
    } catch (error) {
      // Error retrieving data
      console.log("called error " + e);
    }

  };

  validateFields() {
    console.log("validate fields fun called");
    console.log(
      "first name value" +
      this.state.FirstName +
      this.state.EmailId +
      this.state.LastName +
      this.state.City +
      this.state.State +
      this.state.zipcode
    );
    if (
      this.state.WorkerFirstName == "" ||
      this.state.WokerLastName == "" ||
      this.state.WorkerMobileNumber == ""
    ) {
      Alert.alert("", "All fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  validate = text => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      // this.setState({ email: text });
      return false;
    } else {
      //this.setState({ email: text });
      console.log("Email is Correct");
      return true;
    }
  };

  apiCallUserProfileUpdate = () => {
    this.setState({loader:true})
    fetch(`${baseURL}/users/${this.state.uuid}`, {
      method: "GET",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.access_token
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        console.log(responseJson);
      })
      .catch(e => {
        this.setState({loader:false})
        console.log("called error " + e);
      });

    //
  };

  _retrieveWorkerList = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
      }

      fetch(`${baseURL}/users`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.access_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({
            WorkerList: responseJson.workers
          }),
            console.log(responseJson);
        })
        .catch(e => {
          this.setState({loader:false})
          console.log("called error " + e);
        });
    } catch (error) {
      // Error retrieving data
      console.log("called error " + error);
    }
  };

  apiCallAddWorker = () => {
    if (this.validateFields()) {

      this.setState({loader:true})
      console.log("called");
      var myObject = {
        user: {
          first_name: this.state.WorkerFirstName,
          last_name: this.state.WokerLastName,
          contact_number: this.state.WorkerMobileNumber,
          role: "WORKER"
        }
      };

      //api call for profile update

      fetch(`${baseURL}/users/create_worker`, {
        method: "POST",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.access_token
        },
        body: JSON.stringify(myObject)
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({ProjectName: ""});
          console.log(responseJson);

          try {
            // AsyncStorage.setItem("prjid", responseJson.project.uuid);
            this._retrieveWorkerList();
          } catch (error) {
            // Error saving data
            // We have data!!
            console.log("error while saving async");
          }

          Alert.alert(
            "",
            "Worker Added Successfully.",
            [
              {
                text: "OK",
                onPress: () => this.setState({modalVisible: false})
              }
            ],
            {cancelable: false}
          );
          this._retrieveWorkerList();
        })
        .catch(e => {
          this.setState({loader:false})
          console.log("called error " + e);
        });

      //
    } else {
    }
  };

  handleClick = () => {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  };

  handlePeople = () => {
  };

  toggleModal(visible) {
    this.setState({modalVisible: visible});
  }

  handleClickAddWorker = () => {
    {
      this.toggleModal(true);
    }
  };

  onStartShouldSetResponderCapture = () => {
    this.setState({enableScrollViewScroll: true});
  };
  onStartShouldSetResponderCapture = () => {
    this.setState({enableScrollViewScroll: false});
    if (
      this.refs.myList.scrollProperties.offset === 0 &&
      this.state.enableScrollViewScroll === false
    ) {
      this.setState({enableScrollViewScroll: true});
    }
  };

  editProfileAction() {
    console.log("fjhasjkd profile")
    this.props.navigation.navigate("Worker", {editProfile:true})
  }

  render() {
    return (
      <BackgroundNtScroll>
        <View
          onStartShouldSetResponderCapture={() => {
            this.setState({enableScrollViewScroll: true});
          }}
        >
          {this.state.loader ? <MyActivityIndicator /> : null}
          <ScrollView scrollEnabled={this.state.enableScrollViewScroll}>

            <View
              style={{
                position: "absolute",
                top: 50,
                width: "100%"
              }}
            >
              <Modal
                style={{
                  alignItems: "center",
                  justifyContent: "center"
                }}
                backdropOpacity={30}
                animationType={"slide"}
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                  console.log("Modal has been closed.");
                }}
              >

                <TouchableOpacity
                  onPress={() => {
                    this.toggleModal(!this.state.modalVisible);
                  }}
                  style={{
                    borderRadius: 10,
                    marginLeft: "5%",
                    marginRight: "5%",
                    display: "none",
                    marginTop: "15%",
                    backgroundColor: colors.gray03,
                    width: "90%"
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontSize: 20,
                      alignSelf: "center",
                      marginTop: "2%",
                      justifyContent: "center"
                    }}
                  >
                    Add Worker
                  </Text>
                  <TextInput
                    style={{
                      height: 40,
                      fontSize: 18,
                      marginVertical: "4%",
                      borderColor: colors.white,
                      borderWidth: 1,
                      marginLeft: 10,
                      alignSelf: "center",

                      width: "70%",
                      marginRight: 10,
                      borderRadius: 7
                    }}
                    underlineColorAndroid={colors.gray03}
                    placeholder="Enter first name"
                    maxLength={20}
                    placeholderTextColor={colors.white}
                    autoCapitalize="none"
                    value={this.state.WorkerFirstName}
                    onChangeText={value =>
                      this.setState({WorkerFirstName: value})
                    }
                  />
                  <TextInput
                    style={{
                      height: 40,
                      fontSize: 18,
                      marginVertical: "4%",
                      borderColor: colors.white,
                      borderWidth: 1,
                      marginLeft: 10,
                      alignSelf: "center",

                      width: "70%",
                      marginRight: 10,
                      borderRadius: 7
                    }}
                    underlineColorAndroid={colors.gray03}
                    placeholder="Enter last name"
                    maxLength={20}
                    placeholderTextColor={colors.white}
                    autoCapitalize="none"
                    value={this.state.WokerLastName}
                    onChangeText={value =>
                      this.setState({WokerLastName: value})
                    }
                  />
                  <TextInput
                    style={{
                      height: 40,
                      fontSize: 18,
                      marginVertical: "4%",
                      borderColor: colors.white,
                      borderWidth: 1,
                      marginLeft: 10,
                      alignSelf: "center",

                      width: "70%",
                      marginRight: 10,
                      borderRadius: 7
                    }}
                    underlineColorAndroid={colors.gray03}
                    placeholder="Enter mobile name"
                    maxLength={20}
                    placeholderTextColor={colors.white}
                    autoCapitalize="none"
                    value={this.state.WorkerMobileNumber}
                    onChangeText={value =>
                      this.setState({WorkerMobileNumber: value})
                    }
                  />

                  <View
                    style={{
                      alignSelf: "center",
                      width: "40%"
                    }}
                  >
                    <Button
                      marginTop={"1%"}
                      onPress={this.apiCallAddWorker.bind(this)}
                      buttonName={"Add"}
                      backgroundColor={colors.colorSignin}
                      textColor={"white"}
                    />

                    <Text
                      style={{
                        fontWeight: "bold",
                        fontSize: 20,
                        alignSelf: "center",
                        marginTop: "4%",
                        height: 20,
                        justifyContent: "center"
                      }}
                    />
                  </View>
                </TouchableOpacity>
              </Modal>
            </View>

            <View style={{flex: 1, alignItems: 'center'}}>
               <View
            style={{
              // height: 50,
              flexDirection: "row",
              marginTop: 55, width: '100%'
            }}
          >
            <TouchableOpacity style={{ marginLeft: 10, width: 45, height: 45}}
                              onPress={this.handleClick.bind(this)}>
              <Image
                source={require("../../src/assets/h-menu.png")}
              />
            </TouchableOpacity>
          
          <Text
            style={{
              // marginTop: 35,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center',
              width:width - 120
            }}
          >
            PROFILE
          </Text>

          <TouchableOpacity style={{width:60, 
            height:40, 
            alignItems:'center',
            justifyContent:'center'
            }}onPress={()=> this.editProfileAction()}>
              <Image
              source={require("../../src/assets/edit.png")}
              />
          </TouchableOpacity>
          </View>
              <View
                style={{
                  borderTopLeftRadius: 60,
                  borderTopRightRadius: 60,
                  marginTop: "5%",

                  width: "100%",
                  height: height + 270,
                  backgroundColor: colors.gray01
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column"
                  }}
                >
                  <View style={{flex: 1, alignItems: "center"}}>
                    <Text
                      style={{
                        alignSelf: "center",
                        fontSize: 18,
                        marginTop: "5%",
                        paddingLeft: 20,
                        // maxLines: 1,
                        paddingRight: 20,
                        alignItems: "center",
                        justifyContent: "center",
                        fontWeight: "bold"
                      }}
                    >
                      {this.state.FirstName}
                    </Text>

                    <Text
                      style={{
                        alignSelf: "center",
                        fontSize: 18,

                        fontWeight: "bold"
                      }}
                    >
                      {this.state.EmailId}
                    </Text>

                    <Text
                      style={{
                        alignSelf: "center",
                        fontSize: 16,
                        marginTop: "4%",
                        fontWeight: "bold"
                      }}
                    >
                      {this.state.role}
                    </Text>

                    <View
                      style={{
                        height: "24%",
                        flexDirection: "row",

                        margin: "6%",
                        backgroundColor: colors.gray01
                      }}
                    >
                      <View
                        style={{
                          flex: 1,

                          flexDirection: "column"
                        }}
                      >
                        <Text
                          style={{
                            flex: 1.2,
                            fontSize: 18,
                            color: colors.black,
                            alignSelf: "center"
                          }}
                        >
                          {this.state.jobsCount}
                        </Text>
                        <Text
                          style={{
                            flex: 2,
                            color: colors.black,
                            fontSize: 14,
                            alignSelf: "center"
                          }}
                        >
                          JOBS
                        </Text>
                      </View>

                      <View
                        style={{
                          flex: 0.01,

                          flexDirection: "column",
                          backgroundColor: colors.gray05
                        }}
                      />

                      <View
                        style={{
                          flex: 1,

                          flexDirection: "column"
                        }}
                      >
                        <Text
                          style={{
                            flex: 1.2,
                            color: colors.black,
                            fontSize: 20,
                            alignSelf: "center"
                          }}
                        >
                          {this.state.proposalCount}
                        </Text>
                        <Text
                          style={{
                            flex: 2,
                            color: colors.black,
                            fontSize: 14,
                            alignSelf: "center"
                          }}
                        >
                          PROPOSAL
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={{flex: 4}}>
                    <View
                      style={{
                        borderTopLeftRadius: 30,
                        borderTopRightRadius: 30,
                        borderBottomLeftRadius: 30,
                        borderBottomRightRadius: 30,
                        marginTop: "3%",
                        margin: "5%",
                        height: "30%",
                        elevation: 1,
                        backgroundColor: colors.white
                      }}
                    >
                      <Text
                        style={{
                          alignSelf: "center",
                          fontSize: 18,
                          marginTop: "4%",
                          fontWeight: "bold",
                          color: colors.black
                        }}
                      >
                        Address
                      </Text>

                      <View
                        style={{
                          margin: "2%",

                          flexDirection: "column",

                          height: "30%"
                        }}
                      >
                        <View
                          style={{
                            flex: 0.4,
                            flexDirection: "row",

                            margin: "1%",
                            marginLeft: "3%"
                          }}
                        >
                          <Text
                            style={{
                              flex: 3,

                              alignSelf: "center",
                              alignContent: "center",
                              fontSize: 16,

                              color: colors.black,
                              fontWeight: "bold"
                            }}
                          >
                            Street Address
                          </Text>
                          <Text
                            style={{
                              flex: 1,

                              alignContent: "center",
                              fontSize: 16,
                              alignSelf: "flex-start",
                              fontWeight: "bold",
                              color: colors.black
                            }}
                          >
                            City
                          </Text>
                        </View>

                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",

                            marginRight: "1%",
                            marginLeft: "3%"
                          }}
                        >
                          <Text
                            style={{
                              flex: 3,

                              alignSelf: "baseline",
                              fontSize: 14
                            }}
                          >
                            {this.state.StreetAddress}
                          </Text>
                          <Text
                            style={{
                              flex: 1,

                              alignContent: "center",
                              fontSize: 14,
                              paddingBottom: 4
                            }}
                          >
                            {this.state.City}
                          </Text>
                        </View>
                      </View>

                      <View
                        style={{
                          marginLeft: "2%",
                          marginRight: "4%",

                          flexDirection: "column",

                          height: "24%"
                        }}
                      >
                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",

                            marginLeft: "4%",
                            marginRight: "2%"
                          }}
                        >
                          <Text
                            style={{
                              flex: 3,

                              alignSelf: "baseline",
                              fontSize: 16,
                              color: colors.black,

                              fontWeight: "bold"
                            }}
                          >
                            State
                          </Text>
                          <Text
                            style={{
                              flex: 1,

                              alignContent: "center",
                              fontSize: 16,
                              marginLeft: "6%",
                              alignSelf: "flex-start",
                              fontWeight: "bold",
                              color: colors.black
                            }}
                          >
                            Zip code
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            flexDirection: "row",

                            marginLeft: "2%",
                            marginRight: "2%"
                          }}
                        >
                          <Text
                            style={{
                              flex: 3.2,

                              alignSelf: "baseline",
                              fontSize: 14,
                              marginLeft: "2%"
                            }}
                          >
                            {this.state.State}
                          </Text>
                          <Text
                            style={{
                              flex: 1,

                              alignContent: "center",
                              fontSize: 14,

                              alignSelf: "flex-start"
                            }}
                          >
                            {this.state.zipcode}
                          </Text>
                        </View>
                      </View>
                    </View>

                    <View style={{}}>
                      <View
                        style={{
                          borderTopLeftRadius: 30,
                          borderTopRightRadius: 30,
                          borderBottomLeftRadius: 30,
                          borderBottomRightRadius: 30,
                          marginTop: "1%",
                          margin: "5%",
                          height: 150,
                          elevation: 1,
                          backgroundColor: colors.white
                        }}
                      >
                        <Text
                          style={{
                            alignSelf: "center",
                            fontSize: 18,
                            marginTop: "4%",
                            fontWeight: "bold",
                            color: colors.black
                          }}
                        >
                          License
                        </Text>

                        <Image
                          style={{marginLeft: 20, height: 80, width: 70}}
                          source={{
                            // uri: `${baseURL}/${this.state.avatar}`
                            uri: this.state.avatar
                          }}
                        />
                      </View>
                    </View>

                    <View style={{}}>
                      <View
                        style={{
                          borderTopLeftRadius: 30,
                          borderTopRightRadius: 30,
                          borderBottomLeftRadius: 30,
                          borderBottomRightRadius: 30,
                          marginTop: "1%",
                          margin: "5%",
                          display: "none",

                          elevation: 1,
                          backgroundColor: colors.white
                        }}
                      >
                        <View
                          style={{
                            height: "24%",
                            flexDirection: "row",
                            marginLeft: 30,
                            marginTop: 10
                          }}
                        >
                          <Text
                            style={{
                              alignSelf: "center",
                              fontSize: 18,

                              flex: 1,
                              marginTop: "7%",
                              fontWeight: "bold",
                              textAlign: "center",
                              color: colors.black
                            }}
                          >
                            Worker
                          </Text>
                          <TouchableOpacity
                            onPress={this.handleClickAddWorker.bind(this)}
                            style={{
                              flex: 0.2,
                              height: 40,
                              marginRight: 10,
                              width: 40
                            }}
                          >
                            <Image
                              source={require("../../src/assets/add.png")}
                            />
                          </TouchableOpacity>
                        </View>

                        <View
                          onStartShouldSetResponderCapture={() => {
                            this.setState({enableScrollViewScroll: false});
                            if (this.state.enableScrollViewScroll === false) {
                              this.setState({enableScrollViewScroll: true});
                            }
                          }}
                          style={{
                            marginLeft: "1%",

                            marginBottom: "4%",
                            height: 100,
                            marginRight: "1%"
                          }}
                        >
                          <FlatList
                            style={{marginTop: 1}}
                            data={this.state.WorkerList}
                            renderItem={({item, index}) => (
                              <View style={{}}>
                                <View
                                  style={{
                                    borderRadius: 10,

                                    width: "90%",
                                    borderColor: "red",

                                    justifyContent: "center",
                                    backgroundColor: colors.white,
                                    alignSelf: "center"
                                  }}
                                >
                                  <Text
                                    style={{
                                      color: "black",
                                      justifyContent: "center",
                                      alignItems: "center",
                                      fontWeight: "bold",
                                      paddingLeft: 10,
                                      height: 17,
                                      marginTop: 3
                                    }}
                                  >
                                    {item.first_name}
                                  </Text>

                                  <View
                                    style={{
                                      flexDirection: "row"
                                    }}
                                  >
                                    <Image
                                      style={{
                                        marginTop: "1%"
                                      }}
                                      source={require("../../src/assets/mobile-work.png")}
                                    />
                                    <Text
                                      style={{
                                        marginTop: "5%",
                                        marginLeft: "4%"
                                      }}
                                    >
                                      {item.contact_number}
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: "row",
                                      height: 1,
                                      backgroundColor: colors.gray01
                                    }}
                                  />
                                </View>
                              </View>
                            )}
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>

          </ScrollView>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  }
});
