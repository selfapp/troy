import React, { Component } from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  FlatList,
  Alert,
  TouchableOpacity,
  AsyncStorage,
  Text,
  ScrollView,
  View
} from "react-native";
import Moment from "moment";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import MyActivityIndicator from '../Components/activity_indicator';

import colors from "../styles/colors";
import { baseURL } from '../../api';
// import console = require("console");

const height = Dimensions.get("window").height;

export default class ApproveTimeSheetJobs extends Component {
  constructor() {
    super();
    this.state = {
      calenderSelectedValue: [],
      currentDateForCalender: "",
      currentDate: "",
      dateInitailNumber: "",
      jobTitle: "",
      workerName: "",
      workingHours: "",
      approvedStatus: "",
      dateMiddleText: "",
      dateLastText: "",
      scheduleData: [],
      DataJsonColored: "",
      selectedGcName: "",
      selectedEmailAddress: "",
      timecardsArray: [],

      selectedAddress: "",
      selectedWorkingHours: "",
      firstSelectedDate: "",
      modalVisible: false,
      approveDisplay: "none",
      approveButtonDisplay: "none",
      uuid: "",
      auth_token: "",
      SelectProjectData: [],
      projectListRaw: [],
      projectList: [],
      status: "",
      ProjectNameAsUUID: "",
      ProjectName: "Select Project",
      project_uuid: "",
      loader:false
      // projectList = [
      //   { name: "Project 1" },
      //   { name: "Project 2" },
      //   { name: "Project 3" },
      //   { name: "Project 4" },
      //   { name: "Project 5" },
      //   { name: "Project 6" }
      // ]
    };
  }

  componentDidMount() {
    this._retrieveData();
  }

  _retrieveData = async index => {
    try {
      this.setState({loader:true})
      // console.log("project uuid index is " + index);
      // console.log("project uuid is " + this.state.projectListRaw[index].uuid);

      // this.setState({
      //   ProjectNameAsUUID: this.state.projectListRaw[index].uuid
      //   // ProjectName: this.state.projectListRaw[index].name
      // });

      var data = {
        title: "Job",
        description: "Job description",
        date: "2018-11-14",
        timecards: [
          {
            in: "2018-11-14T09:03:11.000Z",
            out: "2018-11-14T09:32:34.000Z",
            total: "29 m",
            worker: "Worker 1",
            status: "Approved"
          },
          {
            in: "2018-11-14T09:04:43.000Z",
            out: "2018-11-14T09:32:28.000Z",
            total: "30 m",
            worker: "Worker 2",
            status: "Approved"
          }
        ],
        total: "59 m"
      };
      this.setState({ projectList: [data] });

      const uuid = await AsyncStorage.getItem("uuid");
      const uuid_jobs = await AsyncStorage.getItem("job_uuid");

      const access_token = await AsyncStorage.getItem("access_token");
      const project_id = await AsyncStorage.getItem("prjid");
      if (uuid !== null) {
        // We have data!!

        this.setState({ uuid: uuid });
        this.setState({ auth_token: access_token });

        //  console.log("project uuid is " + this.state.ProjectNameAsUUID);
      }
      // : {{ base_url  }}/projects/{{ project_id }}/jobs.json (GET)

      fetch(`${baseURL}/gc/timecards/${uuid_jobs}`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          // responseJson.jobs.from_date.substring(0, 5);
          console.log('Retrive ', JSON.stringify(responseJson));
          this.setState({loader:false})
          this.setState({
            projectList: [responseJson]
          });
          if (this.state.projectList[0].timecards.length == 0) {
            this.setState({
              status: "No time sheet for today",
              approveDisplay: "none"
            });
          } else {
            this.setState({
              status: "",
              approveDisplay: "flex",
              approveButtonDisplay: "flex"
            });
            this.setState({
              timecardsArray: this.state.projectList[0].timecards
            });
          }
          if (this.state.projectList[0].timecards[0].status == "Approved") {
            this.setState({
              approveButtonDisplay: "none"
            });
          }
          this.getCurrentDateParams(this.state.projectList[0].date);
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  _ApproveTimeSheet = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const uuid_jobs = await AsyncStorage.getItem("job_uuid");

      if (uuid !== null) {
        // We have data!!
        this.setState({ uuid: uuid });
        this.setState({ access_token: access_token });

        fetch(`${baseURL}/gc/timecards/${uuid_jobs}/approve`, {
          method: "POST",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            console.log('approve response ', JSON.stringify(responseJson));
            Alert.alert(
              "",
              "Approved TimeSheet Successfully.",
              [
                {
                  text: "OK"
                }
              ],
              { cancelable: false }
            );
            this._retrieveData();
          })
          .catch(e => {
            this.setState({loader:false})
          });
      }
    } catch (error) {
      // Error retrieving data
    }
  };
  handleClick = () => {
    this.props.navigation.navigate("ChooseTimeSheetJobs");
  };

  getCurrentDateParams(date2) {
    var dateString = date2;

    const date = Moment(dateString).format("D MMM YYYY");
    const dateInitail = Moment(dateString).format("DD");

    const dateMiddle = Moment(dateString).format("MMMM YYYY");

    const dateLast = Moment(dateString).format("dddd");

    this.setState({ currentDate: date });
    this.setState({ dateInitailNumber: dateInitail });
    this.setState({ dateMiddleText: dateMiddle });

    this.setState({ dateLastText: dateLast });
    this.setState({
      jobTitle: this.state.projectList[0].title,
      workerName: this.state.projectList[0].timecards[0].worker,
      workingHours: this.state.projectList[0].total,
      approvedStatus: this.state.projectList[0].timecards[0].status
    });

    //("YYYY-MM-DD hh:mm:ss"))
  }

  render() {
    return (
      <BackgroundNtScroll>
       
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 40, alignItems: 'center'
          }}
        >
          {
            <TouchableOpacity
              style={{ width: 30 }}
              onPress={this.handleClick.bind(this)}
            >
              <Image
                style={{ alignSelf: "flex-start" }}
                source={require("../../src/assets/backnew.png")}
              />
            </TouchableOpacity>
          }

          <View style={{ flex: 1, marginTop: 17 }}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Approve Timesheet
            </Text>
          </View>
        </View>

        <ScrollView
          style={{
            // height: height / 2 + 50,
            display: this.state.approveDisplay
          }}
        >
           {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomLeftRadius: 15,
              borderBottomRightRadius: 15,
              marginTop: "3%",
              margin: "5%",
              height: 600,
              display: this.state.approveDisplay,
              elevation: 1,
              backgroundColor: colors.gray01
            }}
          >
            <View
              style={{
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  marginTop: 10,
                  marginLeft: 20,
                  width: 60,
                  backgroundColor: colors.colorClipTop,
                  height: 60,
                  borderRadius: 35,
                  alignContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    alignContent: "center",
                    alignItems: "center",
                    fontSize: 25,

                    marginTop: "25%",
                    fontWeight: "bold",
                    color: colors.white
                  }}
                >
                  {this.state.dateInitailNumber}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 0.9
                }}
              >
                <View
                  style={{
                    flex: 1.8,
                    marginLeft: 10
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      alignSelf: "center",
                      textAlignVertical: "bottom",
                      marginTop: 18,
                      fontWeight: "bold",
                      color: colors.black
                    }}
                  >
                    {this.state.dateMiddleText}
                  </Text>
                </View>

                <View style={{ flex: 1 }}>
                  <Text
                    style={{
                      alignSelf: "center",
                      fontSize: 18,
                      marginTop: "2%",
                      fontWeight: "bold",
                      color: colors.black
                    }}
                  >
                    {this.state.dateLastText}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                marginTop: 10,
                backgroundColor: colors.white,
                height: 1
              }}
            />
            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                height: "13%"
              }}
            >
              <View
                style={{
                  flex: 0.6,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{ flex: 0.1 }}>
                  <Image
                    style={{ height: 25, width: 25, marginTop: 5 }}
                    source={require("../../src/assets/worker-icon.png")}
                  />
                </View>
                <View style={{ flex: 0.9 }}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      fontSize: 16,
                      color: colors.colorClipTop,
                      fontWeight: "bold",
                      textAlignVertical: "center"
                    }}
                  >
                    Job Title
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.7,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{ flex: 0.1 }} />
                <View style={{ flex: 0.9 }}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.jobTitle}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 15,
                height: "13%"
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{ flex: 0.1 }}>
                  <Image
                    style={{ height: 25, width: 25, marginTop: 5 }}
                    source={require("../../src/assets/worker-icon.png")}
                  />
                </View>
                <View style={{ flex: 0.9 }}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Worker List
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",
                  marginLeft: "3%"
                }}
              >
                <FlatList
                  style={{
                    marginTop: 1,
                    marginLeft: "12%",
                    borderTopLeftRadius: 15,
                    borderTopRightRadius: 15,
                    borderBottomLeftRadius: 15,
                    borderBottomRightRadius: 15,
                    height: height / 6,
                    width: 120,
                    elevation: 30,
                    backgroundColor: colors.white
                  }}
                  data={this.state.timecardsArray}
                  renderItem={({ item, index }) => (
                    <View style={{ margin: 6 }}>
                      <Text
                        style={{
                          fontSize: 10,

                          color: colors.colorClipTop,
                          fontWeight: "bold",

                          textAlignVertical: "center"
                        }}
                      >
                        {index +
                          1 +
                          ") Worker " +
                          item.worker +
                          " worked on " +
                          this.state.jobTitle +
                          " for " +
                          item.total +
                          "."}
                      </Text>
                    </View>
                  )}
                />
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 110,
                height: "13%"
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{ flex: 0.1 }}>
                  <Image
                    style={{ height: 25, width: 25, marginTop: 5 }}
                    source={require("../../src/assets/clock.png")}
                  />
                </View>
                <View style={{ flex: 0.5 }}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Working hours
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{ flex: 0.1 }} />
                <View style={{ flex: 0.9 }}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.workingHours}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 15,
                height: "25%"
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{ flex: 0.1 }}>
                  <Image
                    style={{ height: 25, width: 25, marginTop: 5 }}
                    source={require("../../src/assets/time-clock.png")}
                  />
                </View>
                <View style={{ flex: 0.9 }}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Status
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{ flex: 0.1 }} />
                <View style={{ flex: 0.9 }}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      height: 20,
                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.approvedStatus}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <View style={{ flex: 1, justifyContent: "center" }}>
          <Text style={{ alignSelf: "center", color: "black", fontSize: 18 }}>
            {this.state.status}
          </Text>
        </View>
        <View style={{ height: 50 }}>
          <TouchableOpacity
            onPress={this._ApproveTimeSheet.bind(this)}
            style={{
              flex: 1,
              justifyContent: "center",
              marginLeft: "30%",
              marginRight: "30%",
              marginBottom: "1%",
              borderRadius: 5,

              backgroundColor: colors.colorSignin,
              display: this.state.approveButtonDisplay
            }}
          >
            <Text
              style={{
                alignSelf: "center",
                color: "white",
                fontWeight: "bold"
              }}
            >
              Approve
            </Text>
          </TouchableOpacity>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
