import React, {Component} from "react";
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  Text,
  View
} from "react-native";
import {DrawerActions} from "react-navigation";

import colors from "../styles/colors";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";

export default class InvoicesAndTimesheet extends Component {
  handleClick = () => {
    {
      // this.props.navigation.navigate("People");

      //  this.props.navigation.navigate("People");
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };

  render() {
    return (
      <BackgroundNtScroll>
        <View
          style={{
            height: "13%",
            flexDirection: "row",
            marginLeft: "0%",
            marginRight: "5%",
            marginTop: "10%"
          }}
        >
          <TouchableOpacity onPress={this.handleClick.bind(this)}>
            <Image
              style={{alignSelf: "flex-start"}}
              source={require("../../src/assets/burger.png")}
            />
          </TouchableOpacity>

          <View style={{flex: 1, marginTop: 15}}>
            <Text
              style={{
                flex: 1,
                color: colors.white,
                fontSize: 22,
                alignSelf: "center",
                fontWeight: "bold"
              }}
            >
              Project dashboard
            </Text>
          </View>

          <TouchableOpacity onPress={this.handleClick.bind(this)}/>
        </View>
        <View style={{flex: 1, justifyContent: "center"}}>
          <Text style={{alignSelf: "center", color: "white"}}>
            COMING SOON
          </Text>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
