import React, { Component } from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  AsyncStorage,
  TouchableOpacity,
  Text,
  View,
  Platform,
  Alert
} from "react-native";
import { DrawerActions } from "react-navigation";

import colors from "../styles/colors";
import BackgroundApp from "../Components/BackgroundApp";
import RoundedButton from "../Components/RoundedButton";
import { API } from '../../api';

const width = Dimensions.get("window").width;

export default class TimeSheetOrInvoice extends Component {
  constructor() {
    super();
    this.state = {
      ProjectName: ""
    };
  }

  componentDidMount() {
    this.setPushConfig();
    this.retrieveData();
  }
  
  handleClick = () => {
    {
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
  };

  saveUserType = propsType => {
    try {
      if (propsType === "INVOICE") {
        this.props.navigation.navigate("SelectedInvoice");
      } else if (propsType === "TIMESHEET") {
        this.props.navigation.navigate("ChooseTimeSheetJobs");
      }
    } catch (error) {
      // Error saving data
      // We have data!!
      console.log("error while saving async");
    }
  };

  retrieveData = async () => {
    try {
      const PrjName = await AsyncStorage.getItem("prjName");
      if (PrjName != null) {
        this.setState({ ProjectName: "Project - " + PrjName });
      } else {
        this.setState({ ProjectName: "Choose Project" });
      }

    } catch (error) {
      console.log("e");
    }
  };

  setPushConfig() {
    let _this = this;
    var PushNotification = require("react-native-push-notification");

    PushNotification.configure({
      onRegister: function (token) {
        try {
          AsyncStorage.setItem(
            "gcm_token",
            JSON.parse(JSON.stringify(token.token))
          );
          API.savePushToken(token.token);
        } catch (error) {
          console.log("error while saving async");
        }
      },

      onNotification: function (notification) {
        console.log("notification received time sheet or invoice....")
        console.log(notification)
        
        if (
          notification.foreground === true &&
          notification.userInteraction === false
        ){
            if(Platform.OS === 'ios'){
                Alert.alert("STAFF UP", notification.data.message);
            }
          }
        if (
          notification.foreground === true &&
          notification.userInteraction === true
        ) {
          if (notification.data.code === "001") {
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        } else if (
          notification.foreground === false ||
          notification.foreground === null
        ) {
          if (notification.code === "001") {
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        }
      },
      senderID: "900411929042",
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
      popInitialNotification: true,
      requestPermissions: true
    });
  }

  render() {
    return (
      <BackgroundApp>
        <View style={{ flex: 1, alignItems: 'center' }}>

          <View
            style={{
              height: 50,
              flexDirection: "row",
              marginTop: 20, width: '100%'
            }}
          >
            <TouchableOpacity style={{ marginTop: 35, marginLeft: 10 }}
              onPress={this.handleClick.bind(this)}>
              <Image

                source={require("../../src/assets/h-menu.png")}
              />
            </TouchableOpacity>
          </View>
          {/* <Text
            style={{
              marginTop: -5,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center', marginBottom: 10, width: 150
            }}
          >
            Dashboard
          </Text> */}
          <TouchableOpacity style={{ marginTop: 30 }} onPress={this.handleClick.bind(this)}>
            <View
              style={{
                height: 50,
                alignContent: "center",
                alignItems: "center",
                justifyContent: 'center',
                elevation: 20,
                borderRadius: 15,
                width: width - 50,
                backgroundColor: colors.colorSignin
              }}
            >
              <Text
                style={{
                  // flex: 1,
                  color: colors.white,
                  fontSize: 15,
                  // alignSelf: "center",
                  fontWeight: "300",
                  // marginTop: "5%",
                  // alignItems: "center"

                }}
              >
                {this.state.ProjectName}
              </Text>
            </View>
          </TouchableOpacity>

          <View
            style={{
              margin: "5%",
              marginTop: "10%",
              flexDirection: "row",
              elevation: 10,
              height: 170,
              backgroundColor: 'white'
            }}
          >
            <View
              style={{
                flex: 1,
                margin: 10, backgroundColor: colors.gray01,
                alignContent: "center", height: '100%'
              }}
            >
              <RoundedButton
                onPress={() => {
                  // this.props.navigation.navigate("LaborContractor");
                  this.saveUserType("INVOICE");
                }}
                buttonName={"INVOICE"}
                showImage
                width={"100%"}

                // backgroundColor={colors.red}
                textColor={"white"}
                imageSource={require("../../src/assets/gc-icon.png")}
              />
            </View>

            <View
              style={{
                flex: 1,
                margin: 10,
                elevation: 30, backgroundColor: colors.gray01,
                marginLeft: "10%", height: '100%'
              }}
            >
              <RoundedButton
                onPress={() => {
                  this.saveUserType("TIMESHEET");
                }}
                buttonName={"TIMESHEET"}
                showImage
                width={"100%"}
                textColor={"white"}
                imageSource={require("../../src/assets/worker-icon.png")}
              />
            </View>
          </View>

          <View style={{ flex: 1, justifyContent: "center", backgroundColor: 'white' }}>
            <Text style={{ alignSelf: "center", color: "white" }} />
          </View>
        </View>
      </BackgroundApp>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
