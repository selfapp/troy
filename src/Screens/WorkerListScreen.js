import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import {
  StyleSheet,
  View,
  Text,
  Modal,
  TextInput,
  Image,
  TouchableOpacity,
  FlatList,
  Alert,
  Dimensions,
} from "react-native";
import {DrawerActions} from "react-navigation";
import MyActivityIndicator from '../Components/activity_indicator';
import colors from "../styles/colors";
import Button from "../Components/button";
import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import {baseURL} from '../../api';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class WorkerListScreen extends Component {
  constructor() {
    super();
    this.state = {
      WorkerFirstName: "",
      WokerLastName: "",
      WorkerMobileNumber: "",
      enableScrollViewScroll: true,
      FirstName: "",
      LastName: "",
      WorkerList: [],
      modalVisible: false,
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "",
      zipcode: "",
      textvalue: "",
      uuid: "",
      auth_token: "",
      avatar: "",
      display: "none",
      status: "",
      loader:false
    };
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "GET",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          }
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            console.log(responseJson);
            this.setState({
              FirstName:
                responseJson.user.first_name + " " + responseJson.user.last_name
            });
            this.setState({EmailId: responseJson.user.email});
            this.setState({StreetAddress: responseJson.user.street_address});
            this.setState({City: responseJson.user.city});
            this.setState({State: responseJson.user.state});
            this.setState({zipcode: responseJson.user.zip_code});
            // this.setState({role: "Labor contractor"});
            this.setState({role: "Staffing Contractor"});
            this.setState({
              avatar: responseJson.user.file
            });

            console.log("image is " + responseJson.user.file);
            console.log(this.state.avatar);
          })
          .catch(e => {
            this.setState({loader:false})
            console.log("called error " + e);
          });
      }
    } catch (error) {
      // Error retrieving data
      console.log("called error " + e);
    }
  };

  validateFields() {
    console.log("validate fields fun called");
    console.log(
      "first name value" +
      this.state.FirstName +
      this.state.EmailId +
      this.state.LastName +
      this.state.City +
      this.state.State +
      this.state.zipcode
    );
    if (
      this.state.WorkerFirstName == "" ||
      this.state.WokerLastName == "" ||
      this.state.WorkerMobileNumber == ""
    ) {
      Alert.alert("", "All fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  kill = () => {
    this.props.navigation.navigate("LCProfile");
  };

  componentWillMount() {
    this._retrieveData();
    this._retrieveWorkerList();
  }

  validate = text => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      // this.setState({ email: text });
      return false;
    } else {
      //this.setState({ email: text });
      console.log("Email is Correct");
      return true;
    }
  };

  apiCallUserProfileUpdate = () => {
    this.setState({loader:true})
    console.log("called get user details api ");
    var myObject = {
      user: {
        first_name: this.state.FirstName,
        last_name: this.state.LastName,
        email: this.state.EmailId,
        street_address: this.state.StreetAddress,
        city: this.state.City,
        state: this.state.State,
        zip_code: this.state.zipcode,
        role: "GC"
      }
    };

    //api call for profile update
    fetch(`${baseURL}/users/${this.state.uuid}`, {
      method: "GET",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.access_token
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        console.log(responseJson);
      })
      .catch(e => {
        this.setState({loader:false})
        console.log("called error " + e);
      });

    //
  };

  _retrieveWorkerList = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");

      if (uuid !== null) {
        // We have data!!

        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});

        console.log("uuid" + this.state.uuid);
        console.log("access_token" + this.state.access_token);
      }

      fetch(`${baseURL}/users`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.access_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          this.setState({
            WorkerList: responseJson.workers
          }),
            console.log(responseJson);
          if (this.state.WorkerList.length == 0) {
            console.log("length is xxx", this.state.WorkerList.length);

            this.setState({status: "No Workers found.", display: "flex"});
          } else {
            this.setState({display: "none"});
          }
        })
        .catch(e => {
          this.setState({loader:false})
          console.log("called error " + e);
        });
    } catch (error) {
      // Error retrieving data
      console.log("called error " + error);
    }
  };

  apiCallAddWorker = () => {
    if (this.validateFields()) {
      this.setState({spinner: true});
      console.log("called");
      var myObject = {
        user: {
          first_name: this.state.WorkerFirstName,
          last_name: this.state.WokerLastName,
          contact_number: this.state.WorkerMobileNumber,
          role: "WORKER"
        }
      };

      //api call for profile update
      fetch(`${baseURL}/users/create_worker`, {
        method: "POST",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.access_token
        },
        body: JSON.stringify(myObject)
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({ProjectName: ""});
          console.log(responseJson);

          try {
            // AsyncStorage.setItem("prjid", responseJson.project.uuid);
            this.setState({spinner: false});

            this._retrieveWorkerList();
          } catch (error) {
            // Error saving data
            // We have data!!
            console.log("error while saving async");
            this.setState({spinner: false});
          }

          Alert.alert(
            "",
            responseJson.message,
            [
              {
                text: "OK",
                onPress: () => this.setState({modalVisible: false})
              }
            ],
            {cancelable: false}
          );
          this._retrieveWorkerList();
        })
        .catch(e => {
          console.log("called error " + e);
          this.setState({spinner: false});
        });

      //
    } else {
    }
  };

  handleClick = () => {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  };

  handlePeople = () => {
  };

  toggleModal(visible) {
    this.setState({modalVisible: visible});
  }

  handleClickAddWorker = () => {
    {
      this.toggleModal(true);
    }
  };

  onStartShouldSetResponderCapture = () => {
    this.setState({enableScrollViewScroll: true});
  };
  onStartShouldSetResponderCapture = () => {
    this.setState({enableScrollViewScroll: false});
    if (
      this.refs.myList.scrollProperties.offset === 0 &&
      this.state.enableScrollViewScroll === false
    ) {
      this.setState({enableScrollViewScroll: true});
    }
  };

  render() {
    return (
      <BackgroundNtScroll>
        <View
          onStartShouldSetResponderCapture={() => {
            this.setState({enableScrollViewScroll: true});
          }}
        >
          <View
            style={{
              position: "absolute",
              top: 50,
              width: "100%"
            }}
          >
            <Modal
              style={{
                alignItems: "center",
                justifyContent: "center"
              }}
              backdropOpacity={30}
              animationType={"slide"}
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                console.log("Modal has been closed.");
              }}
            >
              {}

              <TouchableOpacity
                onPress={() => {
                  this.toggleModal(!this.state.modalVisible);
                }}
                style={{
                  borderRadius: 10,
                  marginLeft: "5%",
                  marginRight: "5%",
                  marginTop: "25%",
                  backgroundColor: colors.gray03,
                  width: "90%"
                }}
              >
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    alignSelf: "center",
                    marginTop: "2%",
                    justifyContent: "center"
                  }}
                >
                  Add Worker
                </Text>
                <TextInput
                  style={{
                    height: 40,
                    fontSize: 18,
                    marginVertical: "4%",
                    borderColor: colors.white,
                    borderWidth: 1,
                    marginLeft: 10,
                    alignSelf: "center",
                    paddingLeft: 8,
                    width: "70%",
                    marginRight: 10,
                    borderRadius: 7
                  }}
                  underlineColorAndroid={colors.gray03}
                  placeholder="Enter first name"
                  maxLength={20}
                  placeholderTextColor={colors.white}
                  autoCapitalize="none"
                  value={this.state.WorkerFirstName}
                  onChangeText={value =>
                    this.setState({WorkerFirstName: value})
                  }
                />
                <TextInput
                  style={{
                    height: 40,
                    fontSize: 18,
                    marginVertical: "4%",
                    borderColor: colors.white,
                    borderWidth: 1,
                    marginLeft: 10,
                    paddingLeft: 8,
                    alignSelf: "center",

                    width: "70%",
                    marginRight: 10,
                    borderRadius: 7
                  }}
                  underlineColorAndroid={colors.gray03}
                  placeholder="Enter last name"
                  maxLength={20}
                  placeholderTextColor={colors.white}
                  autoCapitalize="none"
                  value={this.state.WokerLastName}
                  onChangeText={value =>
                    this.setState({WokerLastName: value})
                  }
                />
                <TextInput
                  style={{
                    height: 40,
                    fontSize: 18,
                    paddingLeft: 8,
                    marginVertical: "4%",
                    borderColor: colors.white,
                    borderWidth: 1,
                    marginLeft: 10,
                    alignSelf: "center",

                    width: "70%",
                    marginRight: 10,
                    borderRadius: 7
                  }}
                  underlineColorAndroid={colors.gray03}
                  placeholder="Enter mobile name"
                  maxLength={20}
                  placeholderTextColor={colors.white}
                  autoCapitalize="none"
                  value={this.state.WorkerMobileNumber}
                  onChangeText={value =>
                    this.setState({WorkerMobileNumber: value})
                  }
                />

                <View
                  style={{
                    alignSelf: "center",
                    width: "40%"
                  }}
                >
                  <Button
                    marginTop={"1%"}
                    onPress={this.apiCallAddWorker.bind(this)}
                    buttonName={"Add"}
                    backgroundColor={colors.colorSignin}
                    textColor={"white"}
                  />

                  <Text
                    style={{
                      fontWeight: "bold",
                      fontSize: 20,
                      alignSelf: "center",
                      marginTop: "4%",
                      height: 20,
                      justifyContent: "center"
                    }}
                  />
                </View>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={{flex: 1}}>
          {this.state.loader ? <MyActivityIndicator /> : null}
            <View
              style={{
                height: "15%",

                flexDirection: "row",
                marginRight: "5%",
                marginTop: "9%"
              }}
            >
              {}

              <View style={{flex: 1}}>
                <Text
                  style={{
                    flex: 1,
                    color: colors.white,
                    fontSize: 22,

                    alignSelf: "center",
                    fontWeight: "bold"
                  }}
                >
                  PROFILE
                </Text>
              </View>
            </View>

            <View style={{flex: 4}}>
              <View style={{}}>
                <View
                  style={{
                    borderTopLeftRadius: 30,
                    borderTopRightRadius: 30,
                    borderBottomLeftRadius: 30,
                    borderBottomRightRadius: 30,
                    marginTop: "1%",
                    marginLeft: "5%",
                    marginRight: "5%",

                    height: height - 80,
                    elevation: 1,
                    backgroundColor: colors.white
                  }}
                >
                  <View
                    style={{
                      height: "10%",
                      borderTopLeftRadius: 30,
                      borderTopRightRadius: 30,
                      flexDirection: "row",
                      backgroundColor: colors.colorSignin
                    }}
                  >
                    <TouchableOpacity
                      onPress={this.kill.bind(this)}
                      style={{
                        flex: 2,
                        alignItems: "center",
                        height: 40,
                        marginTop: "5%",
                        width: 40
                      }}
                    >
                      <Image source={require("../../src/assets/backnew.png")}/>
                    </TouchableOpacity>
                    <Text
                      style={{
                        alignSelf: "center",
                        fontSize: 18,

                        flex: 6,
                        marginTop: "2%",
                        fontWeight: "bold",
                        textAlign: "center",
                        color: colors.white
                      }}
                    >
                      Worker List
                    </Text>
                    <TouchableOpacity
                      onPress={this.handleClickAddWorker.bind(this)}
                      style={{
                        flex: 2,
                        height: 40,
                        marginTop: "2%",
                        width: 40
                      }}
                    >
                      <Image source={require("../../src/assets/add.png")}/>
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      display: this.state.display,
                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        color: "black",
                        fontSize: 18
                      }}
                    >
                      {this.state.status}
                    </Text>
                  </View>
                  <View
                    onStartShouldSetResponderCapture={() => {
                      this.setState({enableScrollViewScroll: false});
                      if (this.state.enableScrollViewScroll === false) {
                        this.setState({enableScrollViewScroll: true});
                      }
                    }}
                    style={{
                      marginLeft: "1%",

                      marginBottom: "4%",
                      height: height - 250,
                      marginRight: "1%"
                    }}
                  >
                    <Spinner
                      visible={this.state.spinner}
                      textContent={"Adding worker, please wait..."}
                      textStyle={styles.spinnerTextStyle}
                    />
                    <FlatList
                      style={{marginTop: 1}}
                      data={this.state.WorkerList}
                      renderItem={({item, index}) => (
                        <View style={{}}>
                          <View
                            style={{
                              borderRadius: 10,

                              width: "90%",
                              borderColor: "red",

                              justifyContent: "center",
                              backgroundColor: colors.white,
                              alignSelf: "center"
                            }}
                          >
                            <Text
                              style={{
                                color: "black",
                                justifyContent: "center",
                                alignItems: "center",
                                fontWeight: "bold",
                                paddingLeft: 10,
                                height: 17,
                                marginTop: 3
                              }}
                            >
                              {item.first_name + " " + item.last_name}
                            </Text>

                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  marginTop: "1%"
                                }}
                                source={require("../../src/assets/mobile-work.png")}
                              />
                              <Text
                                style={{
                                  marginTop: "5%",
                                  marginLeft: "4%"
                                }}
                              >
                                {item.contact_number}
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: "row",
                                height: 1,
                                backgroundColor: colors.gray01
                              }}
                            />
                          </View>
                        </View>
                      )}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  spinnerTextStyle: {
    color: colors.colorGradient,
    backgroundColor: colors.colorSignin,
    height: 100,
    borderRadius: 5,
    padding: 10,
    width: width - 60
  }
});
