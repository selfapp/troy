import React, {Component} from "react";
import {
  Dimensions,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import Moment from "moment";
import CheckBox from "react-native-checkbox";

import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import colors from "../styles/colors";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;

export default class SelectContractorForJob extends Component {
  constructor() {
    super();

    this.state = {
      uuid: "",
      checked: true,
      auth_token: "",
      contractorList: [],
      contractorAddedList: [],
      allContractorIds:[],
      loader:false
    };
  }

  componentDidMount() {
    console.log("this.props.navigation.state.params.")
    console.log(this.props.navigation.state.params.jobId)
    this._retrieveData();
  }

  handleClick = () => {
    {
      this.props.navigation.navigate("JobApplicantLaborConList");
    }
  };

  changeCheckState(contractor_uuid, ischecked) {

    console.log("ischecked ", ischecked)
    if (ischecked === true) {
      if(this.state.contractorAddedList.indexOf(contractor_uuid) === -1) {
        this.state.contractorAddedList.push(contractor_uuid);
      }
    }else{
      const filteredItems = this.state.contractorAddedList.filter(item => item !== contractor_uuid)
      this.setState({contractorAddedList:filteredItems})
    }
  }

  sendRequestToAllContractor() {
    this.sendRequestToContractor('all')
    // this.props.navigation.navigate("JobsApplicantList")
  }

  sendRequestToContractor(value) {

    if(this.state.contractorAddedList.length > 0 || value === 'all'){
      console.log("Contractor list are")
      console.log(this.state.contractorAddedList)
      this.setState({loader:true})
     
      var myObject;

      if(value ==='indivual') {
         myObject = {      
          user_ids: this.state.contractorAddedList
      };
      }else{
        myObject = {      
          user_ids: this.state.allContractorIds
        };
      }
      // var myObject = {      
      //   user_id: this.state.contractorAddedList
      // };
     
      console.log("my object", myObject)
      console.log("my object", JSON.stringify(myObject))
      console.log("this.state.auth_token", this.state.auth_token)
      console.log("this.props.navigation.state.params.jobId}",this.props.navigation.state.params.jobId)
      fetch(`${baseURL}/jobs/${this.props.navigation.state.params.jobId}/send-notification`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.auth_token
          },
          body: JSON.stringify(myObject)
        })
        .then(response => response.json(console.log("response ", response)))
        .then(responseJson => {

          console.log("response contractor send notification")
          console.log(responseJson)
          this.setState({loader:false})  
          this.props.navigation.navigate("JobsApplicantList")
          try {
          } catch (error) {
            // Error saving data
            console.log(error)
          }
        })
        .catch(e => {
          console.log(e)
          this.setState({loader:false})
        });
    }else{
      alert("Please select atleast one staffing contractor.")
    }
  }

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const access_token = await AsyncStorage.getItem("access_token");

      this.setState({ auth_token: access_token });
       
      fetch(`${baseURL}/jobs/list-lc`, {
        method: "GET",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          console.log("repsosne contractor list")
          console.log(responseJson)
          var contractorIds =[]
          responseJson.user.map((item)=>{
            contractorIds.push(item.id)
          })
          
          this.setState({
            contractorList: responseJson.user,
            allContractorIds:contractorIds
          });
          console.log("all contractor ids")
          console.log(this.state.allContractorIds)
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
       
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 40, alignItems: 'center'
          }}
        >
          {/* <TouchableOpacity
            style={{width: 30}}
            onPress={this.handleClick.bind(this)}
          >
            <Image
              style={{alignSelf: "flex-start"}}
              source={require("../../src/assets/backnew.png")}
            />
          </TouchableOpacity> */}

          <View style={{flex: 1, marginTop: 20}}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 22,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Choose Contractor
            </Text>
          </View>
        </View>

        <TouchableOpacity style={{marginHorizontal:'8%', marginTop:5}}
          onPress={()=> this.sendRequestToAllContractor()}>
            <Text style={{fontWeight:'bold', fontSize:17}}>
                Send job request to all contractor.
            </Text>
        </TouchableOpacity>
        <Text style={{fontWeight:'bold', fontSize:17,marginHorizontal:'8%', marginTop:5}}>
                OR
        </Text>
        <Text style={{fontWeight:'bold', fontSize:17,marginHorizontal:'8%', marginTop:5}}>
                Manually select contractor from below list.
            </Text>
        <View
          style={{
            marginTop: "1%"
          }}
        >

{this.state.loader ? <MyActivityIndicator /> : null}

          <View
            style={{
              borderRadius: 10,
              marginHorizontal: "8%",
              marginTop: "4%",
              height: height / 2 -50,
              backgroundColor: colors.white
            }}
          >
            <FlatList
              style={{marginTop: 1, height: '10%'}}
              data={this.state.contractorList}
              renderItem={({item, index}) => (
                <View
                  style={{
                    borderRadius: 10,
                    marginBottom: "4%",
                    width: "100%",
                    borderColor: "red",
                    flexDirection: "column",
                    alignSelf: "center",
                    backgroundColor: colors.gray01,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      margin: 5,
                      alignItems: "center",
                      justifyContent: 'center'
                    }}
                  >
                    <CheckBox
                      checkedImage={require("../assets/check.png")}
                      label=""
                      // checked={item.assigned}
                      uncheckedImage={require("../assets/uncheck.png")}
                      onChange={checked => this.changeCheckState(item.id, checked)}
                    />

                <View
                      style={{
                        flex: 2.5,
                        marginLeft:15,

                        flexDirection: "column"
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold"
                        }}
                      >
                        {item.first_name + " " + item.last_name}
                      </Text>
                      <Text
                        style={{
                          fontSize: 13
                        }}
                      >
                        {item.contact_number}
                      </Text>
                      <Text
                        style={{
                          fontSize: 13
                        }}
                      >
                        {item.city + ", " + item.state}
                      </Text>
                    </View>
                    
                  </View>
                 
                </View>
              )}
            />
          </View>
        </View>

        <View
          style={{
            height: 40,
            justifyContent: "center",
            backgroundColor: colors.colorYogi,
            width: "40%",
            borderRadius: 20,
            alignSelf: "center",
            marginTop: 25
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this.sendRequestToContractor('indivual');
            }}
          >
            <Text
              style={{
                color: "white",
                alignSelf: "center",
                justifyContent: "center",
                fontSize: 16
              }}
            >
              Send
            </Text>
          </TouchableOpacity>
        </View>
      </BackgroundNtScroll>
    );
  }
}
