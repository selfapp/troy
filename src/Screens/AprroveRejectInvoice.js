import React, {Component} from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  FlatList,
  Alert,
  TouchableOpacity,
  AsyncStorage,
  Text,
  View
} from "react-native";
import Moment from "moment";

import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import colors from "../styles/colors";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

const height = Dimensions.get("window").height;

export default class AprroveRejectInvoice extends Component {
  constructor() {
    super();
    this.state = {
      calenderSelectedValue: [],
      currentDateForCalender: "",
      currentDate: "",
      dateInitailNumber: "",
      jobTitle: "",
      workerName: "",
      workingHours: "",
      approvedStatus: "",
      dateMiddleText: "",
      dateLastText: "",
      scheduleData: [],
      DataJsonColored: "",
      selectedGcName: "",
      selectedEmailAddress: "",
      selectedAddress: "",
      selectedWorkingHours: "",
      firstSelectedDate: "",
      modalVisible: false,
      approveDisplay: "none",
      approveButtonDisplay: "none",
      uuid: "",
      auth_token: "",
      SelectProjectData: [],
      projectListRaw: [],
      projectList: [],
      status: "",
      ProjectNameAsUUID: "",
      ProjectName: "Select Project",
      project_uuid: "",
      timecardsArray: [],
      billAmount: "",
      amountPrice:'',
      loader:false
    };
  }

  componentDidMount() {
    this._retrieveData();
  }

  _retrieveData = async index => {
    try {
      this.setState({loader:true})
      var data = {
        title: "Job",
        description: "Job description",
        date: "2018-11-14",
        timecards: [
          {
            in: "2018-11-14T09:03:11.000Z",
            out: "2018-11-14T09:32:34.000Z",
            total: "29 m",
            worker: "Worker 1",
            status: "Approved"
          },
          {
            in: "2018-11-14T09:04:43.000Z",
            out: "2018-11-14T09:32:28.000Z",
            total: "30 m",
            worker: "Worker 2",
            status: "Approved"
          }
        ],
        total: "59 m"
      };
      this.setState({projectList: [data]});
      const uuid = await AsyncStorage.getItem("uuid");
      const uuid_jobs = await AsyncStorage.getItem("job_uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const uuid_invoice = await AsyncStorage.getItem("uuid_invoice");

      if (uuid !== null) {
        this.setState({uuid: uuid});
        this.setState({auth_token: access_token});
      }

      fetch(
        `${baseURL}/gc/invoices/${uuid_jobs}/${uuid_invoice}`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.auth_token
          }
        }
      )
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loader:false})
          console.log("response apprive iinvice")
          console.log(responseJson)
          // responseJson.jobs.from_date.substring(0, 5);
          this.setState({
            projectList: responseJson.invoice
          });
          this.setState({
            timecardsArray: this.state.projectList.workers
          });
          if (this.state.projectList.workers.length === 0) {
            this.setState({
              status: "No invoice found.",
              approveDisplay: "none"
            });
          } else {
            this.setState({
              status: "",
              approveDisplay: "flex",
              approveButtonDisplay: "flex"
            });
          }
          if (this.state.projectList.status === "Paid") {
            this.setState({
              approveButtonDisplay: "none"
            });
          }
          this.getCurrentDateParams();
        })
        .catch(e => {
          this.setState({loader:false})
        });
    } catch (error) {
      // Error retrieving data
    }
  };

  handleClick = () => {
    this.props.navigation.navigate("SelectedInvoicesGC");
  };

  _rejectInvoice = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const uuid_jobs = await AsyncStorage.getItem("job_uuid");
      const uuid_invoice = await AsyncStorage.getItem("uuid_invoice");
      if (uuid !== null) {
        // We have data!!
        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        fetch(
          `${baseURL}/gc/invoices/${uuid_jobs}/${uuid_invoice}`,
          {
            method: "DELETE",

            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Token token=" + this.state.access_token
            }
          }
        )
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            Alert.alert(
              "",
              "Invoice Declined Successfully.",
              [
                {
                  text: "OK"
                }
              ],
              {cancelable: false}
            );
            this._retrieveData();
          })
          .catch(e => {
            this.setState({loader:false})
          });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  _acceptInvoice = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const uuid_jobs = await AsyncStorage.getItem("job_uuid");
      const uuid_invoice = await AsyncStorage.getItem("uuid_invoice");

      if (uuid !== null) {
        // We have data!!
        this.setState({uuid: uuid});
        this.setState({access_token: access_token});

        fetch(
          `${baseURL}/gc/invoices/${uuid_jobs}/${uuid_invoice}`,
          {
            method: "POST",

            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Token token=" + this.state.access_token
            }
          }
        )
          .then(response => response.json())
          .then(responseJson => {
            this.setState({loader:false})
            Alert.alert(
              "",
              "Paid Successfully.",
              [
                {
                  text: "OK"
                }
              ],
              {cancelable: false}
            );
            this._retrieveData();
          })
          .catch(e => {
            this.setState({loader:false})
          });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  getCurrentDateParams() {
    //console.log("called params" + date2);

    // var dateString = date2;

    // const datex = Moment(dateString).format("YYYY-MM-DD");

    // console.log("current time after parse " + datex);

    // const date = Moment(dateString).format("D MMM YYYY");
    // const dateInitail = Moment(dateString).format("DD");

    // const dateMiddle = Moment(dateString).format("MMMM YYYY");

    // const dateLast = Moment(dateString).format("dddd");

    // this.setState({ currentDate: date });
    // this.setState({ dateInitailNumber: dateInitail });
    // this.setState({ dateMiddleText: dateMiddle });

    // this.setState({ dateLastText: dateLast });
    // console.log(this.state.projectList);
    this.setState({
      jobTitle: this.state.projectList.title,
      workerName: this.state.projectList.title,
      workingHours: this.state.projectList.value,
      approvedStatus: this.state.projectList.status,
      billAmount: this.state.projectList.total_hours,
      amountPrice: this.state.projectList.amount
      // timecardsArray: this.state.projectList[0].timecards
    });

    //("YYYY-MM-DD hh:mm:ss"))
  }

  render() {
    return (
      <BackgroundNtScroll>
        
        <View
          style={{
            height: 50,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 40, alignItems: 'center'
          }}
        >
          {
            <TouchableOpacity
              style={{width: 30}}
              onPress={this.handleClick.bind(this)}
            >
              <Image
                style={{alignSelf: "flex-start"}}
                source={require("../../src/assets/backnew.png")}
              />
            </TouchableOpacity>
          }

          <View style={{flex: 1, marginTop: 20}}>
            <Text
              style={{
                flex: 1,
                color: colors.black,
                fontSize: 24,
                alignSelf: "center",
                fontWeight: "400"
              }}
            >
              Approve invoice
            </Text>
          </View>
        </View>

        <ScrollView style={{}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomLeftRadius: 15,
              borderBottomRightRadius: 15,
              marginTop: "3%",
              marginHorizontal: "5%",
              marginBottom: 10,
              height: height / 2 + 270,
              elevation: 1,
              backgroundColor: colors.gray01
              // backgroundColor: 'green'
            }}
          >
            <View
              style={{
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  marginTop: 10,
                  marginLeft: 20,
                  width: 20,
                  backgroundColor: colors.gray01,
                  height: 60,
                  borderRadius: 35,
                  alignContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    alignContent: "center",
                    alignItems: "center",
                    fontSize: 20,

                    marginTop: "25%",
                    fontWeight: "bold",
                    color: colors.colorClipTop
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 0.9
                }}
              >
                <View
                  style={{
                    flex: 1.5,
                    marginLeft: 10
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      alignSelf: "center",
                      textAlignVertical: "bottom",
                      marginTop: 12,
                      fontWeight: "bold",
                      color: colors.colorClipTop
                    }}
                  >
                    {Moment(this.state.projectList.from).format("D MMM YYYY") +
                    "  to  " +
                    Moment(this.state.projectList.to).format("D MMM YYYY")}
                  </Text>
                </View>

                <View style={{flex: 1}}>
                  <Text
                    style={{
                      alignSelf: "center",
                      fontSize: 24,
                      marginTop: "2%",
                      fontWeight: "bold",
                      color: colors.black
                    }}
                  >
                    {this.state.workingHours}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                marginTop: 22,
                backgroundColor: colors.gray01,
                height: 1
              }}
            />
            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                height: "13%"
              }}
            >
              <View
                style={{
                  flex: 0.6,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/worker-icon.png")}
                  />
                </View>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Job Title
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.7,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.jobTitle}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 5,
                height: "20%"
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/worker-icon.png")}
                  />
                </View>
                <View style={{flex: 0.9, height: 30}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Worker List
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",
                  marginLeft: "3%", marginTop: -25
                }}
              >
                <FlatList
                  style={{
                    marginTop: 1,
                    marginLeft: "12%",
                    borderTopLeftRadius: 15,
                    borderTopRightRadius: 15,
                    borderBottomLeftRadius: 15,
                    borderBottomRightRadius: 15,
                    height: height / 6,
                    width: 120,
                    elevation: 1,
                    backgroundColor: colors.white,
                    display: this.state.approveDisplay
                  }}
                  data={this.state.timecardsArray}
                  renderItem={({item, index}) => (
                    <View style={{height: 20, margin: 10}}>
                      <Text
                        style={{
                          fontSize: 11,

                          color: colors.colorClipTop,
                          fontWeight: "bold",

                          textAlignVertical: "center"
                        }}
                      >
                        {index +
                        1 +
                        ") Worker " +
                        item.name +
                        " worked on " +
                        this.state.jobTitle +
                        "."}
                      </Text>
                    </View>
                  )}
                />
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 78,
                height: 70
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/clock.png")}
                  />
                </View>
                <View style={{flex: 0.5}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,

                      color: colors.colorClipTop,
                      fontWeight: "bold",

                      textAlignVertical: "center"
                    }}
                  >
                    Working hours
                  </Text>
                </View>
              </View>
              

              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,

                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.billAmount}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 5,
                height: 70
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/price.png")}
                  />
                </View>
                <View style={{flex: 0.5}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      fontSize: 16,
                      color: colors.colorClipTop,
                      fontWeight: "bold",
                      textAlignVertical: "center"
                    }}
                  >
                    Amount
                  </Text>
                </View>
                <View style={{flex: 0.4}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      fontSize: 16,
                      color: colors.colorClipTop,
                      fontWeight: "bold",
                      textAlignVertical: "center"
                    }}
                  >
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      height: 20,
                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.amountPrice}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                margin: "1%",
                flexDirection: "column",
                marginTop: 5,
                height: 70
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}>
                  <Image
                    style={{height: 25, width: 25, marginTop: 5}}
                    source={require("../../src/assets/time-clock.png")}
                  />
                </View>
                <View style={{flex: 0.5}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      fontSize: 16,
                      color: colors.colorClipTop,
                      fontWeight: "bold",
                      textAlignVertical: "center"
                    }}
                  >
                    Status
                  </Text>
                </View>
                <View style={{flex: 0.4}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      fontSize: 16,
                      color: colors.colorClipTop,
                      fontWeight: "bold",
                      textAlignVertical: "center"
                    }}
                  >
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "row",

                  marginLeft: "3%"
                }}
              >
                <View style={{flex: 0.1}}/>
                <View style={{flex: 0.9}}>
                  <Text
                    style={{
                      flex: 1,
                      marginLeft: 20,
                      height: 20,
                      fontSize: 16,
                      textAlignVertical: "top",
                      color: colors.black,
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.approvedStatus}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <View style={{flex: 1, justifyContent: "center"}}>
          <Text style={{alignSelf: "center", color: "black"}}>
            {this.state.status}
          </Text>
        </View>
        <View style={{height: 50, flexDirection: "row", display: this.state.approveButtonDisplay}}>
          <TouchableOpacity
            onPress={this._rejectInvoice.bind(this)}
            style={{
              flex: 1,
              justifyContent: "center",
              marginLeft: "5%",
              marginRight: "5%",
              marginBottom: "1%",
              borderRadius: 5,
              backgroundColor: colors.colorSignin
            }}
          >
            <Text
              style={{
                alignSelf: "center",
                color: "white",
                fontWeight: "bold"
              }}
            >
              Reject
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this._acceptInvoice.bind(this)}
            style={{
              flex: 1,
              justifyContent: "center",
              marginLeft: "5%",
              marginRight: "5%",
              marginBottom: "1%",
              borderRadius: 5,
              backgroundColor: colors.colorSignin
            }}
          >
            <Text
              style={{
                alignSelf: "center",
                color: "white",
                fontWeight: "bold"
              }}
            >
              Approve
            </Text>
          </TouchableOpacity>
        </View>
      </BackgroundNtScroll>
    );
  }
}
