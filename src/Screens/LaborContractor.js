
import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import ModalDropdown from "react-native-modal-dropdown";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  Dimensions,
  ImageBackground,
  StatusBar,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import colors from "../styles/colors";
import {API} from '../../api';
import {baseURL} from '../../api';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export default class LaborContractor extends Component {
  constructor() {
    super();
    this.state = {
      FirstName: "",
      LastName: "",
      EmailId: "",
      StreetAddress: "",
      City: "",
      State: "State",
      spinner: false,
      zipcode: "",
      textvalue: "",
      CompanyName: "",
      uuid: "",
      auth_token: "",
      avatarSource: "",
      liscenseUri:'',
      gcm_token: "",
      my_Id: "",
      loader:false,
      checkEditProfile:false,
      checkImageChange:false
    };
  }

  componentWillMount() {
    if(this.props.navigation.state.params){
      if(this.props.navigation.state.params.editProfile){
        this.setState({checkEditProfile:true})
      }
    }
    console.log("Labour contractor called ....")
    this._retrieveData();
  }

  validateFields() {
    if (
      this.state.FirstName === "" ||
      this.state.LastName === "" ||
      this.state.EmailId === "" ||
      this.state.StreetAddress === "" ||
      this.state.City === "" ||
      this.state.State === "State" ||
      this.state.zipcode === "" ||
      this.state.CompanyName === "" ||
      this.state.avatarSource === ""
    ) {
      Alert.alert("", "Fields are mandatory.");
      return false;
    } else {
      return true;
    }
  }

  fetchImageFromGallery() {
    var ImagePicker = require("react-native-image-picker");

    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info below in README)
     */
    ImagePicker.showImagePicker(response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {

        console.log("Image picker response")
        console.log(response)
        let data = `data:${response.mime};base64,${response.data}`

        this.setState({
          avatarSource: data,
          checkImageChange:true
        });
      }
    });
  }

  validate = text => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return true;
    } else {
      return true;
    }
  };

  _retrieveData = async () => {
    console.log("retreive data called ")
    var _this = this;

    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const token = await AsyncStorage.getItem("gcm_token", value => {
        JSON.parse(value); // boolean false
      });
      this.setState({gcm_token: token});

      if (uuid !== null) {
        this.setState({
          uuid: uuid,
          access_token: access_token
        });

        try {
          let response = await API.getRequest('/users/' + this.state.uuid, 'GET');
          console.log("print response ...")
          console.log(response)
          _this.setState({loader:false})
          if (response.status === 200) {
            response.json()
              .then(function (responseJson) {
                
                console.log("response json LC ")
                console.log(responseJson.user)
                _this.setState({
                  FirstName: responseJson.user.first_name,
                  LastName: responseJson.user.last_name,
                  EmailId: responseJson.user.email,
                  StreetAddress: responseJson.user.street_address,
                  City: responseJson.user.city,
                  State: responseJson.user.state,
                  zipcode: responseJson.user.zip_code,
                  role: responseJson.user.role,
                  CompanyName: responseJson.user.company_name
                });

                if (responseJson.user.file != null) {
                  _this.setState({avatarSource: responseJson.user.file});
                  _this.setState({liscenseUri: responseJson.user.file});
                }

                if (responseJson.user.state == null) {
                  _this.setState({State: "State"});
                }

              })
          } else {
            this.setState({loader:false})
          }
        } catch (error) {
          this.setState({loader:false})
          Alert.alert(
            "",
            "Network issue occured, try again.",
            [
              {
                text: "OK"
              }
            ],
            {cancelable: false}
          );
        }


      }
    } catch (error) {
    }
  };

  apiCallUserProfileUpdate = () => {
    if (this.validateFields()) {
      if (this.validate(this.state.EmailId)) {
        this.setState({spinner:true})
var myObject;
    if(this.state.checkImageChange){
       myObject = {
        user: {
          first_name: this.state.FirstName,
          last_name: this.state.LastName,
          email: this.state.EmailId,
          street_address: this.state.StreetAddress,
          city: this.state.City,
          state: this.state.State,
          zip_code: this.state.zipcode,
          file: this.state.avatarSource,
          role: "LC",
          gcm_token: this.state.gcm_token,
          company_name: this.state.CompanyName
        }
      };
    }else{
       myObject = {
        user: {
          first_name: this.state.FirstName,
          last_name: this.state.LastName,
          email: this.state.EmailId,
          street_address: this.state.StreetAddress,
          city: this.state.City,
          state: this.state.State,
          zip_code: this.state.zipcode,
          // file: this.state.avatarSource,
          role: "LC",
          gcm_token: this.state.gcm_token,
          company_name: this.state.CompanyName
        }
      };
    }
        // var myObject = {
        //   user: {
        //     first_name: this.state.FirstName,
        //     last_name: this.state.LastName,
        //     email: this.state.EmailId,
        //     street_address: this.state.StreetAddress,
        //     city: this.state.City,
        //     state: this.state.State,
        //     zip_code: this.state.zipcode,
        //     file: this.state.avatarSource,
        //     role: "LC",
        //     gcm_token: this.state.gcm_token,
        //     company_name: this.state.CompanyName
        //   }
        // };
        console.log("Bosy object for edit profile ...")
        console.log(myObject)

        fetch(`${baseURL}/users/${this.state.uuid}`, {
          method: "PATCH",

          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Token token=" + this.state.access_token
          },
          body: JSON.stringify(myObject)
        })
          .then(response => response.json())
          .then(responseJson => {
            console.log("response after update profile")
            console.log(responseJson)
            try {

              this.setState({spinner:false})
              if(this.state.checkEditProfile){
                this.props.navigation.navigate("LCProfile");
              }else{
                AsyncStorage.setItem("userType", "LC");
                this.props.navigation.navigate("PROPOSAL");
              }
            } catch (error) {
              this.setState({spinner:false})
            }
          })
          .catch(e => {
            this.setState({spinner:false})
            Alert.alert(
              "",
              "Network issue occured, try again.",
              [
                {
                  text: "OK"
                }
              ],
              {cancelable: false}
            );
          });

        //
      } else {

        Alert.alert("", "Please enter correct Email address.");
      }
    } else {
    }
  };

  handleClick = () => {
    console.log("caled handle click...")
    if(this.state.checkEditProfile){
      this.props.navigation.navigate("LCProfile");
    }else{
      this.props.navigation.navigate("People");
    }
  };

  render() {

    // console.log("render called ..........")
    var keyboardBehavior = ''
    if(Platform.OS === 'ios'){
      keyboardBehavior = 'padding'
    }else {
      keyboardBehavior = 'height'
    }

    return (
      <ImageBackground
        source={require("../../src/assets/bg-screen.png")}
        resizeMode="stretch"
        style={{flex: 1}}
      >
        <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated/>
        <Spinner
              visible={this.state.spinner}
              textContent={"Initializing app setting, please wait..."}
              textStyle={styles.spinnerTextStyle}
            />
        <View
          style={{
            height: 30,
            flexDirection: "row",
            marginLeft: "5%",
            marginRight: "5%",
            marginTop: 50, alignItems: 'center'
          }}
        >
          <TouchableOpacity
            style={{flex: 0.4}}
            onPress={this.handleClick.bind(this)}
          >
            <Image source={require("../../src/assets/backnew.png")}/>
          </TouchableOpacity>


        </View>

        <View style={{height: 40, alignItems: "center", marginTop: -5}}>
          <Text
            style={{
              color: colors.black,
              fontSize: 24,
              fontWeight: "bold"
            }}
          >
          {this.state.checkEditProfile ? "Edit Profile" : "Register"}

          </Text>
        </View>
        {this.state.checkEditProfile ? (null) : (
          <View style={{height: 20, alignItems: "center"}}>
            <Text
              style={{
                color: colors.gray02,
                fontSize: 16,
              }}
            >
              As a staffing company
            </Text>
          </View>
        )}
        
        <KeyboardAvoidingView behavior={keyboardBehavior} enabled>

        <ScrollView
          scrollEventThrottle={700}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={"handled"}
          keyboardDismissMode={"interactive"}
        >
           {this.state.loader ? <MyActivityIndicator /> : null}

          <View style={{height: 850}}>

            <View
              style={{
                borderRadius: 10,
                marginLeft: "10%",
                marginRight: "10%",

                width: "80%",
                height: height / 2 + 150,
              }}
            >
              <TextInput
                style={{
                  marginTop: 15,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10,
                }}
                underlineColorAndroid={colors.gray01}
                placeholder="First Name"
                autoCapitalize="none"
                value={this.state.FirstName}
                onChangeText={value => this.setState({FirstName: value})}
              />

              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.LastName}
                onChangeText={value => this.setState({LastName: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="Last Name"
                autoCapitalize="none"
              />
              <TextInput
                style={{
                  marginTop: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10,
                }}
                value={this.state.CompanyName}
                underlineColorAndroid={colors.gray01}
                placeholder="Company Name"
                autoCapitalize="none"
                onChangeText={value => this.setState({CompanyName: value})}
              />
              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.EmailId}
                onChangeText={value => this.setState({EmailId: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="Email Id"
                autoCapitalize="none"
                keyboardType={'email-address'}
              />

              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.StreetAddress}
                onChangeText={value => this.setState({StreetAddress: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="Street Address"
                autoCapitalize="none"
              />
              <TextInput
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  height: "10%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
                value={this.state.City}
                onChangeText={value => this.setState({City: value})}
                underlineColorAndroid={colors.gray01}
                placeholder="City"
                autoCapitalize="none"
              />

              <View
                style={{
                  marginTop: 5,
                  height: "11%",
                  fontSize: 18,
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <View
                  style={{
                    flex: 1,

                    flexDirection: "row"
                  }}
                >
                  <ModalDropdown
                    style={{
                      flex: 0.4,
                      height: height * 0.07,
                      justifyContent: "center"
                    }}
                    options={[
                      "Alabama - AL",
                      "Alaska - AK",
                      "Arizona - AZ",
                      "Arkansas - AR",
                      "California - CA",
                      "Colorado - CO",
                      "Connecticut - CT",
                      "Delaware - DE",
                      "Florida - FL",
                      "Georgia - GA",
                      "Hawaii - HI",
                      "Idaho - ID",
                      "Illinois - IL",
                      "Indiana - IN",
                      "Iowa - IA",
                      "Kansas - KS",
                      "Kentucky - KY",
                      "Louisiana - LA",
                      "Maine - ME",
                      "Maryland - MD",
                      "Massachusetts - MA",
                      "Michigan - MI",
                      "Minnesota - MN",
                      "Mississippi - MS",
                      "Missouri - MO",
                      "Montana - MT",
                      "Nebraska - NE",
                      "Nevada - NE",
                      "New Hampshire - NH",
                      "New Jersey - NJ",
                      "New Mexico - NM",
                      "New York - NY",
                      "North Carolina - NC",
                      "North Dakota - ND",
                      "Ohio - OH",
                      "Oklahoma - OK",
                      "Oregon - OR",
                      "Pennsylvania - PA",
                      "Rhode Island - RI"
                    ]}
                    dropdownStyle={{
                      width: "40%",
                      fontWeight: "bold",
                    }}
                    defaultValue={this.state.State}
                    onSelect={(index, value) => this.setState({State: value})}
                    textStyle={{
                      paddingLeft: 10,
                      fontSize: 20,
                      color: colors.gray09
                    }}
                  />
                  <View style={{flex: 0.2}}/>
                  <View style={{flex: 0.4}}>
                    <TextInput
                      style={{
                        fontSize: 18,
                        marginLeft: 10,
                        marginRight: 10
                      }}
                      placeholder="Zip code"
                      autoCapitalize="none"
                      keyboardType='number-pad'
                      maxLength={6}
                      value={this.state.zipcode}
                      onChangeText={value => this.setState({zipcode: value})}
                    />
                  </View>
                </View>
              </View>

              <View
                style={{
                  height: 1,
                  marginLeft: 13,
                  marginRight: 13
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row"
                  }}
                >
                  <View style={{flex: 0.4, backgroundColor: colors.gray01}}/>
                  <View style={{flex: 0.2}}/>
                  <View style={{flex: 0.4, backgroundColor: colors.gray01}}/>
                </View>
              </View>
              {
                this.state.avatarSource.length > 0 ? (
                  <Image style={{
                      marginTop:10,
                      height:120,
                      width:120,  
                      marginLeft:width/2 - 100       
                  }} source={{uri:this.state.avatarSource}}>
                  </Image>
                ) : (null)
              }
              <View
                style={{
                  height: "10%",
                  justifyContent: "center",
                  backgroundColor: colors.gray02,
                  width: "60%",
                  borderRadius: 30,
                  alignSelf: "center",
                  marginTop: 10
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.fetchImageFromGallery();
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      alignSelf: "center",
                      justifyContent: "center",
                      fontSize: 14
                    }}
                  >
                    Upload license
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
              style={{
                marginTop: "15%",
                borderRadius: 30,
                marginLeft: "10%",
                marginRight: "10%",
                backgroundColor: colors.colorYogi,
                height: 50,
                width: "80%",
                alignItems: "center",
                marginBottom:30
              }}
              onPress={this.apiCallUserProfileUpdate.bind(this)}
            >
              <Text
                style={{
                  justifyContent: "center",
                  marginTop: "4%",
                  color: colors.white,
                  fontSize: 20,
                  height: 50,
                  fontWeight: "bold",
                  alignSelf: "center"
                }}
              >
              {this.state.checkEditProfile ? "Update Profile" : "Create Account"}

              </Text>
            </TouchableOpacity>
            </View>

           
          </View>
        </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  spinnerTextStyle: {
    color: colors.colorGradient,
    backgroundColor: colors.colorSignin,
    height: 100,
    borderRadius: 5,
    padding: 10,
    width: width - 60
  }
});

