import React, {Component} from "react";
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  Text,
  View,
  Platform,
  Alert
} from "react-native";
import Moment from "moment";
import {Calendar} from "react-native-calendars";
import {DrawerActions} from "react-navigation";
import { API } from '../../api';

import BackgroundNtScroll from "../Components/BackgroundNtScroll";
import colors from "../styles/colors";
import {baseURL} from '../../api';
import MyActivityIndicator from '../Components/activity_indicator';

export default class WorkerSchedule extends Component {
  constructor() {
    super();
    this.state = {
      calenderSelectedValue: [],
      currentDateForCalender: "",
      currentDate: "",
      dateInitailNumber: "",
      dateMiddleText: "",
      dateLastText: "",
      scheduleData: [],
      DataJsonColored: "",
      selectedGcName: "",
      selectedEmailAddress: "",
      selectedAddress: "",
      selectedWorkingHours: "",
      firstSelectedDate: "",
      profileUpdated: "false",
      loader:false
    };
  }

  componentDidMount() {
    this.getCurrentDate();
    this._retrieveData();
    this.setPushConfig();
  }

  setPushConfig() {
    let _this = this;
    var PushNotification = require("react-native-push-notification");

    PushNotification.configure({
      onRegister: function (token) {
        try {
          AsyncStorage.setItem(
            "gcm_token",
            JSON.parse(JSON.stringify(token.token))
          );
          API.savePushToken(token.token);
        } catch (error) {
          console.log("error while saving async");
        }
      },

      onNotification: function (notification) {
        console.log("notification received worker schedule....")
        console.log(notification)
        
        if (
          notification.foreground === true &&
          notification.userInteraction === false
        ){
            if(Platform.OS === 'ios'){
                Alert.alert("STAFF UP", notification.data.message);
            }
          }
        if (
          notification.foreground === true &&
          notification.userInteraction === true
        ) {
          if (notification.data.code === "001") {
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        } else if (
          notification.foreground === false ||
          notification.foreground === null
        ) {
          if (notification.code === "001") {
          } else if (notification.code === "002") {
            _this.props.navigation.navigate("JobsApplicantList");
          } else if (notification.code === "003") {
            _this.slideMenu();
          } else if (notification.code === "004") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          } else if (notification.code === "005") {
            _this.props.navigation.navigate("TimeSheetOrInvoice");
          }
        }
      },
      senderID: "900411929042",
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
      popInitialNotification: true,
      requestPermissions: true
    });
  }

  apiCallUserProfileUpdate = token => {
    this.setState({loader:true})
    var myObject = {
      user: {
        gcm_token: token
      }
    };

    fetch(`${baseURL}/users/${this.state.uuid}`, {
      method: "PATCH",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token token=" + this.state.auth_token
      },
      body: JSON.stringify(myObject)
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loader:false})
        console.log('response =>', responseJson);
        try {
          //  AsyncStorage.setItem("userType", "LC");
        } catch (error) {
          // Error saving data
          // We have data!!
          console.log("error while saving async");
        }
      })
      .catch(e => {
        this.setState({loader:false})
        console.log("called error " + e);
      });

    //
  };

  _retrieveData = async () => {
    try {
      this.setState({loader:true})
      const uuid = await AsyncStorage.getItem("uuid");
      const access_token = await AsyncStorage.getItem("access_token");
      const profileUpdated = await AsyncStorage.getItem("profileUpdated");

      if (uuid !== null) {
        // We have data!!
        this.setState({
          profileUpdated: profileUpdated,
          uuid: uuid,
          auth_token: access_token
        });
      }

      fetch(`${baseURL}/worker/timecards/schedule`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + this.state.auth_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log('response =>', responseJson)
          this.setState({loader:false})
          this.setState({
            scheduleData: responseJson.jobs
          });
          this.setJsonDataforDate();
        })
        .catch(e => {
        });
    } catch (error) {
      this.setState({loader:false})
      // Error retrieving data
    }
  };

  handleClick = () => {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  };

  setJsonDataforDate() {
    console.log('schedule length ', this.state.scheduleData)
    var finalDates = [];
    for (let i = 0; i < this.state.scheduleData.length; i++) {
      var type = this.state.scheduleData[i].date;
      this.setState({firstSelectedDate: type});

      finalDates[type] = {
        customStyles: {
          container: {
            backgroundColor: colors.colorClipTop,
            elevation: 8
          },
          text: {
            color: "white"
          }
        }
      };
    }
    this.getCurrentDateParamsMM_DD_YYY(this.state.firstSelectedDate);

    if (this.state.scheduleData.length == 0) {
      this.setState({selectedWorkingHours: "No jobs found on this date."});
      this.getCurrentDate();
    }
    this.setState({calenderSelectedValue: finalDates});
  }

  getCurrentDate() {
    console.log("current time is -" + Moment(Moment()).format("D MMM YYYY"));

    // Moment(Moment()).format("D MMM YYYY");
    // ("YYYY-MM-DD hh:mm:ss");
    const date1 = Moment(Moment()).format("D MMM YYYY");
    console.log('current date = ', date1)
    const dateInitail = Moment(Moment()).format("DD");

    const dateMiddle = Moment(Moment()).format("MMMM YYYY");

    const dateLast = Moment(Moment()).format("dddd");

    this.setState({currentDateForCalender: date1});
    this.setState({dateInitailNumber: dateInitail});
    this.setState({dateMiddleText: dateMiddle});

    this.setState({dateLastText: dateLast});

    //("YYYY-MM-DD hh:mm:ss"))
  }

  getCurrentDateParams(date2) {
    console.log("called params" + date2);

    var dateString = Moment(date2).format("L");

    const datex = Moment(dateString).format("YYYY-MM-DD");

    console.log("current time after parse " + datex);

    const date = Moment(dateString).format("D MMM YYYY");
    const dateInitail = Moment(dateString).format("DD");

    const dateMiddle = Moment(dateString).format("MMMM YYYY");

    const dateLast = Moment(dateString).format("dddd");

    this.setState({currentDate: date});
    this.setState({dateInitailNumber: dateInitail});
    this.setState({dateMiddleText: dateMiddle});

    this.setState({dateLastText: dateLast});

    var valuesof = {
      jobs: [
        {
          uuid: "ecd2c0a2-8499-43bf-9d70-2ef27284996f",
          gc: "Jagjot Singh",
          email: null,
          address: "test, test city, test state - test zip code",
          date: "2018-11-14"
        },
        {
          uuid: "ecd2c0a2-8499-43bf-9d70-2ef27284996f",
          gc: "Jagjot Singh",
          email: null,
          address: "test, test city, test state - test zip code",
          date: "2018-11-15"
        }
      ]
    };

    for (let i = 0; i < this.state.scheduleData.length; i++) {
      var type = this.state.scheduleData[i].date;

      console.log("current time after parse " + type);
      console.log("working hour")
      console.log(this.state.scheduleData[i])
      if (datex == type) {
        this.setState({
          selectedEmailAddress: this.state.scheduleData[i].email,
          selectedGcName: this.state.scheduleData[i].gc,
          selectedWorkingHours: this.state.scheduleData[i].workinghours,
          selectedAddress: this.state.scheduleData[i].address
        });

        break;
      } else {
        this.setState({
          selectedEmailAddress: "",
          selectedGcName: "",
          selectedWorkingHours: "No jobs found on this date.",
          selectedAddress: ""
        });
      }
    }
    if (this.state.scheduleData.length == 0) {
      this.setState({
        selectedEmailAddress: "",
        selectedGcName: "",
        selectedWorkingHours: "No jobs found on this date.",
        selectedAddress: ""
      });
    }

    //("YYYY-MM-DD hh:mm:ss"))
  }

  getCurrentDateParamsMM_DD_YYY(dateString) {
    console.log("date string is", dateString);

    // var dateString = Moment(date2).format("L");
    if (dateString != null) {
      const datex = Moment(dateString).format("YYYY-MM-DD");

      console.log("current time after parse " + datex);

      const date = Moment(dateString).format("D MMM YYYY");
      const dateInitail = Moment(dateString).format("DD");

      const dateMiddle = Moment(dateString).format("MMMM YYYY");

      const dateLast = Moment(dateString).format("dddd");

      this.setState({currentDate: date});
      this.setState({dateInitailNumber: dateInitail});
      this.setState({dateMiddleText: dateMiddle});

      this.setState({dateLastText: dateLast});

      var valuesof = {
        jobs: [
          {
            uuid: "ecd2c0a2-8499-43bf-9d70-2ef27284996f",
            gc: "Jagjot Singh",
            email: null,
            address: "test, test city, test state - test zip code",
            date: "2018-11-14"
          },
          {
            uuid: "ecd2c0a2-8499-43bf-9d70-2ef27284996f",
            gc: "Jagjot Singh",
            email: null,
            address: "test, test city, test state - test zip code",
            date: "2018-11-15"
          }
        ]
      };

      for (let i = 0; i < this.state.scheduleData.length; i++) {
        var type = this.state.scheduleData[i].date;

        console.log("current time after parse " + type);
        console.log("working hour")
        console.log(this.state.scheduleData[i])
        if (datex == type) {
          this.setState({
            selectedEmailAddress: this.state.scheduleData[i].email,
            selectedGcName: this.state.scheduleData[i].gc,
            selectedWorkingHours: this.state.scheduleData[i].workinghours,
            selectedAddress: this.state.scheduleData[i].address
          });

          break;
        } else {
          this.setState({
            selectedEmailAddress: "",
            selectedGcName: "",
            selectedWorkingHours: "No jobs found on this date.",
            selectedAddress: ""
          });
        }
      }
      if (this.state.scheduleData.length == 0) {
        this.setState({
          selectedEmailAddress: "",
          selectedGcName: "",
          selectedWorkingHours: "No jobs found on this date.",
          selectedAddress: ""
        });
      }
    }
    //("YYYY-MM-DD hh:mm:ss"))
  }

  render() {
    var _this = this;
    Moment.locale("en");
    return (
      <BackgroundNtScroll>
        <View style={{flex: 1, alignItems: 'center'}}>
        {this.state.loader ? <MyActivityIndicator /> : null}
          <View
            style={{
              height: 50,
              flexDirection: "row",
              marginTop: 20, width: '100%'
            }}
          >
            <TouchableOpacity
              style={{marginTop: 30, marginLeft: 10}}
              onPress={this.handleClick.bind(this)}
            >
              <Image
                source={require("../../src/assets/h-menu.png")}
              />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              marginTop: -10,
              color: 'black',
              fontSize: 24,
              fontWeight: "400",
              textAlign: 'center'
            }}
          >
            Schedule
          </Text>
          <ScrollView
            style={{
              height: 800, width: '100%'
            }}
          >
            <View
              style={{
                // backgroundColor: colors.gray01,
                marginLeft: 20,
                marginRight: 20,
                marginTop: 10,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
                borderBottomLeftRadius: 15,
                borderBottomRightRadius: 15,
                marginBottom: 10,
                elevation: 1,
                borderWidth: 0.5,
                borderColor: colors.gray01
                // height: 350
              }}
            >
              <Calendar
                style={{
                  marginHorizontal: 5,
                  marginVertical: 5
                }}
                theme={{
                  backgroundColor: colors.gray04,
                  calendarBackground: "#ffffff",
                  textSectionTitleColor: "#b6c1cd",
                  selectedDayBackgroundColor: colors.colorGradient,
                  selectedDayTextColor: colors.gray04,
                  todayTextColor: colors.colorGradient,
                  dayTextColor: colors.black,
                  textDisabledColor: colors.gray04,
                  dotColor: colors.green01,
                  selectedDotColor: colors.green01,
                  arrowColor: colors.colorGradient,
                  monthTextColor: colors.black,

                  textMonthFontWeight: "bold",
                  textDayFontSize: 16,
                  textMonthFontSize: 16,
                  textDayHeaderFontSize: 16
                }}
                // markedDates={{
                //   "2012-03-01": {
                //     selected: true,
                //     selectedColor: colors.colorClipTop
                //   }
                // }}
                markingType={"custom"}
                markedDates={this.state.calenderSelectedValue}
                // Initially visible month. Default = Date()
                // current={this.state.currentDateForCalender}
                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                minDate={"2012-05-10"}
                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                //  maxDate={this.state.currentDateForCalender}
                // Handler which gets executed on day press. Default = undefined
                onDayPress={day => {
                  console.log("selected day", day);
                  console.log("m called", day);

                  this.getCurrentDateParams(day.timestamp);
                  console.log("m called", day);
                }}
                // Handler which gets executed on day long press. Default = undefined
                onDayLongPress={day => {
                  console.log("selected day", day);
                }}
                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                monthFormat={"MMMM yyyy"}
                // Handler which gets executed when visible month changes in calendar. Default = undefined
                onMonthChange={month => {
                  console.log("month changed", month);
                }}
                // Hide month navigation arrows. Default = false
                hideArrows={true}
                //  renderArrow={direction => <Arrow />}
                // Replace default arrows with custom ones (direction can be 'left' or 'right')
                // renderArrow={direction => <Arrow />}
                // Do not show days of other months in month page. Default = false
                hideExtraDays={true}
                // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                // day from another month that is visible in calendar page. Default = false
                disableMonthChange={false}
                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                firstDay={1}
                // Hide day names. Default = false
                hideDayNames={false}
                // Show week numbers to the left. Default = false
                showWeekNumbers={false}
                // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                onPressArrowLeft={substractMonth => substractMonth()}
                // Handler which gets executed when press arrow icon left. It receive a callback can go next month
                onPressArrowRight={addMonth => addMonth()}
              />
            </View>
            <View style={{}}>
              <View
                style={{
                  borderTopLeftRadius: 15,
                  borderTopRightRadius: 15,
                  borderBottomLeftRadius: 15,
                  borderBottomRightRadius: 15,
                  marginTop: "3%",
                  margin: "5%",
                  height: 400,
                  elevation: 1,
                  backgroundColor: colors.white
                }}
              >
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      marginTop: 10,
                      marginLeft: 20,
                      width: 60,
                      backgroundColor: colors.colorClipTop,
                      height: 60,
                      borderRadius: 35,
                      alignContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        alignContent: "center",
                        alignItems: "center",
                        fontSize: 25,

                        marginTop: "25%",
                        fontWeight: "bold",
                        color: colors.white
                      }}
                    >
                      {this.state.dateInitailNumber}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      flex: 0.8
                    }}
                  >
                    <View
                      style={{
                        flex: 1.8
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 25,
                          alignSelf: "center",
                          textAlignVertical: "bottom",
                          marginTop: 18,
                          fontWeight: "bold",
                          color: colors.black
                        }}
                      >
                        {this.state.dateMiddleText}
                      </Text>
                    </View>

                    <View style={{flex: 1}}>
                      <Text
                        style={{
                          alignSelf: "center",
                          fontSize: 18,
                          marginTop: "4%",
                          fontWeight: "bold",
                          color: colors.black
                        }}
                      >
                        {this.state.dateLastText}
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: 10,
                    backgroundColor: colors.gray01,
                    height: 1
                  }}
                />
                <View
                  style={{
                    margin: "1%",
                    flexDirection: "column",
                    height: "13%"
                  }}
                >
                  <View
                    style={{
                      flex: 0.6,
                      flexDirection: "row",

                      marginLeft: "3%"
                    }}
                  >
                    <View style={{flex: 0.1}}>
                      <Image
                        style={{height: 25, width: 25, marginTop: 5}}
                        source={require("../../src/assets/worker-icon.png")}
                      />
                    </View>
                    <View style={{flex: 0.9, height: 20}}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: 20,

                          fontSize: 16,

                          color: colors.colorClipTop,
                          fontWeight: "bold",

                          textAlignVertical: "center"
                        }}
                      >
                        {/* General contractor */}
                        Customer
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0.7,
                      flexDirection: "row",

                      marginLeft: "3%"
                    }}
                  >
                    <View style={{flex: 0.1}}/>
                    <View style={{flex: 0.9}}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: 20,

                          fontSize: 16,
                          textAlignVertical: "top",
                          color: colors.black,
                          fontWeight: "bold"
                        }}
                      >
                        {this.state.selectedGcName}
                      </Text>
                    </View>
                  </View>
                </View>

                {/* <View
                  style={{
                    margin: "1%",
                    flexDirection: "column",
                    marginTop: 15,
                    height: "13%"
                  }}
                >
                  <View
                    style={{
                      flex: 0.5,
                      flexDirection: "row",

                      marginLeft: "3%"
                    }}
                  >
                    <View style={{ flex: 0.1 }}>
                      <Image
                        style={{ height: 25, width: 25, marginTop: 5 }}
                        source={require("../../src/assets/clock.png")}
                      />
                    </View>
                    <View style={{ flex: 0.9, height: 20 }}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: 20,

                          fontSize: 16,

                          color: colors.colorClipTop,
                          fontWeight: "bold",

                          textAlignVertical: "center"
                        }}
                      >
                        Working hours
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0.5,
                      flexDirection: "row",

                      marginLeft: "3%"
                    }}
                  >
                    <View style={{ flex: 0.1 }} />
                    <View style={{ flex: 0.9 }}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: 20,

                          fontSize: 16,
                          textAlignVertical: "top",
                          color: colors.black,
                          fontWeight: "bold"
                        }}
                      >
                        {this.state.selectedWorkingHours}
                      </Text>
                    </View>
                  </View>
                </View> */}

                <View
                  style={{
                    margin: "1%",
                    flexDirection: "column",
                    marginTop: 15,
                    height: "13%"
                  }}
                >

                  <View
                    style={{
                      flex: 0.5,
                      flexDirection: "row",

                      marginLeft: "3%"
                    }}
                  >
                    <View style={{flex: 0.1}}>
                      <Image
                        style={{height: 25, width: 25, marginTop: 5}}
                        source={require("../../src/assets/email.png")}
                      />
                    </View>
                    <View style={{flex: 0.5, height: 20}}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: 20,

                          fontSize: 16,

                          color: colors.colorClipTop,
                          fontWeight: "bold",

                          textAlignVertical: "center"
                        }}
                      >
                        Email
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0.5,
                      flexDirection: "row",

                      marginLeft: "3%"
                    }}
                  >
                    <View style={{flex: 0.1}}/>
                    <View style={{flex: 0.9}}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: 20,

                          fontSize: 16,
                          textAlignVertical: "top",
                          color: colors.black,
                          fontWeight: "bold"
                        }}
                      >
                        {this.state.selectedEmailAddress}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    margin: "1%",
                    flexDirection: "column",
                    marginTop: 15,
                    height: "25%"
                  }}
                >
                  <View
                    style={{
                      flex: 0.5,
                      flexDirection: "row",

                      marginLeft: "3%"
                    }}
                  >
                    <View style={{flex: 0.1}}>
                      <Image
                        style={{height: 33, width: 25, marginTop: 5}}
                        source={require("../../src/assets/location.png")}
                      />
                    </View>
                    <View style={{flex: 0.5, height: 20}}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: 20,

                          fontSize: 16,

                          color: colors.colorClipTop,
                          fontWeight: "bold",

                          textAlignVertical: "center"
                        }}
                      >
                        Address
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0.5,
                      flexDirection: "row",
                      marginLeft: 40, marginTop: -40
                    }}
                  >
                    <View style={{flex: 0.1}}/>
                    <View style={{flex: 0.9}}>
                      <Text
                        style={{
                          flex: 1,
                          marginLeft: -10,
                          fontSize: 16,
                          textAlignVertical: "top",
                          color: colors.black,
                          fontWeight: "bold"
                        }}
                      >
                        {this.state.selectedAddress}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </BackgroundNtScroll>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
