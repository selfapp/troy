import {
  Platform,
  StyleSheet,
  ActivityIndicator,
  Button,
  FlatList,
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Avatar, Header, Icon, Text } from "react-native-elements";
import colors from "../styles/colors";
import React from "react";

import styles from "./ce-theme-style";

import MessageList from "./ce-view-messagelist";

import MessageEntry from "./ce-view-messageentry";
import ChatEngineProvider from "./ce-core-chatengineprovider";

class ChatRoom extends React.Component {
  constructor(props) {
    super(props);
  }

  renderContents() {
    let self = this;
    const navigate = this.props.navigation;

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 0.95}}>
          <MessageList
            ref="MessageList"
            navigation={self.props.navigation}
            now={ChatEngineProvider.getChatRoomModel().state.now}
            chatRoomModel={ChatEngineProvider.getChatRoomModel()}
          />
        </View>

        <View style={{flex: 0.05, marginBottom: 25}}>
          <MessageEntry
            chatRoomModel={ChatEngineProvider.getChatRoomModel()}
            typingIndicator
            // keyboardVerticalOffset={0}
          />
        </View>
      </View>
    );
  }


  
  render() {
    if (Platform.OS === "ios") {
      return (
        <SafeAreaView style={styles.container}>
          <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={95} style={{flex: 1}}>
            {this.renderContents()}
          </KeyboardAvoidingView>
        </SafeAreaView>
      );
    }

    return <View style={styles.container}>{this.renderContents()}</View>;
  }
}

export default ChatRoom;
