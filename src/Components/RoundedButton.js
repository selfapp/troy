import React from "react";
import {
  Text,
  TouchableOpacity,
  Image,
  View
} from "react-native";
import colors from "../styles/colors";

export default (RoundedButton = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        borderRadius: 15,
        backgroundColor: props.backgroundColor
      }}
    >
      {props.showImage ? (
        <View style={{ flexDirection: "column", height: 160 }}>
          <View
            style={{
              margin: 10,
              height: 90,
              borderRadius: 40,
              width: 80,
              alignSelf: "center",
              backgroundColor: colors.colorYogi
            }}
          >
            <Image
              style={{
                justifyContent: "center",
                marginTop: "35%",
                alignItems: "center",
                alignSelf: "center",
                marginLeft: "2%"
              }}
              source={props.imageSource}
            />
          </View>

          <Text
            style={{
              color: colors.black,
              fontSize: 16,
              // marginBottom: 10,
              alignSelf: "center",
              alignItems: "center",
              fontWeight: "bold",
              textAlign: "center",

            }}
          >
            {props.buttonName}
          </Text>
        </View>
      ) : (
          <View style={{ alignItems: "center" }}>
            <Text
              style={{
                color: props.textColor,
                marginTop: 13,
                fontSize: 16,
                justifyContent: "center",
                alignSelf: "center"
              }}
            >
              {props.buttonName}
            </Text>
          </View>
        )}
    </TouchableOpacity>
  );
});
