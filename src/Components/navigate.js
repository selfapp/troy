import { NavigationActions, StackActions } from 'react-navigation';

var navigate = {
    navigateTo(navigation, screen, params) {
        return (
            navigation.dispatch(NavigationActions.navigate(
                {
                    routeName: screen, params: params
                })
            )
        );
    },
    navigateWithReset(navigation, screen, params) {
        return (
            navigation.dispatch(StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({
                        routeName: screen,
                        action: NavigationActions.navigate({
                            routeName: screen,
                        }),
                    }),
                ],
                key: null,
            });
        );
        //     navigation.dispatch(NavigationActions.reset(
        //     {
        //         index: 0,
        //         actions: [
        //             NavigationActions.navigate({routeName: screen, params})
        //         ]
        //     })
        // )
    )}
};
module.exports = navigate;