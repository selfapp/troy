import React, {Component} from "react";
import {
  ImageBackground,
  StatusBar
} from "react-native";

export default class BackgroundNtScroll extends Component {
  render() {
    return (
      <ImageBackground
        source={require("../../src/assets/bg-screen.png")}
        resizeMode='stretch'
        style={{flex: 1}}
      >
        <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated/>
        {this.props.children}
      </ImageBackground>
    );
  }
}
