import PropTypes from "prop-types";
import React, { Component } from "react";
import { DrawerActions } from "react-navigation";
import ChatEngineProvider from "../lib/ce-core-chatengineprovider";

import {
  ScrollView,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image
} from "react-native";
import colors from "../styles/colors";
import { AsyncStorage } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
import {baseURL} from '../../api';

class SlideMenuWorker extends Component {
  navigateToScreen() {
    this.props.navigation.navigate("ProjectSelection");
  }

  Logout = async ()=> {
    if(ChatEngineProvider._connected){
      ChatEngineProvider.logout()
      this.logoutAPI()
    }else{
      this.logoutAPI()
    }
  }

  logoutAPI = async ()=> {
    console.log("Logout calle d......")
    try {

      
      const access_token = await AsyncStorage.getItem("access_token");
      console.log("acces token", access_token)
      fetch(`${baseURL}/logout`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token token=" + access_token
        }
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log("logout response 3333 .....")
          console.log(responseJson)
          // AsyncStorage.setItem("userType", "");
          // AsyncStorage.setItem("profileUpdated", "false");
          AsyncStorage.clear();
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Otp" })]
          });
    
        this.props.navigation.navigate("Otp");
        })
        .catch(e => {
        });
    } catch (error) {
      // Error retrieving data
      console.log("error kjjl", error)
    }
  }

  render() {
    return (
      <View style={{ backgroundColor: colors.colorDrawer, height: "100%" }}>
        <View
          style={{
            marginTop: 20,
            backgroundColor: colors.colorDrawer,
            alignContent: "center",
            alignItems: "center",
            height: 100
          }}
        >
          <Image
            style={{
              justifyContent: "center",
              marginTop: "8%",

              alignItems: "center"
            }}
            source={require("../../src/assets/staffup-logo.png")}
          />
        </View>
        <View
          style={{
            display: "none",
            marginTop: 30,

            flexDirection: "row"
          }}
        >
          <TouchableOpacity
            onPress={this.navigateToScreen.bind(this)}
            style={{
              flex: 1.5,

              alignSelf: "center"
            }}
          >
            <Text
              style={{
                marginLeft: 10,

                textAlignVertical: "bottom",
                color: colors.white,
                fontSize: 25,
                fontWeight: "bold"
              }}
            >
              My Proposals
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.navigateToScreen.bind(this)}
            style={{ flex: 0.4 }}
          >
            <Image
              style={{
                justifyContent: "center",
                marginTop: "6%",
                alignItems: "center"
              }}
              source={require("../../src/assets/job-list.png")}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 2,
            marginLeft: 10,
            marginRight: 10,
            marginTop: 15,

            backgroundColor: colors.white
          }}
        />

        <View
          style={{
            marginTop: 20,

            flexDirection: "row"
          }}
        >
          <TouchableOpacity
            onPress={this.Logout.bind(this)}
            style={{
              flex: 1.5,

              alignSelf: "center"
            }}
          >
            <Text
              style={{
                marginLeft: 10,

                textAlignVertical: "bottom",
                color: colors.white,
                fontSize: 25,
                fontWeight: "bold"
              }}
            >
              LogOut
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.Logout.bind(this)}
            style={{ flex: 0.4 }}
          >
            <Image
              style={{
                justifyContent: "center",
                marginTop: "6%",
                alignItems: "center"
              }}
              source={require("../../src/assets/project.png")}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

SlideMenuWorker.propTypes = {
  navigation: PropTypes.object
};
const styles = StyleSheet.create({
  footerContainer: {
    flex: 1
  },
  navItemStyle: {
    flex: 1
  },
  MainContainer: {
    flex: 1
  },
  sectionHeadingStyle: {
    flex: 1
  },
  container: {
    flex: 1
  },
  navSectionStyle: {
    flex: 1
  }
});
export default SlideMenuWorker;
