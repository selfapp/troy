import React from "react";
import { Text, TouchableOpacity, Platform, Image, View } from "react-native";
import colors from "../styles/colors";

export default (Button = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      disabled={props.disabled}
      style={{
        marginTop: props.marginTop,
        borderRadius: 20,
        marginLeft: "5%",
        marginRight: "5%",
        backgroundColor: props.backgroundColor,
        height: 40,
        width: props.width,
      }}
    >
      {props.showImage ? (
        <View style={{ flexDirection: "row", flex: 1 }}>
          <Image
            style={{
              flex: 1,
              marginTop: "1.5%",
              marginTop: "1%",
              alignItems: "center",
              alignSelf: "center",
              height: "100%"
            }}
            source={props.imageSource}
          />
          <View
            style={{
              height: "100%",
              marginTop: "1%",
              flex: 0.01,
              backgroundColor: "white"
            }}
          />
          <Text
            style={{
              color: colors.white,
              fontSize: 16,
              flex: 7.09,
              alignSelf: "center",

              fontWeight: "bold",
              textAlign: "center",

              marginTop: "3%"
            }}
          >
            {props.buttonName}
          </Text>
        </View>
      ) : (
          <View style={{ alignItems: "center", justifyContent: 'center', height: 40 }}>
            <Text
              style={{
                color: props.textColor,
                // marginTop: 13,
                fontSize: 16,
                justifyContent: "center",
                alignSelf: "center"
              }}
            >
              {props.buttonName}
            </Text>
          </View>
        )}
    </TouchableOpacity>
  );
});
