import React from "react";
import { View, ScrollView, ImageBackground, StatusBar } from "react-native";

export default (Background = props => {
  return (
    <ImageBackground
      source={require("../../src/assets/bg-screen.png")}
      resizeMode="stretch"
      style={{ flex: 1 }}
    >
      <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated />
      <ScrollView
        scrollEventThrottle={1000}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={"handled"}
        keyboardDismissMode={"interactive"}
      >
        {props.children}
      </ScrollView>
    </ImageBackground>
  );
});
