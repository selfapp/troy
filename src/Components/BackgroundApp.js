import React from "react";
import { View, ScrollView, ImageBackground, StatusBar } from "react-native";
import colors from "../styles/colors";

export default (BackgroundApp = props => {
  return (
    <ImageBackground

      source={require("../../src/assets/bg-screen.png")}
      resizeMode="stretch"
      style={{ flex: 1 }}
    >
      <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.20)" animated />
      {props.children}
    </ImageBackground>
  );
});
